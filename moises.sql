USE [Programmers]
GO
/****** Object:  UserDefinedFunction [dbo].[fnSplitStringTable05]    Script Date: 02/06/2020 9:31:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


create  FUNCTION [dbo].[fnSplitStringTable05] 
( 
    @string NVARCHAR(MAX), 
    @rowdelimiter CHAR(1) ,
	@columndemiliter CHAR(1)
) 
RETURNS @output TABLE(

 field1 NVARCHAR(MAX),  field2 NVARCHAR(MAX) ,  field3 NVARCHAR(MAX) ,  field4 NVARCHAR(MAX),  field5 NVARCHAR(MAX)

) 
BEGIN 

/*
'¯' Listas
'¬' Registros
'¦' Campos

*/

    DECLARE @start INT, @end INT 
	declare 
	 @campo1 NVARCHAR(MAX) = '' ,  @campo2 NVARCHAR(MAX) = '' ,  @campo3 NVARCHAR(MAX) = '' ,  @campo4 NVARCHAR(MAX) = '' ,  @campo5 NVARCHAR(MAX) = '' 

    SELECT @start = 1, @end = CHARINDEX(@rowdelimiter, @string) 
    WHILE @start < LEN(@string) + 1 BEGIN 
        IF @end = 0  
            SET @end = LEN(@string) + 1

		-- ===============================================================================================       
		DECLARE @lstCadena NVARCHAR(MAX)
		DECLARE @lstDato NVARCHAR(MAX)
		DECLARE @lnuPosComa int

		SET @lstCadena = SUBSTRING(@string, @start, @end - @start)	--Cadena de FILA

		declare @cuentacampos int = 0
		WHILE  LEN(@lstCadena)> 0
		BEGIN
			SET @lnuPosComa = CHARINDEX(@columndemiliter, @lstCadena ) -- Buscamos el caracter separador
			IF ( @lnuPosComa=0 )
			BEGIN
				SET @lstDato = @lstCadena
				SET @lstDato = isnull( @lstDato, 'er' )
				SET @lstCadena = ''
			END
			ELSE
			BEGIN
				SET @lstDato = Substring( @lstCadena , 1  , @lnuPosComa-1)
		
				SET @lstCadena = Substring( @lstCadena , @lnuPosComa + 1 , LEN(@lstCadena))
			END
	
			set @cuentacampos = @cuentacampos +1 

			if @cuentacampos =  1 set  @campo1 = @lstDato
			if @cuentacampos =  2 set  @campo2 = @lstDato
			if @cuentacampos =  3 set  @campo3 = @lstDato
			if @cuentacampos =  4 set  @campo4 = @lstDato
			if @cuentacampos =  5 set  @campo5 = @lstDato




		END
		-- ===============================================================================================       

		INSERT INTO @output ( 
			field1 , field2 , field3 , field4 , field5 ) 
        VALUES( 
			@campo1 , @campo2 , @campo3 , @campo4 , @campo5 ) 

		Select  
			@campo1 = '' ,  @campo2 = ''  ,  @campo3 = ''  ,  @campo4 = ''  ,  @campo5 = '' 

        SET @start = @end + 1 
        SET @end = CHARINDEX(@rowdelimiter, @string, @start)
        
    END 
    RETURN 
END






GO
/****** Object:  UserDefinedFunction [dbo].[fnSplitStringTable10]    Script Date: 02/06/2020 9:31:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


create   FUNCTION [dbo].[fnSplitStringTable10] 
( 
    @string NVARCHAR(MAX), 
    @rowdelimiter CHAR(1) ,
	@columndemiliter CHAR(1)
) 
RETURNS @output TABLE(

 field1 NVARCHAR(MAX) ,  field2 NVARCHAR(MAX) ,  field3 NVARCHAR(MAX) ,  field4 NVARCHAR(MAX) ,  field5 NVARCHAR(MAX) ,
 field6 NVARCHAR(MAX) ,  field7 NVARCHAR(MAX) ,  field8 NVARCHAR(MAX) ,  field9 NVARCHAR(MAX) , field10 NVARCHAR(MAX) 

) 
BEGIN 

/*
'¯' Listas
'¬' Registros
'¦' Campos

*/

    DECLARE @start INT, @end INT 
	Declare 
	@campo1 NVARCHAR(MAX) = '' ,  @campo2 NVARCHAR(MAX) = '' ,  @campo3 NVARCHAR(MAX) = '' ,  @campo4 NVARCHAR(MAX) = '' ,  @campo5 NVARCHAR(MAX) = '' ,
	@campo6 NVARCHAR(MAX) = '' ,  @campo7 NVARCHAR(MAX) = '' ,  @campo8 NVARCHAR(MAX) = '' ,  @campo9 NVARCHAR(MAX) = '' , @campo10 NVARCHAR(MAX) = '' 

    SELECT @start = 1, @end = CHARINDEX(@rowdelimiter, @string) 
    WHILE @start < LEN(@string) + 1 BEGIN 
        IF @end = 0  
            SET @end = LEN(@string) + 1

		-- ===============================================================================================       
		DECLARE @lstCadena NVARCHAR(MAX)
		DECLARE @lstDato NVARCHAR(MAX)
		DECLARE @lnuPosComa int

		SET @lstCadena = SUBSTRING(@string, @start, @end - @start)	--Cadena de FILA

		declare @cuentacampos int = 0
		WHILE  LEN(@lstCadena)> 0
		BEGIN
			SET @lnuPosComa = CHARINDEX(@columndemiliter, @lstCadena ) -- Buscamos el caracter separador
			IF ( @lnuPosComa=0 )
			BEGIN
				SET @lstDato = @lstCadena
				SET @lstCadena = ''
			END
			ELSE
			BEGIN
				SET @lstDato = Substring( @lstCadena , 1  , @lnuPosComa-1)
				SET @lstCadena = Substring( @lstCadena , @lnuPosComa + 1 , LEN(@lstCadena))
			END
	
			set @cuentacampos = @cuentacampos +1 

			if @cuentacampos =  1 set  @campo1 = @lstDato
			if @cuentacampos =  2 set  @campo2 = @lstDato
			if @cuentacampos =  3 set  @campo3 = @lstDato
			if @cuentacampos =  4 set  @campo4 = @lstDato
			if @cuentacampos =  5 set  @campo5 = @lstDato
			if @cuentacampos =  6 set  @campo6 = @lstDato
			if @cuentacampos =  7 set  @campo7 = @lstDato
			if @cuentacampos =  8 set  @campo8 = @lstDato
			if @cuentacampos =  9 set  @campo9 = @lstDato
			if @cuentacampos = 10 set @campo10 = @lstDato



		END
		-- ===============================================================================================       

		INSERT INTO @output ( 
			field1 , field2 , field3 , field4 , field5 , field6 , field7 , field8 , field9 , field10 ) 
        VALUES( 
			@campo1 , @campo2 , @campo3 , @campo4 , @campo5 , @campo6 , @campo7 , @campo8 , @campo9 , @campo10 ) 

		Select  
			@campo1 = '' ,  @campo2 = ''  ,  @campo3 = ''  ,  @campo4 = ''  ,  @campo5 = ''  ,
			@campo6 = '' ,  @campo7 = ''  ,  @campo8 = ''  ,  @campo9 = ''  , @campo10 = ''  

        SET @start = @end + 1 
        SET @end = CHARINDEX(@rowdelimiter, @string, @start)
        
    END 
    RETURN 
END






GO
/****** Object:  UserDefinedFunction [dbo].[fnSplitStringTable20]    Script Date: 02/06/2020 9:31:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create  FUNCTION [dbo].[fnSplitStringTable20] 
( 
    @string NVARCHAR(MAX), 
    @rowdelimiter CHAR(1) ,
	@columndemiliter CHAR(1)
) 
RETURNS @output TABLE(

 field1 NVARCHAR(MAX) ,  field2 NVARCHAR(MAX) ,  field3 NVARCHAR(MAX) ,  field4 NVARCHAR(MAX) ,  field5 NVARCHAR(MAX) ,
 field6 NVARCHAR(MAX) ,  field7 NVARCHAR(MAX) ,  field8 NVARCHAR(MAX) ,  field9 NVARCHAR(MAX) , field10 NVARCHAR(MAX) ,
field11 NVARCHAR(MAX) , field12 NVARCHAR(MAX) , field13 NVARCHAR(MAX) , field14 NVARCHAR(MAX) , field15 NVARCHAR(MAX) ,
field16 NVARCHAR(MAX) , field17 NVARCHAR(MAX) , field18 NVARCHAR(MAX) , field19 NVARCHAR(MAX) , field20 NVARCHAR(MAX)

) 
BEGIN 

/*
'¯' Listas
'¬' Registros
'¦' Campos

*/

    DECLARE @start INT, @end INT 
	declare 
	 @campo1 NVARCHAR(MAX) = '' ,  @campo2 NVARCHAR(MAX) = '' ,  @campo3 NVARCHAR(MAX) = '' ,  @campo4 NVARCHAR(MAX) = '' ,  @campo5 NVARCHAR(MAX) = '' ,
	 @campo6 NVARCHAR(MAX) = '' ,  @campo7 NVARCHAR(MAX) = '' ,  @campo8 NVARCHAR(MAX) = '' ,  @campo9 NVARCHAR(MAX) = '' ,  @campo10 NVARCHAR(MAX) = '' ,
	@campo11 NVARCHAR(MAX) = '' , @campo12 NVARCHAR(MAX) = '' ,  @campo13 NVARCHAR(MAX) = '' , @campo14 NVARCHAR(MAX) = '' , @campo15 NVARCHAR(MAX) = '',
	@campo16 NVARCHAR(MAX) = '' , @campo17 NVARCHAR(MAX) = '' ,  @campo18 NVARCHAR(MAX) = '' , @campo19 NVARCHAR(MAX) = '' , @campo20 NVARCHAR(MAX) = ''

    SELECT @start = 1, @end = CHARINDEX(@rowdelimiter, @string) 
    WHILE @start < LEN(@string) + 1 BEGIN 
        IF @end = 0  
            SET @end = LEN(@string) + 1

-- ===============================================================================================       
DECLARE @lstCadena NVARCHAR(MAX)
DECLARE @lstDato NVARCHAR(MAX)
DECLARE @lnuPosComa int

SET @lstCadena = SUBSTRING(@string, @start, @end - @start)	--Cadena de FILA

declare @cuentacampos int = 0
WHILE  LEN(@lstCadena)> 0
BEGIN
	SET @lnuPosComa = CHARINDEX(@columndemiliter, @lstCadena ) -- Buscamos el caracter separador
	IF ( @lnuPosComa=0 )
	BEGIN
		SET @lstDato = @lstCadena
		SET @lstCadena = ''
	END
	ELSE
	BEGIN
		SET @lstDato = Substring( @lstCadena , 1  , @lnuPosComa-1)
		SET @lstCadena = Substring( @lstCadena , @lnuPosComa + 1 , LEN(@lstCadena))
	END
	
	set @cuentacampos = @cuentacampos +1 

	if @cuentacampos =  1 set  @campo1 = @lstDato
	if @cuentacampos =  2 set  @campo2 = @lstDato
	if @cuentacampos =  3 set  @campo3 = @lstDato
	if @cuentacampos =  4 set  @campo4 = @lstDato
	if @cuentacampos =  5 set  @campo5 = @lstDato
	if @cuentacampos =  6 set  @campo6 = @lstDato
	if @cuentacampos =  7 set  @campo7 = @lstDato
	if @cuentacampos =  8 set  @campo8 = @lstDato
	if @cuentacampos =  9 set  @campo9 = @lstDato
	if @cuentacampos = 10 set @campo10 = @lstDato
	if @cuentacampos = 11 set @campo11 = @lstDato
	if @cuentacampos = 12 set @campo12 = @lstDato
	if @cuentacampos = 13 set @campo13 = @lstDato
	if @cuentacampos = 14 set @campo14 = @lstDato
	if @cuentacampos = 15 set @campo15 = @lstDato
	if @cuentacampos = 16 set @campo16 = @lstDato
	if @cuentacampos = 17 set @campo17 = @lstDato
	if @cuentacampos = 18 set @campo18 = @lstDato
	if @cuentacampos = 19 set @campo19 = @lstDato
	if @cuentacampos = 20 set @campo20 = @lstDato	

END
-- ===============================================================================================       

		INSERT INTO @output ( field1	,field2	,field3	,field4	,field5	,field6	,field7	,field8	,field9	,field10	,field11	,field12	,field13	,field14	,field15	,field16	,field17	,field18	,field19	,field20	 ) 
        VALUES( @campo1	,@campo2	,@campo3	,@campo4	,@campo5	,@campo6	,@campo7	,@campo8	,@campo9	,@campo10	,@campo11	,@campo12	,@campo13	,@campo14	,@campo15	,@campo16	,@campo17	,@campo18	,@campo19	,@campo20		 ) 

    SELECT  
			@campo1 = '',  @campo2 = '',  @campo3 = '',  @campo4 = '',  @campo5 = ''  ,
			@campo6 = '',  @campo7 = '',  @campo8 = '' , @campo9 = '',  @campo10 = '' ,
      @campo11 = '', @campo12 = '', @campo13 = '', @campo14 = '', @campo15 = '' ,
      @campo16 = '', @campo17 = '', @campo18 = '', @campo19 = '', @campo20 = ''

        SET @start = @end + 1 
        SET @end = CHARINDEX(@rowdelimiter, @string, @start)
        
    END 
    RETURN 
END






GO
/****** Object:  UserDefinedFunction [dbo].[fnSplitStringTable40]    Script Date: 02/06/2020 9:31:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create FUNCTION [dbo].[fnSplitStringTable40] 
( 
    @string NVARCHAR(MAX), 
    @rowdelimiter CHAR(1) ,
	@columndemiliter CHAR(1)
) 
RETURNS @output TABLE(

 field1 NVARCHAR(MAX) ,  field2 NVARCHAR(MAX) ,  field3 NVARCHAR(MAX) ,  field4 NVARCHAR(MAX) ,  field5 NVARCHAR(MAX) ,
 field6 NVARCHAR(MAX) ,  field7 NVARCHAR(MAX) ,  field8 NVARCHAR(MAX) ,  field9 NVARCHAR(MAX) , field10 NVARCHAR(MAX) ,
field11 NVARCHAR(MAX) , field12 NVARCHAR(MAX) , field13 NVARCHAR(MAX) , field14 NVARCHAR(MAX) , field15 NVARCHAR(MAX) ,
field16 NVARCHAR(MAX) , field17 NVARCHAR(MAX) , field18 NVARCHAR(MAX) , field19 NVARCHAR(MAX) , field20 NVARCHAR(MAX) ,
field21 NVARCHAR(MAX) , field22 NVARCHAR(MAX) , field23 NVARCHAR(MAX) , field24 NVARCHAR(MAX) , field25 NVARCHAR(MAX) ,
field26 NVARCHAR(MAX) , field27 NVARCHAR(MAX) , field28 NVARCHAR(MAX) , field29 NVARCHAR(MAX) , field30 NVARCHAR(MAX) ,
field31 NVARCHAR(MAX) , field32 NVARCHAR(MAX) , field33 NVARCHAR(MAX) , field34 NVARCHAR(MAX) , field35 NVARCHAR(MAX) ,
field36 NVARCHAR(MAX) , field37 NVARCHAR(MAX) , field38 NVARCHAR(MAX) , field39 NVARCHAR(MAX) , field40 NVARCHAR(MAX) 

) 
BEGIN 

/*
'¯' Listas
'¬' Registros
'¦' Campos

*/

    DECLARE @start INT, @end INT 
	declare 
	 @campo1 NVARCHAR(MAX) = '' ,  @campo2 NVARCHAR(MAX) = ''  , @campo3 NVARCHAR(MAX) = '' ,  @campo4 NVARCHAR(MAX) = '' ,  @campo5 NVARCHAR(MAX) = '' ,
	 @campo6 NVARCHAR(MAX) = '' ,  @campo7 NVARCHAR(MAX) = ''  , @campo8 NVARCHAR(MAX) = '' ,  @campo9 NVARCHAR(MAX) = '' ,  @campo10 NVARCHAR(MAX) = '' ,
	@campo11 NVARCHAR(MAX) = '' ,  @campo12 NVARCHAR(MAX) = '' , @campo13 NVARCHAR(MAX) = '' , @campo14 NVARCHAR(MAX) = '' , @campo15 NVARCHAR(MAX) = '',
	@campo16 NVARCHAR(MAX) = '' ,  @campo17 NVARCHAR(MAX) = '' , @campo18 NVARCHAR(MAX) = '' , @campo19 NVARCHAR(MAX) = '' , @campo20 NVARCHAR(MAX) = '',
	@campo21 NVARCHAR(MAX) = '' ,  @campo22 NVARCHAR(MAX) = '' , @campo23 NVARCHAR(MAX) = '' , @campo24 NVARCHAR(MAX) = '' , @campo25 NVARCHAR(MAX) = '',
	@campo26 NVARCHAR(MAX) = '' ,  @campo27 NVARCHAR(MAX) = '' , @campo28 NVARCHAR(MAX) = '' , @campo29 NVARCHAR(MAX) = '' , @campo30 NVARCHAR(MAX) = '',
	@campo31 NVARCHAR(MAX) = '' ,  @campo32 NVARCHAR(MAX) = '' , @campo33 NVARCHAR(MAX) = '' , @campo34 NVARCHAR(MAX) = '' , @campo35 NVARCHAR(MAX) = '' ,
	@campo36 NVARCHAR(MAX) = '' ,  @campo37 NVARCHAR(MAX) = '' , @campo38 NVARCHAR(MAX) = '' , @campo39 NVARCHAR(MAX) = '' , @campo40 NVARCHAR(MAX) = ''

    SELECT @start = 1, @end = CHARINDEX(@rowdelimiter, @string) 
    WHILE @start < LEN(@string) + 1 BEGIN 
        IF @end = 0  
            SET @end = LEN(@string) + 1

-- ===============================================================================================       
DECLARE @lstCadena NVARCHAR(MAX)
DECLARE @lstDato NVARCHAR(MAX)
DECLARE @lnuPosComa int

SET @lstCadena = SUBSTRING(@string, @start, @end - @start)	--Cadena de FILA

declare @cuentacampos int = 0
WHILE  LEN(@lstCadena)> 0
BEGIN
	SET @lnuPosComa = CHARINDEX(@columndemiliter, @lstCadena ) -- Buscamos el caracter separador
	IF ( @lnuPosComa=0 )
	BEGIN
		SET @lstDato = @lstCadena
		SET @lstCadena = ''
	END
	ELSE
	BEGIN
		SET @lstDato = Substring( @lstCadena , 1  , @lnuPosComa-1)
		SET @lstCadena = Substring( @lstCadena , @lnuPosComa + 1 , LEN(@lstCadena))
	END
	
	set @cuentacampos = @cuentacampos +1 

	if @cuentacampos =  1 set  @campo1 = @lstDato
	if @cuentacampos =  2 set  @campo2 = @lstDato
	if @cuentacampos =  3 set  @campo3 = @lstDato
	if @cuentacampos =  4 set  @campo4 = @lstDato
	if @cuentacampos =  5 set  @campo5 = @lstDato
	if @cuentacampos =  6 set  @campo6 = @lstDato
	if @cuentacampos =  7 set  @campo7 = @lstDato
	if @cuentacampos =  8 set  @campo8 = @lstDato
	if @cuentacampos =  9 set  @campo9 = @lstDato
	if @cuentacampos = 10 set @campo10 = @lstDato
	if @cuentacampos = 11 set @campo11 = @lstDato
	if @cuentacampos = 12 set @campo12 = @lstDato
	if @cuentacampos = 13 set @campo13 = @lstDato
	if @cuentacampos = 14 set @campo14 = @lstDato
	if @cuentacampos = 15 set @campo15 = @lstDato
	if @cuentacampos = 16 set @campo16 = @lstDato
	if @cuentacampos = 17 set @campo17 = @lstDato
	if @cuentacampos = 18 set @campo18 = @lstDato
	if @cuentacampos = 19 set @campo19 = @lstDato
	if @cuentacampos = 20 set @campo20 = @lstDato	
	if @cuentacampos = 21 set @campo21 = @lstDato
	if @cuentacampos = 22 set @campo22 = @lstDato
	if @cuentacampos = 23 set @campo23 = @lstDato
	if @cuentacampos = 24 set @campo24 = @lstDato
	if @cuentacampos = 25 set @campo25 = @lstDato
	if @cuentacampos = 26 set @campo26 = @lstDato
	if @cuentacampos = 27 set @campo27 = @lstDato
	if @cuentacampos = 28 set @campo28 = @lstDato
	if @cuentacampos = 29 set @campo29 = @lstDato
	if @cuentacampos = 30 set @campo30 = @lstDato
	if @cuentacampos = 31 set @campo31 = @lstDato
	if @cuentacampos = 32 set @campo32 = @lstDato
	if @cuentacampos = 33 set @campo33 = @lstDato
	if @cuentacampos = 34 set @campo34 = @lstDato
	if @cuentacampos = 35 set @campo35 = @lstDato
	if @cuentacampos = 36 set @campo36 = @lstDato
	if @cuentacampos = 37 set @campo37 = @lstDato
	if @cuentacampos = 38 set @campo38 = @lstDato
	if @cuentacampos = 39 set @campo39 = @lstDato
	if @cuentacampos = 40 set @campo40 = @lstDato

END
-- ===============================================================================================       

		INSERT INTO @output ( field1	,field2	,field3	,field4	,field5	,field6	,field7	,field8	,field9	,field10	,field11	,field12	,field13	,field14	,field15	,field16	,field17	,field18	,field19	,field20	,field21	,field22	,field23	,field24	,field25	,field26	,field27	,field28	,field29	,field30	,field31	,field32	,field33	,field34	,field35 , field36	,field37	,field38	,field39	,field40) 
        VALUES( @campo1	,@campo2	,@campo3	,@campo4	,@campo5	,@campo6	,@campo7	,@campo8	,@campo9	,@campo10	,@campo11	,@campo12	,@campo13	,@campo14	,@campo15	,@campo16	,@campo17	,@campo18	,@campo19	,@campo20	,@campo21	,@campo22	,@campo23	,@campo24	,@campo25	,@campo26	,@campo27	,@campo28	,@campo29	,@campo30	,@campo31	,@campo32	,@campo33	,@campo34	,@campo35, @campo36	,@campo37	,@campo38	,@campo39	,@campo40 ) 

    SELECT  
			@campo1 = '',  @campo2 = '',  @campo3 = '',  @campo4 = '',  @campo5 = ''  ,
			@campo6 = '',  @campo7 = '',  @campo8 = '' , @campo9 = '',  @campo10 = '' ,
      @campo11 = '', @campo12 = '', @campo13 = '', @campo14 = '', @campo15 = '' ,
      @campo16 = '', @campo17 = '', @campo18 = '', @campo19 = '', @campo20 = '' ,
      @campo21 = '', @campo22 = '', @campo23 = '', @campo24 = '', @campo25 = '' ,
      @campo26 = '', @campo27 = '', @campo28 = '', @campo29 = '', @campo30 = '' ,
      @campo31 = '', @campo32 = '', @campo33 = '', @campo34 = '', @campo35 = '' ,
      @campo36 = '', @campo37 = '', @campo38 = '', @campo39 = '', @campo40 = ''

        SET @start = @end + 1 
        SET @end = CHARINDEX(@rowdelimiter, @string, @start)
        
    END 
    RETURN 
END






GO
/****** Object:  UserDefinedFunction [dbo].[fnSplitStringTable55]    Script Date: 02/06/2020 9:31:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create FUNCTION [dbo].[fnSplitStringTable55] 
( 
    @string NVARCHAR(MAX), 
    @rowdelimiter CHAR(1) ,
	@columndemiliter CHAR(1)
) 
RETURNS @output TABLE(

 field1 NVARCHAR(MAX) ,  field2 NVARCHAR(MAX) ,  field3 NVARCHAR(MAX) ,  field4 NVARCHAR(MAX) ,  field5 NVARCHAR(MAX) ,
 field6 NVARCHAR(MAX) ,  field7 NVARCHAR(MAX) ,  field8 NVARCHAR(MAX) ,  field9 NVARCHAR(MAX) , field10 NVARCHAR(MAX) ,
field11 NVARCHAR(MAX) , field12 NVARCHAR(MAX) , field13 NVARCHAR(MAX) , field14 NVARCHAR(MAX) , field15 NVARCHAR(MAX) ,
field16 NVARCHAR(MAX) , field17 NVARCHAR(MAX) , field18 NVARCHAR(MAX) , field19 NVARCHAR(MAX) , field20 NVARCHAR(MAX) ,
field21 NVARCHAR(MAX) , field22 NVARCHAR(MAX) , field23 NVARCHAR(MAX) , field24 NVARCHAR(MAX) , field25 NVARCHAR(MAX) ,
field26 NVARCHAR(MAX) , field27 NVARCHAR(MAX) , field28 NVARCHAR(MAX) , field29 NVARCHAR(MAX) , field30 NVARCHAR(MAX) ,
field31 NVARCHAR(MAX) , field32 NVARCHAR(MAX) , field33 NVARCHAR(MAX) , field34 NVARCHAR(MAX) , field35 NVARCHAR(MAX) ,
field36 NVARCHAR(MAX) , field37 NVARCHAR(MAX) , field38 NVARCHAR(MAX) , field39 NVARCHAR(MAX) , field40 NVARCHAR(MAX) ,
field41 NVARCHAR(MAX) , field42 NVARCHAR(MAX) , field43 NVARCHAR(MAX) , field44 NVARCHAR(MAX) , field45 NVARCHAR(MAX) ,
field46 NVARCHAR(MAX) , field47 NVARCHAR(MAX) , field48 NVARCHAR(MAX) , field49 NVARCHAR(MAX) , field50 NVARCHAR(MAX) ,
field51 NVARCHAR(MAX) , field52 NVARCHAR(MAX) , field53 NVARCHAR(MAX) , field54 NVARCHAR(MAX) , field55 NVARCHAR(MAX) , field56 NVARCHAR(MAX) , field57 NVARCHAR(MAX) 

) 
BEGIN 

/*
'¯' Listas
'¬' Registros
'¦' Campos

*/

    DECLARE @start INT, @end INT 
	Declare 
	@campo1 NVARCHAR(MAX) = '' ,  @campo2 NVARCHAR(MAX) = '' ,  @campo3 NVARCHAR(MAX) = '' ,  @campo4 NVARCHAR(MAX) = '' ,  @campo5 NVARCHAR(MAX) = ''  ,
	@campo6 NVARCHAR(MAX) = '' ,  @campo7 NVARCHAR(MAX) = '' ,  @campo8 NVARCHAR(MAX) = '' ,  @campo9 NVARCHAR(MAX) = '' ,  @campo10 NVARCHAR(MAX) = '' ,
	@campo11 NVARCHAR(MAX) = '' , @campo12 NVARCHAR(MAX) = '' , @campo13 NVARCHAR(MAX) = '' , @campo14 NVARCHAR(MAX) = '' , @campo15 NVARCHAR(MAX) = '' ,
	@campo16 NVARCHAR(MAX) = '' , @campo17 NVARCHAR(MAX) = '' , @campo18 NVARCHAR(MAX) = '' , @campo19 NVARCHAR(MAX) = '' , @campo20 NVARCHAR(MAX) = '' ,
	@campo21 NVARCHAR(MAX) = '' , @campo22 NVARCHAR(MAX) = '' , @campo23 NVARCHAR(MAX) = '' , @campo24 NVARCHAR(MAX) = '' , @campo25 NVARCHAR(MAX) = '' ,
	@campo26 NVARCHAR(MAX) = '' , @campo27 NVARCHAR(MAX) = '' , @campo28 NVARCHAR(MAX) = '' , @campo29 NVARCHAR(MAX) = '' , @campo30 NVARCHAR(MAX) = '' ,
	@campo31 NVARCHAR(MAX) = '' , @campo32 NVARCHAR(MAX) = '' , @campo33 NVARCHAR(MAX) = '' , @campo34 NVARCHAR(MAX) = '' , @campo35 NVARCHAR(MAX) = '' ,
	@campo36 NVARCHAR(MAX) = '' , @campo37 NVARCHAR(MAX) = '' , @campo38 NVARCHAR(MAX) = '' , @campo39 NVARCHAR(MAX) = '' , @campo40 NVARCHAR(MAX) = '' ,
	@campo41 NVARCHAR(MAX) = '' , @campo42 NVARCHAR(MAX) = '' , @campo43 NVARCHAR(MAX) = '' , @campo44 NVARCHAR(MAX) = '' , @campo45 NVARCHAR(MAX) = '' ,
	@campo46 NVARCHAR(MAX) = '' , @campo47 NVARCHAR(MAX) = '' , @campo48 NVARCHAR(MAX) = '' , @campo49 NVARCHAR(MAX) = '' , @campo50 NVARCHAR(MAX) = '' ,
	@campo51 NVARCHAR(MAX) = '' , @campo52 NVARCHAR(MAX) = '' , @campo53 NVARCHAR(MAX) = '' , @campo54 NVARCHAR(MAX) = '' , @campo55 NVARCHAR(MAX) = '', @campo56 NVARCHAR(MAX)='', @campo57 NVARCHAR(MAX)

    SELECT @start = 1, @end = CHARINDEX(@rowdelimiter, @string) 
    WHILE @start < LEN(@string) + 1 BEGIN 
        IF @end = 0  
            SET @end = LEN(@string) + 1

-- ===============================================================================================       
DECLARE @lstCadena NVARCHAR(MAX)
DECLARE @lstDato NVARCHAR(MAX)
DECLARE @lnuPosComa int

SET @lstCadena = SUBSTRING(@string, @start, @end - @start)	--Cadena de FILA

declare @cuentacampos int = 0
WHILE  LEN(@lstCadena)> 0
BEGIN
	SET @lnuPosComa = CHARINDEX(@columndemiliter, @lstCadena ) -- Buscamos el caracter separador
	IF ( @lnuPosComa=0 )
	BEGIN
		SET @lstDato = @lstCadena
		SET @lstCadena = ''
	END
	ELSE
	BEGIN
		SET @lstDato = Substring( @lstCadena , 1  , @lnuPosComa-1)
		SET @lstCadena = Substring( @lstCadena , @lnuPosComa + 1 , LEN(@lstCadena))
	END
	
	set @cuentacampos = @cuentacampos +1 

	if @cuentacampos =  1 set  @campo1 = @lstDato
	if @cuentacampos =  2 set  @campo2 = @lstDato
	if @cuentacampos =  3 set  @campo3 = @lstDato
	if @cuentacampos =  4 set  @campo4 = @lstDato
	if @cuentacampos =  5 set  @campo5 = @lstDato
	if @cuentacampos =  6 set  @campo6 = @lstDato
	if @cuentacampos =  7 set  @campo7 = @lstDato
	if @cuentacampos =  8 set  @campo8 = @lstDato
	if @cuentacampos =  9 set  @campo9 = @lstDato
	if @cuentacampos = 10 set @campo10 = @lstDato
	if @cuentacampos = 11 set @campo11 = @lstDato
	if @cuentacampos = 12 set @campo12 = @lstDato
	if @cuentacampos = 13 set @campo13 = @lstDato
	if @cuentacampos = 14 set @campo14 = @lstDato
	if @cuentacampos = 15 set @campo15 = @lstDato
	if @cuentacampos = 16 set @campo16 = @lstDato
	if @cuentacampos = 17 set @campo17 = @lstDato
	if @cuentacampos = 18 set @campo18 = @lstDato
	if @cuentacampos = 19 set @campo19 = @lstDato
	if @cuentacampos = 20 set @campo20 = @lstDato	
	if @cuentacampos = 21 set @campo21 = @lstDato
	if @cuentacampos = 22 set @campo22 = @lstDato
	if @cuentacampos = 23 set @campo23 = @lstDato
	if @cuentacampos = 24 set @campo24 = @lstDato
	if @cuentacampos = 25 set @campo25 = @lstDato
	if @cuentacampos = 26 set @campo26 = @lstDato
	if @cuentacampos = 27 set @campo27 = @lstDato
	if @cuentacampos = 28 set @campo28 = @lstDato
	if @cuentacampos = 29 set @campo29 = @lstDato
	if @cuentacampos = 30 set @campo30 = @lstDato
	if @cuentacampos = 31 set @campo31 = @lstDato
	if @cuentacampos = 32 set @campo32 = @lstDato
	if @cuentacampos = 33 set @campo33 = @lstDato
	if @cuentacampos = 34 set @campo34 = @lstDato
	if @cuentacampos = 35 set @campo35 = @lstDato
	if @cuentacampos = 36 set @campo36 = @lstDato
	if @cuentacampos = 37 set @campo37 = @lstDato
	if @cuentacampos = 38 set @campo38 = @lstDato
	if @cuentacampos = 39 set @campo39 = @lstDato
	if @cuentacampos = 40 set @campo40 = @lstDato
	if @cuentacampos = 41 set @campo41 = @lstDato
	if @cuentacampos = 42 set @campo42 = @lstDato
	if @cuentacampos = 43 set @campo43 = @lstDato
	if @cuentacampos = 44 set @campo44 = @lstDato
	if @cuentacampos = 45 set @campo45 = @lstDato
	if @cuentacampos = 46 set @campo46 = @lstDato
	if @cuentacampos = 47 set @campo47 = @lstDato
	if @cuentacampos = 48 set @campo48 = @lstDato
	if @cuentacampos = 49 set @campo49 = @lstDato
	if @cuentacampos = 50 set @campo50 = @lstDato
	if @cuentacampos = 51 set @campo51 = @lstDato
	if @cuentacampos = 52 set @campo52 = @lstDato
	if @cuentacampos = 53 set @campo53 = @lstDato
	if @cuentacampos = 54 set @campo54 = @lstDato
	if @cuentacampos = 55 set @campo55 = @lstDato
	if @cuentacampos = 56 set @campo56 = @lstDato
	if @cuentacampos = 57 set @campo57 = @lstDato
END
-- ===============================================================================================       

		INSERT INTO @output ( field1	,field2	,field3	,field4	,field5	,field6	,field7	,field8	,field9	,field10	,field11	,field12	,field13	,field14	,field15	,field16	,field17	,field18	,field19	,field20	,field21	,field22	,field23	,field24	,field25	,field26	,field27	,field28	,field29	,field30	,field31	,field32	,field33	,field34	,field35 , field36	,field37	,field38	,field39	,field40, field41	,field42	,field43	,field44	,field45, field46	,field47	,field48	,field49	,field50, field51	,field52	,field53	,field54	,field55,field56,field57) 
        VALUES( @campo1	,@campo2	,@campo3	,@campo4	,@campo5	,@campo6	,@campo7	,@campo8	,@campo9	,@campo10	,@campo11	,@campo12	,@campo13	,@campo14	,@campo15	,@campo16	,@campo17	,@campo18	,@campo19	,@campo20	,@campo21	,@campo22	,@campo23	,@campo24	,@campo25	,@campo26	,@campo27	,@campo28	,@campo29	,@campo30	,@campo31	,@campo32	,@campo33	,@campo34	,@campo35, @campo36	,@campo37	,@campo38	,@campo39	,@campo40, @campo41	,@campo42	,@campo43	,@campo44	,@campo45, @campo46, @campo47	,@campo48	,@campo49	,@campo50, @campo51	,@campo52	,@campo53	,@campo54	,@campo56,@campo55,@campo57 ) 

    SELECT  
			@campo1 = '',  @campo2 = '',  @campo3 = '',  @campo4 = '',  @campo5 = ''  ,
			@campo6 = '',  @campo7 = '',  @campo8 = '' , @campo9 = '',  @campo10 = '' ,
      @campo11 = '', @campo12 = '', @campo13 = '', @campo14 = '', @campo15 = '' ,
      @campo16 = '', @campo17 = '', @campo18 = '', @campo19 = '', @campo20 = '' ,
      @campo21 = '', @campo22 = '', @campo23 = '', @campo24 = '', @campo25 = '' ,
      @campo26 = '', @campo27 = '', @campo28 = '', @campo29 = '', @campo30 = '' ,
      @campo31 = '', @campo32 = '', @campo33 = '', @campo34 = '', @campo35 = '' ,
      @campo36 = '', @campo37 = '', @campo38 = '', @campo39 = '', @campo40 = '' ,
      @campo41 = '', @campo42 = '', @campo43 = '', @campo44 = '', @campo45 = '' ,
      @campo46 = '', @campo47 = '', @campo48 = '', @campo49 = '', @campo50 = '' ,
      @campo51 = '', @campo52 = '', @campo53 = '', @campo54 = '', @campo55 = '', @campo56 = '', @campo57 = ''

        SET @start = @end + 1 
        SET @end = CHARINDEX(@rowdelimiter, @string, @start)
        
    END 
    RETURN 
END






GO
/****** Object:  UserDefinedFunction [dbo].[fnSplitStringTable85]    Script Date: 02/06/2020 9:31:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create FUNCTION [dbo].[fnSplitStringTable85] 
( 
    @string NVARCHAR(MAX), 
    @rowdelimiter CHAR(1) ,
	@columndemiliter CHAR(1)
) 
RETURNS @output TABLE(

 field1 NVARCHAR(MAX) ,  field2 NVARCHAR(MAX) ,  field3 NVARCHAR(MAX) ,  field4 NVARCHAR(MAX) ,  field5 NVARCHAR(MAX) ,
 field6 NVARCHAR(MAX) ,  field7 NVARCHAR(MAX) ,  field8 NVARCHAR(MAX) ,  field9 NVARCHAR(MAX) , field10 NVARCHAR(MAX) ,
field11 NVARCHAR(MAX) , field12 NVARCHAR(MAX) , field13 NVARCHAR(MAX) , field14 NVARCHAR(MAX) , field15 NVARCHAR(MAX) ,
field16 NVARCHAR(MAX) , field17 NVARCHAR(MAX) , field18 NVARCHAR(MAX) , field19 NVARCHAR(MAX) , field20 NVARCHAR(MAX) ,
field21 NVARCHAR(MAX) , field22 NVARCHAR(MAX) , field23 NVARCHAR(MAX) , field24 NVARCHAR(MAX) , field25 NVARCHAR(MAX) ,
field26 NVARCHAR(MAX) , field27 NVARCHAR(MAX) , field28 NVARCHAR(MAX) , field29 NVARCHAR(MAX) , field30 NVARCHAR(MAX) ,
field31 NVARCHAR(MAX) , field32 NVARCHAR(MAX) , field33 NVARCHAR(MAX) , field34 NVARCHAR(MAX) , field35 NVARCHAR(MAX) ,
field36 NVARCHAR(MAX) , field37 NVARCHAR(MAX) , field38 NVARCHAR(MAX) , field39 NVARCHAR(MAX) , field40 NVARCHAR(MAX) ,
field41 NVARCHAR(MAX) , field42 NVARCHAR(MAX) , field43 NVARCHAR(MAX) , field44 NVARCHAR(MAX) , field45 NVARCHAR(MAX) ,
field46 NVARCHAR(MAX) , field47 NVARCHAR(MAX) , field48 NVARCHAR(MAX) , field49 NVARCHAR(MAX) , field50 NVARCHAR(MAX) ,
field51 NVARCHAR(MAX) , field52 NVARCHAR(MAX) , field53 NVARCHAR(MAX) , field54 NVARCHAR(MAX) , field55 NVARCHAR(MAX) ,
field56 NVARCHAR(MAX) , field57 NVARCHAR(MAX) , field58 NVARCHAR(MAX) , field59 NVARCHAR(MAX) , field60 NVARCHAR(MAX) ,
field61 NVARCHAR(MAX) , field62 NVARCHAR(MAX) , field63 NVARCHAR(MAX) , field64 NVARCHAR(MAX) , field65 NVARCHAR(MAX) ,
field66 NVARCHAR(MAX) , field67 NVARCHAR(MAX) , field68 NVARCHAR(MAX) , field69 NVARCHAR(MAX) , field70 NVARCHAR(MAX) ,
field71 NVARCHAR(MAX) , field72 NVARCHAR(MAX) , field73 NVARCHAR(MAX) , field74 NVARCHAR(MAX) , field75 NVARCHAR(MAX),
field76 NVARCHAR(MAX) ,field77 NVARCHAR(MAX) , field78 NVARCHAR(MAX) , field79 NVARCHAR(MAX) , field80 NVARCHAR(MAX) ,
 field81 NVARCHAR(MAX), field82 NVARCHAR(MAX), field83 NVARCHAR(MAX), field84 NVARCHAR(MAX), field85 NVARCHAR(MAX)
) 
BEGIN 

/*
'¯' Listas
'¬' Registros
'¦' Campos

*/

    DECLARE @start INT, @end INT 
	Declare 
	@campo1 NVARCHAR(MAX) = '' ,  @campo2 NVARCHAR(MAX) = '' ,  @campo3 NVARCHAR(MAX) = '' ,  @campo4 NVARCHAR(MAX) = '' ,  @campo5 NVARCHAR(MAX) = ''  ,
	@campo6 NVARCHAR(MAX) = '' ,  @campo7 NVARCHAR(MAX) = '' ,  @campo8 NVARCHAR(MAX) = '' ,  @campo9 NVARCHAR(MAX) = '' ,  @campo10 NVARCHAR(MAX) = '' ,
	@campo11 NVARCHAR(MAX) = '' , @campo12 NVARCHAR(MAX) = '' , @campo13 NVARCHAR(MAX) = '' , @campo14 NVARCHAR(MAX) = '' , @campo15 NVARCHAR(MAX) = '' ,
	@campo16 NVARCHAR(MAX) = '' , @campo17 NVARCHAR(MAX) = '' , @campo18 NVARCHAR(MAX) = '' , @campo19 NVARCHAR(MAX) = '' , @campo20 NVARCHAR(MAX) = '' ,
	@campo21 NVARCHAR(MAX) = '' , @campo22 NVARCHAR(MAX) = '' , @campo23 NVARCHAR(MAX) = '' , @campo24 NVARCHAR(MAX) = '' , @campo25 NVARCHAR(MAX) = '' ,
	@campo26 NVARCHAR(MAX) = '' , @campo27 NVARCHAR(MAX) = '' , @campo28 NVARCHAR(MAX) = '' , @campo29 NVARCHAR(MAX) = '' , @campo30 NVARCHAR(MAX) = '' ,
	@campo31 NVARCHAR(MAX) = '' , @campo32 NVARCHAR(MAX) = '' , @campo33 NVARCHAR(MAX) = '' , @campo34 NVARCHAR(MAX) = '' , @campo35 NVARCHAR(MAX) = '' ,
	@campo36 NVARCHAR(MAX) = '' , @campo37 NVARCHAR(MAX) = '' , @campo38 NVARCHAR(MAX) = '' , @campo39 NVARCHAR(MAX) = '' , @campo40 NVARCHAR(MAX) = '' ,
	@campo41 NVARCHAR(MAX) = '' , @campo42 NVARCHAR(MAX) = '' , @campo43 NVARCHAR(MAX) = '' , @campo44 NVARCHAR(MAX) = '' , @campo45 NVARCHAR(MAX) = '' ,
	@campo46 NVARCHAR(MAX) = '' , @campo47 NVARCHAR(MAX) = '' , @campo48 NVARCHAR(MAX) = '' , @campo49 NVARCHAR(MAX) = '' , @campo50 NVARCHAR(MAX) = '' ,
	@campo51 NVARCHAR(MAX) = '' , @campo52 NVARCHAR(MAX) = '' , @campo53 NVARCHAR(MAX) = '' , @campo54 NVARCHAR(MAX) = '' , @campo55 NVARCHAR(MAX) = '' ,
	@campo56 NVARCHAR(MAX) = '' , @campo57 NVARCHAR(MAX) = '' , @campo58 NVARCHAR(MAX) = '' , @campo59 NVARCHAR(MAX) = '' , @campo60 NVARCHAR(MAX) = '' ,
	@campo61 NVARCHAR(MAX) = '' , @campo62 NVARCHAR(MAX) = '' , @campo63 NVARCHAR(MAX) = '' , @campo64 NVARCHAR(MAX) = '' , @campo65 NVARCHAR(MAX) = '' ,
   @campo66 NVARCHAR(MAX) = '' , @campo67 NVARCHAR(MAX) = '' , @campo68 NVARCHAR(MAX) = '' , @campo69 NVARCHAR(MAX) = '' , @campo70 NVARCHAR(MAX) = '' ,
   @campo71 NVARCHAR(MAX) = '' , @campo72 NVARCHAR(MAX) = '' , @campo73 NVARCHAR(MAX) = '' , @campo74 NVARCHAR(MAX) = '' , @campo75 NVARCHAR(MAX) = '' ,
   @campo76 NVARCHAR(MAX) = '' , @campo77 NVARCHAR(MAX) = '' , @campo78 NVARCHAR(MAX) = '' , @campo79 NVARCHAR(MAX) = '' , @campo80 NVARCHAR(MAX) = '' , 
   @campo81 NVARCHAR(MAX) = '' , @campo82 NVARCHAR(MAX) = '' , @campo83 NVARCHAR(MAX) = '' , @campo84 NVARCHAR(MAX) = '', @campo85 NVARCHAR(MAX) = ''

    SELECT @start = 1, @end = CHARINDEX(@rowdelimiter, @string) 
    WHILE @start < LEN(@string) + 1 BEGIN 
        IF @end = 0  
            SET @end = LEN(@string) + 1

-- ===============================================================================================       
DECLARE @lstCadena NVARCHAR(MAX)
DECLARE @lstDato NVARCHAR(MAX)
DECLARE @lnuPosComa int

SET @lstCadena = SUBSTRING(@string, @start, @end - @start)	--Cadena de FILA

declare @cuentacampos int = 0
WHILE  LEN(@lstCadena)> 0
BEGIN
	SET @lnuPosComa = CHARINDEX(@columndemiliter, @lstCadena ) -- Buscamos el caracter separador
	IF ( @lnuPosComa=0 )
	BEGIN
		SET @lstDato = @lstCadena
		SET @lstCadena = ''
	END
	ELSE
	BEGIN
		SET @lstDato = Substring( @lstCadena , 1  , @lnuPosComa-1)
		SET @lstCadena = Substring( @lstCadena , @lnuPosComa + 1 , LEN(@lstCadena))
	END
	
	set @cuentacampos = @cuentacampos +1 

	if @cuentacampos =  1 set  @campo1 = @lstDato
	if @cuentacampos =  2 set  @campo2 = @lstDato
	if @cuentacampos =  3 set  @campo3 = @lstDato
	if @cuentacampos =  4 set  @campo4 = @lstDato
	if @cuentacampos =  5 set  @campo5 = @lstDato
	if @cuentacampos =  6 set  @campo6 = @lstDato
	if @cuentacampos =  7 set  @campo7 = @lstDato
	if @cuentacampos =  8 set  @campo8 = @lstDato
	if @cuentacampos =  9 set  @campo9 = @lstDato
	if @cuentacampos = 10 set @campo10 = @lstDato
	if @cuentacampos = 11 set @campo11 = @lstDato
	if @cuentacampos = 12 set @campo12 = @lstDato
	if @cuentacampos = 13 set @campo13 = @lstDato
	if @cuentacampos = 14 set @campo14 = @lstDato
	if @cuentacampos = 15 set @campo15 = @lstDato
	if @cuentacampos = 16 set @campo16 = @lstDato
	if @cuentacampos = 17 set @campo17 = @lstDato
	if @cuentacampos = 18 set @campo18 = @lstDato
	if @cuentacampos = 19 set @campo19 = @lstDato
	if @cuentacampos = 20 set @campo20 = @lstDato	
	if @cuentacampos = 21 set @campo21 = @lstDato
	if @cuentacampos = 22 set @campo22 = @lstDato
	if @cuentacampos = 23 set @campo23 = @lstDato
	if @cuentacampos = 24 set @campo24 = @lstDato
	if @cuentacampos = 25 set @campo25 = @lstDato
	if @cuentacampos = 26 set @campo26 = @lstDato
	if @cuentacampos = 27 set @campo27 = @lstDato
	if @cuentacampos = 28 set @campo28 = @lstDato
	if @cuentacampos = 29 set @campo29 = @lstDato
	if @cuentacampos = 30 set @campo30 = @lstDato
	if @cuentacampos = 31 set @campo31 = @lstDato
	if @cuentacampos = 32 set @campo32 = @lstDato
	if @cuentacampos = 33 set @campo33 = @lstDato
	if @cuentacampos = 34 set @campo34 = @lstDato
	if @cuentacampos = 35 set @campo35 = @lstDato
	if @cuentacampos = 36 set @campo36 = @lstDato
	if @cuentacampos = 37 set @campo37 = @lstDato
	if @cuentacampos = 38 set @campo38 = @lstDato
	if @cuentacampos = 39 set @campo39 = @lstDato
	if @cuentacampos = 40 set @campo40 = @lstDato
	if @cuentacampos = 41 set @campo41 = @lstDato
	if @cuentacampos = 42 set @campo42 = @lstDato
	if @cuentacampos = 43 set @campo43 = @lstDato
	if @cuentacampos = 44 set @campo44 = @lstDato
	if @cuentacampos = 45 set @campo45 = @lstDato
	if @cuentacampos = 46 set @campo46 = @lstDato
	if @cuentacampos = 47 set @campo47 = @lstDato
	if @cuentacampos = 48 set @campo48 = @lstDato
	if @cuentacampos = 49 set @campo49 = @lstDato
	if @cuentacampos = 50 set @campo50 = @lstDato
	if @cuentacampos = 51 set @campo51 = @lstDato
	if @cuentacampos = 52 set @campo52 = @lstDato
	if @cuentacampos = 53 set @campo53 = @lstDato
	if @cuentacampos = 54 set @campo54 = @lstDato
	if @cuentacampos = 55 set @campo55 = @lstDato
	if @cuentacampos = 56 set @campo56 = @lstDato
	if @cuentacampos = 57 set @campo57 = @lstDato
	if @cuentacampos = 58 set @campo58 = @lstDato
	if @cuentacampos = 59 set @campo59 = @lstDato
	if @cuentacampos = 60 set @campo60 = @lstDato
	if @cuentacampos = 61 set @campo61 = @lstDato
	if @cuentacampos = 62 set @campo62 = @lstDato
	if @cuentacampos = 63 set @campo63 = @lstDato
	if @cuentacampos = 64 set @campo64 = @lstDato
	if @cuentacampos = 65 set @campo65 = @lstDato
   if @cuentacampos = 66 set @campo66 = @lstDato
	if @cuentacampos = 67 set @campo67 = @lstDato
	if @cuentacampos = 68 set @campo68 = @lstDato
	if @cuentacampos = 69 set @campo69 = @lstDato
	if @cuentacampos = 70 set @campo70 = @lstDato
   if @cuentacampos = 71 set @campo71 = @lstDato
	if @cuentacampos = 72 set @campo72 = @lstDato
	if @cuentacampos = 73 set @campo73 = @lstDato
	if @cuentacampos = 74 set @campo74 = @lstDato
	if @cuentacampos = 75 set @campo75 = @lstDato
	if @cuentacampos = 76 set @campo61 = @lstDato
	if @cuentacampos = 77 set @campo62 = @lstDato
	if @cuentacampos = 78 set @campo63 = @lstDato
	if @cuentacampos = 79 set @campo64 = @lstDato
	if @cuentacampos = 80 set @campo65 = @lstDato
   if @cuentacampos = 81 set @campo66 = @lstDato
	if @cuentacampos = 82 set @campo67 = @lstDato
	if @cuentacampos = 83 set @campo68 = @lstDato
	if @cuentacampos = 84 set @campo69 = @lstDato
	if @cuentacampos = 85 set @campo70 = @lstDato
 

END
-- ===============================================================================================       

		INSERT INTO @output ( 
		field1  , field2  , field3  , field4  , field5  , field6  , field7  , field8  , field9  , field10 ,
		field11	, field12 , field13 , field14 , field15 , field16 , field17 , field18 , field19 , field20 ,
		field21 , field22 , field23 , field24 , field25 , field26 , field27 , field28 , field29 , field30 ,
		field31 , field32 , field33 , field34 , field35 , field36 , field37 , field38 , field39 , field40 , 
		field41 , field42 , field43 , field44 , field45 , field46 , field47 , field48 , field49 , field50 ,
		field51 , field52 , field53 , field54 , field55 , field56 , field57 , field58 , field59 , field60 ,
		field61 , field62 , field63 , field64 , field65 , field66 , field67 , field68 , field69 , field70 ,
      field71 , field72 , field73 , field74 , field75,field76 , field77 , field78 , field79 , field80 , field81 ,
      field82 , field83 , field84 , field85  ) 
        VALUES(
		@campo1	 , @campo2  , @campo3  , @campo4  , @campo5  , @campo6  , @campo7  , @campo8  , @campo9  , @campo10 ,
		@campo11 , @campo12 , @campo13 , @campo14 , @campo15 , @campo16 , @campo17 , @campo18 , @campo19 , @campo20 ,
		@campo21 , @campo22 , @campo23 , @campo24 , @campo25 , @campo26 , @campo27 , @campo28 , @campo29 , @campo30 ,
		@campo31 , @campo32 , @campo33 , @campo34 , @campo35 , @campo36 , @campo37 , @campo38 , @campo39 , @campo40 , 
		@campo41 , @campo42 , @campo43 , @campo44 , @campo45 , @campo46 , @campo47 , @campo48 , @campo49 , @campo50 , 
		@campo51 , @campo52 , @campo53 , @campo54 , @campo55 , @campo56 , @campo57 , @campo58 , @campo59 , @campo60 ,
		@campo61 , @campo62 , @campo63 , @campo64 , @campo65 , @campo66 , @campo67 , @campo68 , @campo69 , @campo70 ,
      @campo71 , @campo72 , @campo73 , @campo74 , @campo75 , @campo76 , @campo77 , @campo78 , @campo79 , @campo80 ,
      @campo81 , @campo82 , @campo83 , @campo84 , @campo85) 

    SELECT  
			@campo1 = '',  @campo2 = '',  @campo3 = '',  @campo4 = '',  @campo5 = ''  ,
			@campo6 = '',  @campo7 = '',  @campo8 = '' , @campo9 = '',  @campo10 = '' ,
      @campo11 = '', @campo12 = '', @campo13 = '', @campo14 = '', @campo15 = '' ,
      @campo16 = '', @campo17 = '', @campo18 = '', @campo19 = '', @campo20 = '' ,
      @campo21 = '', @campo22 = '', @campo23 = '', @campo24 = '', @campo25 = '' ,
      @campo26 = '', @campo27 = '', @campo28 = '', @campo29 = '', @campo30 = '' ,
      @campo31 = '', @campo32 = '', @campo33 = '', @campo34 = '', @campo35 = '' ,
      @campo36 = '', @campo37 = '', @campo38 = '', @campo39 = '', @campo40 = '' ,
      @campo41 = '', @campo42 = '', @campo43 = '', @campo44 = '', @campo45 = '' ,
      @campo46 = '', @campo47 = '', @campo48 = '', @campo49 = '', @campo50 = '' ,
      @campo51 = '', @campo52 = '', @campo53 = '', @campo54 = '', @campo55 = '' ,
      @campo56 = '', @campo57 = '', @campo58 = '', @campo59 = '', @campo60 = '' ,
	   @campo61 = '', @campo62 = '', @campo63 = '', @campo64 = '', @campo65 = '' ,
      @campo66 = '', @campo67 = '', @campo68 = '', @campo69 = '', @campo70 = '' ,
      @campo71 = '', @campo72 = '', @campo73 = '', @campo74 = '', @campo75 = '' ,
	    @campo76 = '', @campo77 = '', @campo78 = '', @campo79 = '', @campo80 = '' ,
      @campo81 = '', @campo82 = '', @campo83 = '', @campo84 = '', @campo85 = ''
        SET @start = @end + 1 
        SET @end = CHARINDEX(@rowdelimiter, @string, @start)
        
    END 
    RETURN 
END




GO
/****** Object:  Table [dbo].[BoletaElectronica]    Script Date: 02/06/2020 9:31:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BoletaElectronica](
	[ID] [bigint] NOT NULL,
	[TipoDocumento] [varchar](2) NOT NULL,
	[SerieDocumento] [varchar](4) NOT NULL,
	[CorrelativoDocumento] [decimal](8, 0) NOT NULL,
	[FechaEmision] [datetime] NOT NULL,
	[FechaVencimiento] [datetime] NULL,
	[ProveedorContableID] [bigint] NOT NULL,
	[ClienteContableID] [bigint] NOT NULL,
	[CodigoMoneda] [varchar](3) NOT NULL,
	[TotalOperacionesGravadas] [decimal](18, 2) NULL,
	[TotalOperacionesInafectas] [decimal](18, 2) NULL,
	[TotalOperacionesExoneradas] [decimal](18, 2) NULL,
	[TotalOperacionesGratuitas] [decimal](18, 2) NULL,
	[BaseImponiblePercepcion] [decimal](18, 2) NULL,
	[TotalPercepciones] [decimal](18, 2) NULL,
	[MontoTotalConPercepcion] [decimal](18, 2) NULL,
	[TotalRetenciones] [decimal](18, 2) NULL,
	[TotalDetracciones] [decimal](18, 2) NULL,
	[TotalBonificaciones] [decimal](18, 2) NULL,
	[TotalDescuentos] [decimal](18, 2) NULL,
	[TipoCambio] [decimal](18, 2) NULL,
	[TotalFISE] [decimal](18, 2) NULL,
	[SubtotalVenta] [decimal](18, 2) NULL,
	[TotalIGV] [decimal](18, 2) NOT NULL,
	[TotalISC] [decimal](18, 2) NULL,
	[TotalOtrosImpuestos] [decimal](18, 2) NULL,
	[TotalOtrosCargos] [decimal](18, 2) NULL,
	[TotalMontoPagar] [decimal](18, 2) NOT NULL,
	[TotalSubvencionado] [decimal](18, 2) NULL,
	[MontoLetras] [varchar](255) NULL,
	[CodigoRptaSUNAT] [varchar](10) NULL,
	[DescripcionRptaSUNAT] [varchar](1000) NULL,
	[estado] [varchar](2) NULL,
	[fechaRefEnvio] [datetime] NULL,
	[CDRRptaSUNAT] [varbinary](max) NULL,
	[usuarioCrea] [varchar](25) NULL,
	[FechaCrea] [datetime] NULL,
	[usuarioUPDiExtract] [varchar](25) NULL,
	[fechaUPDiExtract] [datetime] NULL,
	[UblVersion] [varchar](3) NULL,
	[Perfil] [varchar](4) NULL,
	[NumeroOrden] [varchar](50) NULL,
	[CodigoEstablecimiento] [varchar](50) NULL,
	[DescuentoGlobal] [decimal](18, 2) NULL,
	[DescuentoGlobalBase] [decimal](18, 2) NULL,
	[DescuentoGlobalCodigo] [varchar](4) NULL,
	[TotalImpuestos] [decimal](18, 2) NULL,
	[TotalISCBase] [decimal](18, 2) NULL,
	[TotalOtrosImpuestosBase] [decimal](18, 2) NULL,
	[GuiaRemision] [varchar](25) NULL,
	[DetraccionCodigo] [varchar](4) NULL,
	[DetraccionMedioPago] [varchar](4) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Cliente]    Script Date: 02/06/2020 9:31:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cliente](
	[Idcliente] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](100) NULL,
	[Direccion] [varchar](100) NULL,
	[Correo] [varchar](100) NULL,
	[telefono] [int] NULL,
	[Estado] [varchar](1) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Constantes]    Script Date: 02/06/2020 9:31:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Constantes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](500) NULL,
	[Valor] [nvarchar](max) NULL,
	[visible] [bit] NULL,
	[Nota] [nvarchar](max) NULL,
 CONSTRAINT [PK_Constantes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Invoice_Cab]    Script Date: 02/06/2020 9:31:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Invoice_Cab](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Codigo] [varchar](10) NULL,
	[Nombre] [varchar](100) NULL,
	[fechaentrega] [date] NULL,
	[Direccion] [varchar](100) NULL,
	[fechaterminada] [date] NULL,
	[Correo] [varchar](100) NULL,
	[telefono] [int] NULL,
	[subtotal] [decimal](8, 4) NULL,
	[descuento] [decimal](8, 4) NULL,
	[subtotalmenos] [decimal](8, 4) NULL,
	[igv] [decimal](8, 4) NULL,
	[igvtotal] [decimal](8, 4) NULL,
	[total] [decimal](8, 4) NULL,
	[Estado] [varchar](1) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Invoice_Det]    Script Date: 02/06/2020 9:31:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Invoice_Det](
	[IdDetalle] [int] IDENTITY(1,1) NOT NULL,
	[Id] [int] NULL,
	[Descripcion] [varchar](100) NULL,
	[Cantidad] [int] NULL,
	[PrecioU] [decimal](8, 4) NULL,
	[TotalDEt] [decimal](8, 4) NULL,
	[Estado] [varchar](1) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[llxny_user]    Script Date: 02/06/2020 9:31:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[llxny_user](
	[rowid] [int] IDENTITY(1,1) NOT NULL,
	[entity] [int] NULL,
	[ref_ext] [nvarchar](50) NULL,
	[ref_int] [nvarchar](50) NULL,
	[employee] [tinyint] NULL,
	[fk_establishment] [int] NULL,
	[datec] [datetime2](7) NULL,
	[tms] [datetime2](7) NOT NULL,
	[fk_user_creat] [int] NULL,
	[fk_user_modif] [int] NULL,
	[login] [nvarchar](50) NOT NULL,
	[pass] [nvarchar](128) NULL,
	[pass_crypted] [nvarchar](128) NULL,
	[pass_temp] [nvarchar](128) NULL,
	[api_key] [nvarchar](128) NULL,
	[gender] [nvarchar](10) NULL,
	[civility] [nvarchar](6) NULL,
	[lastname] [nvarchar](50) NULL,
	[firstname] [nvarchar](50) NULL,
	[address] [nvarchar](255) NULL,
	[zip] [nvarchar](25) NULL,
	[town] [nvarchar](50) NULL,
	[fk_state] [int] NULL,
	[fk_country] [int] NULL,
	[job] [nvarchar](128) NULL,
	[skype] [nvarchar](255) NULL,
	[office_phone] [nvarchar](20) NULL,
	[office_fax] [nvarchar](20) NULL,
	[user_mobile] [nvarchar](20) NULL,
	[email] [nvarchar](255) NULL,
	[signature] [nvarchar](max) NULL,
	[admin] [smallint] NULL,
	[module_comm] [smallint] NULL,
	[module_compta] [smallint] NULL,
	[fk_soc] [int] NULL,
	[fk_socpeople] [int] NULL,
	[fk_member] [int] NULL,
	[fk_user] [int] NULL,
	[note_public] [nvarchar](max) NULL,
	[note] [nvarchar](max) NULL,
	[model_pdf] [nvarchar](255) NULL,
	[datelastlogin] [datetime2](7) NULL,
	[datepreviouslogin] [datetime2](7) NULL,
	[egroupware_id] [int] NULL,
	[ldap_sid] [nvarchar](255) NULL,
	[openid] [nvarchar](255) NULL,
	[statut] [char](8) NULL,
	[photo] [nvarchar](255) NULL,
	[lang] [nvarchar](6) NULL,
	[color] [nvarchar](6) NULL,
	[barcode] [nvarchar](255) NULL,
	[fk_barcode_type] [int] NULL,
	[accountancy_code] [nvarchar](32) NULL,
	[nb_holiday] [int] NULL,
	[thm] [real] NULL,
	[tjm] [real] NULL,
	[salary] [real] NULL,
	[salaryextra] [real] NULL,
	[dateemployment] [date] NULL,
	[weeklyhours] [real] NULL,
	[import_key] [nvarchar](14) NULL,
	[TipoUsuario] [int] NULL,
	[idEmpresa] [int] NULL,
	[rucProveedor] [varchar](19) NULL,
 CONSTRAINT [PK_llxny_user] PRIMARY KEY CLUSTERED 
(
	[rowid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Menu]    Script Date: 02/06/2020 9:31:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Menu](
	[MenuId] [int] IDENTITY(1,1) NOT NULL,
	[NombreMenu] [nvarchar](50) NULL,
	[URLPagina] [nvarchar](50) NULL,
	[MenuPadreId] [int] NULL,
	[Comentario] [varchar](50) NULL,
	[IconName] [varchar](20) NULL,
	[EstadoRegistro] [char](3) NOT NULL,
	[numeroOrden] [int] NULL,
 CONSTRAINT [PK_Menu] PRIMARY KEY CLUSTERED 
(
	[MenuId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MultiTabla]    Script Date: 02/06/2020 9:31:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MultiTabla](
	[IdMultiTabla] [int] IDENTITY(1,1) NOT NULL,
	[NombreTabla] [nvarchar](100) NULL,
	[EstadoRegistro] [nvarchar](5) NULL,
 CONSTRAINT [XPKTabla] PRIMARY KEY CLUSTERED 
(
	[IdMultiTabla] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MultiTabla2]    Script Date: 02/06/2020 9:31:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MultiTabla2](
	[IdMultiTabla] [int] IDENTITY(1,1) NOT NULL,
	[NombreTabla] [nvarchar](100) NULL,
	[EstadoRegistro] [nvarchar](5) NULL,
 CONSTRAINT [XPKTabla2] PRIMARY KEY CLUSTERED 
(
	[IdMultiTabla] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MultitablaValor]    Script Date: 02/06/2020 9:31:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MultitablaValor](
	[IdValor] [int] IDENTITY(1,1) NOT NULL,
	[Codigo] [nvarchar](10) NULL,
	[CodigoCorto] [nvarchar](20) NULL,
	[CodigoLargo] [nvarchar](20) NULL,
	[Descripcion] [nvarchar](100) NULL,
	[DescripcionCorta] [nvarchar](50) NULL,
	[DescripcionLarga] [nvarchar](200) NULL,
	[Icono] [nvarchar](50) NULL,
	[IdMultiTabla] [int] NOT NULL,
	[Orden] [int] NULL,
	[EstadoRegistro] [nvarchar](5) NULL,
 CONSTRAINT [XPKValor] PRIMARY KEY CLUSTERED 
(
	[IdValor] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MultitablaValor2]    Script Date: 02/06/2020 9:31:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MultitablaValor2](
	[IdValor] [int] IDENTITY(1,1) NOT NULL,
	[Codigo] [nvarchar](10) NULL,
	[CodigoCorto] [nvarchar](20) NULL,
	[CodigoLargo] [nvarchar](20) NULL,
	[Descripcion] [nvarchar](100) NULL,
	[DescripcionCorta] [nvarchar](50) NULL,
	[DescripcionLarga] [nvarchar](200) NULL,
	[Icono] [nvarchar](50) NULL,
	[IdMultiTabla] [int] NOT NULL,
	[Orden] [int] NULL,
	[EstadoRegistro] [nvarchar](5) NULL,
 CONSTRAINT [XPKValor2] PRIMARY KEY CLUSTERED 
(
	[IdValor] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PaginaFrecuente]    Script Date: 02/06/2020 9:31:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PaginaFrecuente](
	[UsuarioId] [int] NULL,
	[Formulario] [int] NULL,
	[visitas] [int] NULL,
	[Descripcion] [varchar](250) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Permisos]    Script Date: 02/06/2020 9:31:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Permisos](
	[MenuId] [int] NULL,
	[UsuarioId] [int] NULL,
	[PadreId] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Producto]    Script Date: 02/06/2020 9:31:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Producto](
	[IdProducto] [int] IDENTITY(1,1) NOT NULL,
	[Descripcion] [varchar](100) NULL,
	[Precio] [decimal](8, 0) NULL,
	[Estado] [varchar](1) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Token]    Script Date: 02/06/2020 9:31:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Token](
	[GUIDToken] [uniqueidentifier] NULL,
	[CodigoUsuario] [varchar](20) NULL,
	[EstadoRegistro] [char](3) NULL,
	[FechaRegistro] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Ubigeo]    Script Date: 02/06/2020 9:31:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ubigeo](
	[DepartamentoId] [varchar](2) NOT NULL,
	[ProvinciaId] [varchar](2) NOT NULL,
	[DistritoId] [varchar](4) NOT NULL,
	[Nombre] [varchar](100) NULL,
 CONSTRAINT [PK_Ubigeo] PRIMARY KEY CLUSTERED 
(
	[DepartamentoId] ASC,
	[ProvinciaId] ASC,
	[DistritoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Cliente] ON 

INSERT [dbo].[Cliente] ([Idcliente], [Nombre], [Direccion], [Correo], [telefono], [Estado]) VALUES (1, N'Moises Daniel Castillo Cañavi', N'San Jose', N'moisescastillo.dev@gmail.com', 918273645, N'A')
INSERT [dbo].[Cliente] ([Idcliente], [Nombre], [Direccion], [Correo], [telefono], [Estado]) VALUES (2, N'Edgar Condor Castillo', N'S.M.P', N'eddccperu@gmail.com', 978645312, N'A')
SET IDENTITY_INSERT [dbo].[Cliente] OFF
SET IDENTITY_INSERT [dbo].[Constantes] ON 

INSERT [dbo].[Constantes] ([Id], [Nombre], [Valor], [visible], [Nota]) VALUES (1, N'MAIN_NAME', N'Evaluacion Salud', 1, NULL)
INSERT [dbo].[Constantes] ([Id], [Nombre], [Valor], [visible], [Nota]) VALUES (2, N'MAIN_HEADER_AREA_PRINCIPAL_BC', N'#ffffff', 1, NULL)
INSERT [dbo].[Constantes] ([Id], [Nombre], [Valor], [visible], [Nota]) VALUES (3, N'MAIN_HEADER_TEXT_NAME_EMPRESA_C', N'#333333', 1, NULL)
INSERT [dbo].[Constantes] ([Id], [Nombre], [Valor], [visible], [Nota]) VALUES (4, N'MAIN_HEADER_TEXT_C', N'#575757', 1, NULL)
INSERT [dbo].[Constantes] ([Id], [Nombre], [Valor], [visible], [Nota]) VALUES (5, N'MAIN_MENU_HEADER_AREA_PRINCIPAL_BC', N'#2d2d32', 1, NULL)
INSERT [dbo].[Constantes] ([Id], [Nombre], [Valor], [visible], [Nota]) VALUES (6, N'MAIN_MENU_HEADER_NAME_EMPRESA_C', N'#ffffff', 1, NULL)
INSERT [dbo].[Constantes] ([Id], [Nombre], [Valor], [visible], [Nota]) VALUES (7, N'MAIN_MENU_AREA_BC', N'#323237', 1, NULL)
INSERT [dbo].[Constantes] ([Id], [Nombre], [Valor], [visible], [Nota]) VALUES (8, N'MAIN_MENU_TEXT_ICON_C', N'#85858e', 1, NULL)
INSERT [dbo].[Constantes] ([Id], [Nombre], [Valor], [visible], [Nota]) VALUES (9, N'MAIN_MENU_SUBMENU_AREA_BC', N'#27272a', 1, NULL)
INSERT [dbo].[Constantes] ([Id], [Nombre], [Valor], [visible], [Nota]) VALUES (10, N'MAIN_MODAL_HEADER_AREA_BC', N'#ffffff', 1, NULL)
INSERT [dbo].[Constantes] ([Id], [Nombre], [Valor], [visible], [Nota]) VALUES (11, N'MAIN_MODAL_HEADER_TEXT_C', N'#76838f', 1, NULL)
INSERT [dbo].[Constantes] ([Id], [Nombre], [Valor], [visible], [Nota]) VALUES (12, N'MAIN_MODAL_FOOTER_AREA_BC', N'#ffffff', 1, NULL)
SET IDENTITY_INSERT [dbo].[Constantes] OFF
SET IDENTITY_INSERT [dbo].[llxny_user] ON 

INSERT [dbo].[llxny_user] ([rowid], [entity], [ref_ext], [ref_int], [employee], [fk_establishment], [datec], [tms], [fk_user_creat], [fk_user_modif], [login], [pass], [pass_crypted], [pass_temp], [api_key], [gender], [civility], [lastname], [firstname], [address], [zip], [town], [fk_state], [fk_country], [job], [skype], [office_phone], [office_fax], [user_mobile], [email], [signature], [admin], [module_comm], [module_compta], [fk_soc], [fk_socpeople], [fk_member], [fk_user], [note_public], [note], [model_pdf], [datelastlogin], [datepreviouslogin], [egroupware_id], [ldap_sid], [openid], [statut], [photo], [lang], [color], [barcode], [fk_barcode_type], [accountancy_code], [nb_holiday], [thm], [tjm], [salary], [salaryextra], [dateemployment], [weeklyhours], [import_key], [TipoUsuario], [idEmpresa], [rucProveedor]) VALUES (1, 0, NULL, NULL, 1, 0, CAST(N'2018-01-22T14:26:55.0000000' AS DateTime2), CAST(N'2018-01-22T14:26:55.0000000' AS DateTime2), NULL, NULL, N'mpuma', N'123654Ab', N'3132333635344162', NULL, NULL, N'', NULL, N'Pumahuacre Mendoza', N'Manuel', N'', N'', N'', NULL, NULL, N'', N'', N'', N'', N'', N'manuelpumahuacre@gmail.com', N'', 1, 1, 1, NULL, NULL, NULL, NULL, NULL, N'', NULL, CAST(N'2018-02-24T10:07:56.0000000' AS DateTime2), CAST(N'2018-02-22T13:15:52.0000000' AS DateTime2), NULL, N'', NULL, N'REG     ', NULL, NULL, N'', NULL, 0, N'', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL)
INSERT [dbo].[llxny_user] ([rowid], [entity], [ref_ext], [ref_int], [employee], [fk_establishment], [datec], [tms], [fk_user_creat], [fk_user_modif], [login], [pass], [pass_crypted], [pass_temp], [api_key], [gender], [civility], [lastname], [firstname], [address], [zip], [town], [fk_state], [fk_country], [job], [skype], [office_phone], [office_fax], [user_mobile], [email], [signature], [admin], [module_comm], [module_compta], [fk_soc], [fk_socpeople], [fk_member], [fk_user], [note_public], [note], [model_pdf], [datelastlogin], [datepreviouslogin], [egroupware_id], [ldap_sid], [openid], [statut], [photo], [lang], [color], [barcode], [fk_barcode_type], [accountancy_code], [nb_holiday], [thm], [tjm], [salary], [salaryextra], [dateemployment], [weeklyhours], [import_key], [TipoUsuario], [idEmpresa], [rucProveedor]) VALUES (2, 0, NULL, NULL, NULL, NULL, NULL, CAST(N'2020-01-02T16:56:49.2533333' AS DateTime2), NULL, NULL, N'soporte', N'100', N'313030', NULL, NULL, NULL, NULL, N'soporte', N'soporte', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'soporte@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'REG     ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL)
INSERT [dbo].[llxny_user] ([rowid], [entity], [ref_ext], [ref_int], [employee], [fk_establishment], [datec], [tms], [fk_user_creat], [fk_user_modif], [login], [pass], [pass_crypted], [pass_temp], [api_key], [gender], [civility], [lastname], [firstname], [address], [zip], [town], [fk_state], [fk_country], [job], [skype], [office_phone], [office_fax], [user_mobile], [email], [signature], [admin], [module_comm], [module_compta], [fk_soc], [fk_socpeople], [fk_member], [fk_user], [note_public], [note], [model_pdf], [datelastlogin], [datepreviouslogin], [egroupware_id], [ldap_sid], [openid], [statut], [photo], [lang], [color], [barcode], [fk_barcode_type], [accountancy_code], [nb_holiday], [thm], [tjm], [salary], [salaryextra], [dateemployment], [weeklyhours], [import_key], [TipoUsuario], [idEmpresa], [rucProveedor]) VALUES (3, 0, NULL, NULL, NULL, NULL, NULL, CAST(N'2020-01-02T16:58:10.1933333' AS DateTime2), NULL, NULL, N'soporte1', N'100', N'313030', NULL, NULL, NULL, NULL, N'soporte1', N'soporte1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'soporte1@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'REG     ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL)
INSERT [dbo].[llxny_user] ([rowid], [entity], [ref_ext], [ref_int], [employee], [fk_establishment], [datec], [tms], [fk_user_creat], [fk_user_modif], [login], [pass], [pass_crypted], [pass_temp], [api_key], [gender], [civility], [lastname], [firstname], [address], [zip], [town], [fk_state], [fk_country], [job], [skype], [office_phone], [office_fax], [user_mobile], [email], [signature], [admin], [module_comm], [module_compta], [fk_soc], [fk_socpeople], [fk_member], [fk_user], [note_public], [note], [model_pdf], [datelastlogin], [datepreviouslogin], [egroupware_id], [ldap_sid], [openid], [statut], [photo], [lang], [color], [barcode], [fk_barcode_type], [accountancy_code], [nb_holiday], [thm], [tjm], [salary], [salaryextra], [dateemployment], [weeklyhours], [import_key], [TipoUsuario], [idEmpresa], [rucProveedor]) VALUES (4, 0, NULL, NULL, NULL, NULL, NULL, CAST(N'2020-01-02T18:02:29.1700000' AS DateTime2), NULL, NULL, N'soporte2', N'100', N'313030', NULL, NULL, NULL, NULL, N'soporte2', N'soporte2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'soporte2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'REG     ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL)
INSERT [dbo].[llxny_user] ([rowid], [entity], [ref_ext], [ref_int], [employee], [fk_establishment], [datec], [tms], [fk_user_creat], [fk_user_modif], [login], [pass], [pass_crypted], [pass_temp], [api_key], [gender], [civility], [lastname], [firstname], [address], [zip], [town], [fk_state], [fk_country], [job], [skype], [office_phone], [office_fax], [user_mobile], [email], [signature], [admin], [module_comm], [module_compta], [fk_soc], [fk_socpeople], [fk_member], [fk_user], [note_public], [note], [model_pdf], [datelastlogin], [datepreviouslogin], [egroupware_id], [ldap_sid], [openid], [statut], [photo], [lang], [color], [barcode], [fk_barcode_type], [accountancy_code], [nb_holiday], [thm], [tjm], [salary], [salaryextra], [dateemployment], [weeklyhours], [import_key], [TipoUsuario], [idEmpresa], [rucProveedor]) VALUES (5, 0, NULL, NULL, NULL, NULL, NULL, CAST(N'2020-01-02T18:03:43.3400000' AS DateTime2), NULL, NULL, N'soporte3', N'soporte3', N'736f706f72746533', NULL, NULL, NULL, NULL, N'soporte3', N'soporte3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'soporte3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'REG     ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL)
INSERT [dbo].[llxny_user] ([rowid], [entity], [ref_ext], [ref_int], [employee], [fk_establishment], [datec], [tms], [fk_user_creat], [fk_user_modif], [login], [pass], [pass_crypted], [pass_temp], [api_key], [gender], [civility], [lastname], [firstname], [address], [zip], [town], [fk_state], [fk_country], [job], [skype], [office_phone], [office_fax], [user_mobile], [email], [signature], [admin], [module_comm], [module_compta], [fk_soc], [fk_socpeople], [fk_member], [fk_user], [note_public], [note], [model_pdf], [datelastlogin], [datepreviouslogin], [egroupware_id], [ldap_sid], [openid], [statut], [photo], [lang], [color], [barcode], [fk_barcode_type], [accountancy_code], [nb_holiday], [thm], [tjm], [salary], [salaryextra], [dateemployment], [weeklyhours], [import_key], [TipoUsuario], [idEmpresa], [rucProveedor]) VALUES (6, 0, NULL, NULL, NULL, NULL, NULL, CAST(N'2020-01-02T18:05:58.6333333' AS DateTime2), NULL, NULL, N'soporte4', N'100', N'313030', NULL, NULL, NULL, NULL, N'soporte4', N'soporte4', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'soporte4', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'REG     ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL)
INSERT [dbo].[llxny_user] ([rowid], [entity], [ref_ext], [ref_int], [employee], [fk_establishment], [datec], [tms], [fk_user_creat], [fk_user_modif], [login], [pass], [pass_crypted], [pass_temp], [api_key], [gender], [civility], [lastname], [firstname], [address], [zip], [town], [fk_state], [fk_country], [job], [skype], [office_phone], [office_fax], [user_mobile], [email], [signature], [admin], [module_comm], [module_compta], [fk_soc], [fk_socpeople], [fk_member], [fk_user], [note_public], [note], [model_pdf], [datelastlogin], [datepreviouslogin], [egroupware_id], [ldap_sid], [openid], [statut], [photo], [lang], [color], [barcode], [fk_barcode_type], [accountancy_code], [nb_holiday], [thm], [tjm], [salary], [salaryextra], [dateemployment], [weeklyhours], [import_key], [TipoUsuario], [idEmpresa], [rucProveedor]) VALUES (7, 0, NULL, NULL, NULL, NULL, NULL, CAST(N'2020-01-02T18:08:13.9200000' AS DateTime2), NULL, NULL, N'soporte5', N'soporte5', N'736f706f72746535', NULL, NULL, NULL, NULL, N'soporte5', N'soporte5', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'soporte5', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'REG     ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL)
INSERT [dbo].[llxny_user] ([rowid], [entity], [ref_ext], [ref_int], [employee], [fk_establishment], [datec], [tms], [fk_user_creat], [fk_user_modif], [login], [pass], [pass_crypted], [pass_temp], [api_key], [gender], [civility], [lastname], [firstname], [address], [zip], [town], [fk_state], [fk_country], [job], [skype], [office_phone], [office_fax], [user_mobile], [email], [signature], [admin], [module_comm], [module_compta], [fk_soc], [fk_socpeople], [fk_member], [fk_user], [note_public], [note], [model_pdf], [datelastlogin], [datepreviouslogin], [egroupware_id], [ldap_sid], [openid], [statut], [photo], [lang], [color], [barcode], [fk_barcode_type], [accountancy_code], [nb_holiday], [thm], [tjm], [salary], [salaryextra], [dateemployment], [weeklyhours], [import_key], [TipoUsuario], [idEmpresa], [rucProveedor]) VALUES (8, 0, NULL, NULL, NULL, NULL, NULL, CAST(N'2020-01-02T18:13:43.0800000' AS DateTime2), NULL, NULL, N'soporte6', N'100', N'313030', NULL, NULL, NULL, NULL, N'soporte6', N'soporte6', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'soporte6', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'REG     ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL)
INSERT [dbo].[llxny_user] ([rowid], [entity], [ref_ext], [ref_int], [employee], [fk_establishment], [datec], [tms], [fk_user_creat], [fk_user_modif], [login], [pass], [pass_crypted], [pass_temp], [api_key], [gender], [civility], [lastname], [firstname], [address], [zip], [town], [fk_state], [fk_country], [job], [skype], [office_phone], [office_fax], [user_mobile], [email], [signature], [admin], [module_comm], [module_compta], [fk_soc], [fk_socpeople], [fk_member], [fk_user], [note_public], [note], [model_pdf], [datelastlogin], [datepreviouslogin], [egroupware_id], [ldap_sid], [openid], [statut], [photo], [lang], [color], [barcode], [fk_barcode_type], [accountancy_code], [nb_holiday], [thm], [tjm], [salary], [salaryextra], [dateemployment], [weeklyhours], [import_key], [TipoUsuario], [idEmpresa], [rucProveedor]) VALUES (9, 0, NULL, NULL, NULL, NULL, NULL, CAST(N'2020-01-02T21:48:46.6333333' AS DateTime2), NULL, NULL, N'soporte7', N'soporte7', N'736f706f72746537', NULL, NULL, NULL, NULL, N'soporte7', N'soporte7', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'soporte7', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'REG     ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL)
INSERT [dbo].[llxny_user] ([rowid], [entity], [ref_ext], [ref_int], [employee], [fk_establishment], [datec], [tms], [fk_user_creat], [fk_user_modif], [login], [pass], [pass_crypted], [pass_temp], [api_key], [gender], [civility], [lastname], [firstname], [address], [zip], [town], [fk_state], [fk_country], [job], [skype], [office_phone], [office_fax], [user_mobile], [email], [signature], [admin], [module_comm], [module_compta], [fk_soc], [fk_socpeople], [fk_member], [fk_user], [note_public], [note], [model_pdf], [datelastlogin], [datepreviouslogin], [egroupware_id], [ldap_sid], [openid], [statut], [photo], [lang], [color], [barcode], [fk_barcode_type], [accountancy_code], [nb_holiday], [thm], [tjm], [salary], [salaryextra], [dateemployment], [weeklyhours], [import_key], [TipoUsuario], [idEmpresa], [rucProveedor]) VALUES (10, 0, NULL, NULL, NULL, NULL, NULL, CAST(N'2020-01-02T21:51:46.3866667' AS DateTime2), NULL, NULL, N'soporte8', N'soporte8', N'736f706f72746538', NULL, NULL, NULL, NULL, N'soporte8', N'soporte8', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'soporte8', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'REG     ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL)
INSERT [dbo].[llxny_user] ([rowid], [entity], [ref_ext], [ref_int], [employee], [fk_establishment], [datec], [tms], [fk_user_creat], [fk_user_modif], [login], [pass], [pass_crypted], [pass_temp], [api_key], [gender], [civility], [lastname], [firstname], [address], [zip], [town], [fk_state], [fk_country], [job], [skype], [office_phone], [office_fax], [user_mobile], [email], [signature], [admin], [module_comm], [module_compta], [fk_soc], [fk_socpeople], [fk_member], [fk_user], [note_public], [note], [model_pdf], [datelastlogin], [datepreviouslogin], [egroupware_id], [ldap_sid], [openid], [statut], [photo], [lang], [color], [barcode], [fk_barcode_type], [accountancy_code], [nb_holiday], [thm], [tjm], [salary], [salaryextra], [dateemployment], [weeklyhours], [import_key], [TipoUsuario], [idEmpresa], [rucProveedor]) VALUES (11, 0, NULL, NULL, NULL, NULL, NULL, CAST(N'2020-01-02T21:52:58.2600000' AS DateTime2), NULL, NULL, N'soporte8', N'soporte8', N'736f706f72746538', NULL, NULL, NULL, NULL, N'soporte8', N'soporte8', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'soporte8', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'REG     ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL)
INSERT [dbo].[llxny_user] ([rowid], [entity], [ref_ext], [ref_int], [employee], [fk_establishment], [datec], [tms], [fk_user_creat], [fk_user_modif], [login], [pass], [pass_crypted], [pass_temp], [api_key], [gender], [civility], [lastname], [firstname], [address], [zip], [town], [fk_state], [fk_country], [job], [skype], [office_phone], [office_fax], [user_mobile], [email], [signature], [admin], [module_comm], [module_compta], [fk_soc], [fk_socpeople], [fk_member], [fk_user], [note_public], [note], [model_pdf], [datelastlogin], [datepreviouslogin], [egroupware_id], [ldap_sid], [openid], [statut], [photo], [lang], [color], [barcode], [fk_barcode_type], [accountancy_code], [nb_holiday], [thm], [tjm], [salary], [salaryextra], [dateemployment], [weeklyhours], [import_key], [TipoUsuario], [idEmpresa], [rucProveedor]) VALUES (12, 0, NULL, NULL, NULL, NULL, NULL, CAST(N'2020-01-02T21:54:20.4000000' AS DateTime2), NULL, NULL, N'soporte9', N'soporte9', N'736f706f72746539', NULL, NULL, NULL, NULL, N'soporte9', N'soporte9', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'soporte9', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'REG     ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL)
INSERT [dbo].[llxny_user] ([rowid], [entity], [ref_ext], [ref_int], [employee], [fk_establishment], [datec], [tms], [fk_user_creat], [fk_user_modif], [login], [pass], [pass_crypted], [pass_temp], [api_key], [gender], [civility], [lastname], [firstname], [address], [zip], [town], [fk_state], [fk_country], [job], [skype], [office_phone], [office_fax], [user_mobile], [email], [signature], [admin], [module_comm], [module_compta], [fk_soc], [fk_socpeople], [fk_member], [fk_user], [note_public], [note], [model_pdf], [datelastlogin], [datepreviouslogin], [egroupware_id], [ldap_sid], [openid], [statut], [photo], [lang], [color], [barcode], [fk_barcode_type], [accountancy_code], [nb_holiday], [thm], [tjm], [salary], [salaryextra], [dateemployment], [weeklyhours], [import_key], [TipoUsuario], [idEmpresa], [rucProveedor]) VALUES (13, 0, NULL, NULL, NULL, NULL, NULL, CAST(N'2020-01-02T22:49:26.2233333' AS DateTime2), NULL, NULL, N'soporte10', N'soporte10', N'736f706f7274653130', NULL, NULL, NULL, NULL, N'soporte10', N'soporte10', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'soporte10', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'REG     ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL)
INSERT [dbo].[llxny_user] ([rowid], [entity], [ref_ext], [ref_int], [employee], [fk_establishment], [datec], [tms], [fk_user_creat], [fk_user_modif], [login], [pass], [pass_crypted], [pass_temp], [api_key], [gender], [civility], [lastname], [firstname], [address], [zip], [town], [fk_state], [fk_country], [job], [skype], [office_phone], [office_fax], [user_mobile], [email], [signature], [admin], [module_comm], [module_compta], [fk_soc], [fk_socpeople], [fk_member], [fk_user], [note_public], [note], [model_pdf], [datelastlogin], [datepreviouslogin], [egroupware_id], [ldap_sid], [openid], [statut], [photo], [lang], [color], [barcode], [fk_barcode_type], [accountancy_code], [nb_holiday], [thm], [tjm], [salary], [salaryextra], [dateemployment], [weeklyhours], [import_key], [TipoUsuario], [idEmpresa], [rucProveedor]) VALUES (14, 0, NULL, NULL, NULL, NULL, NULL, CAST(N'2020-01-02T22:51:17.9700000' AS DateTime2), NULL, NULL, N'soporte11', N'soporte11', N'736f706f7274653131', NULL, NULL, NULL, NULL, N'soporte11', N'soporte11', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'soporte11', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'REG     ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL)
INSERT [dbo].[llxny_user] ([rowid], [entity], [ref_ext], [ref_int], [employee], [fk_establishment], [datec], [tms], [fk_user_creat], [fk_user_modif], [login], [pass], [pass_crypted], [pass_temp], [api_key], [gender], [civility], [lastname], [firstname], [address], [zip], [town], [fk_state], [fk_country], [job], [skype], [office_phone], [office_fax], [user_mobile], [email], [signature], [admin], [module_comm], [module_compta], [fk_soc], [fk_socpeople], [fk_member], [fk_user], [note_public], [note], [model_pdf], [datelastlogin], [datepreviouslogin], [egroupware_id], [ldap_sid], [openid], [statut], [photo], [lang], [color], [barcode], [fk_barcode_type], [accountancy_code], [nb_holiday], [thm], [tjm], [salary], [salaryextra], [dateemployment], [weeklyhours], [import_key], [TipoUsuario], [idEmpresa], [rucProveedor]) VALUES (15, 0, NULL, NULL, NULL, NULL, NULL, CAST(N'2020-01-02T23:09:44.3166667' AS DateTime2), NULL, NULL, N'soporte12', N'soporte12', N'736f706f7274653132', NULL, NULL, NULL, NULL, N'soporte12', N'soporte12', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'soporte12', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'REG     ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL)
INSERT [dbo].[llxny_user] ([rowid], [entity], [ref_ext], [ref_int], [employee], [fk_establishment], [datec], [tms], [fk_user_creat], [fk_user_modif], [login], [pass], [pass_crypted], [pass_temp], [api_key], [gender], [civility], [lastname], [firstname], [address], [zip], [town], [fk_state], [fk_country], [job], [skype], [office_phone], [office_fax], [user_mobile], [email], [signature], [admin], [module_comm], [module_compta], [fk_soc], [fk_socpeople], [fk_member], [fk_user], [note_public], [note], [model_pdf], [datelastlogin], [datepreviouslogin], [egroupware_id], [ldap_sid], [openid], [statut], [photo], [lang], [color], [barcode], [fk_barcode_type], [accountancy_code], [nb_holiday], [thm], [tjm], [salary], [salaryextra], [dateemployment], [weeklyhours], [import_key], [TipoUsuario], [idEmpresa], [rucProveedor]) VALUES (16, 0, NULL, NULL, NULL, NULL, NULL, CAST(N'2020-01-02T23:12:37.9733333' AS DateTime2), NULL, NULL, N'soporte13', N'soporte13', N'736f706f7274653133', NULL, NULL, NULL, NULL, N'soporte13', N'soporte13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'soporte13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'REG     ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL)
INSERT [dbo].[llxny_user] ([rowid], [entity], [ref_ext], [ref_int], [employee], [fk_establishment], [datec], [tms], [fk_user_creat], [fk_user_modif], [login], [pass], [pass_crypted], [pass_temp], [api_key], [gender], [civility], [lastname], [firstname], [address], [zip], [town], [fk_state], [fk_country], [job], [skype], [office_phone], [office_fax], [user_mobile], [email], [signature], [admin], [module_comm], [module_compta], [fk_soc], [fk_socpeople], [fk_member], [fk_user], [note_public], [note], [model_pdf], [datelastlogin], [datepreviouslogin], [egroupware_id], [ldap_sid], [openid], [statut], [photo], [lang], [color], [barcode], [fk_barcode_type], [accountancy_code], [nb_holiday], [thm], [tjm], [salary], [salaryextra], [dateemployment], [weeklyhours], [import_key], [TipoUsuario], [idEmpresa], [rucProveedor]) VALUES (17, 0, NULL, NULL, NULL, NULL, NULL, CAST(N'2020-01-02T23:17:16.1200000' AS DateTime2), NULL, NULL, N'soporte14', N'soporte14', N'736f706f7274653134', NULL, NULL, NULL, NULL, N'soporte14', N'soporte14', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'soporte14', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'REG     ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL)
INSERT [dbo].[llxny_user] ([rowid], [entity], [ref_ext], [ref_int], [employee], [fk_establishment], [datec], [tms], [fk_user_creat], [fk_user_modif], [login], [pass], [pass_crypted], [pass_temp], [api_key], [gender], [civility], [lastname], [firstname], [address], [zip], [town], [fk_state], [fk_country], [job], [skype], [office_phone], [office_fax], [user_mobile], [email], [signature], [admin], [module_comm], [module_compta], [fk_soc], [fk_socpeople], [fk_member], [fk_user], [note_public], [note], [model_pdf], [datelastlogin], [datepreviouslogin], [egroupware_id], [ldap_sid], [openid], [statut], [photo], [lang], [color], [barcode], [fk_barcode_type], [accountancy_code], [nb_holiday], [thm], [tjm], [salary], [salaryextra], [dateemployment], [weeklyhours], [import_key], [TipoUsuario], [idEmpresa], [rucProveedor]) VALUES (18, 0, NULL, NULL, NULL, NULL, NULL, CAST(N'2020-01-02T23:17:32.6733333' AS DateTime2), NULL, NULL, N'soporte15', N'soporte15', N'736f706f7274653135', NULL, NULL, NULL, NULL, N'soporte15', N'soporte15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'soporte15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'REG     ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL)
INSERT [dbo].[llxny_user] ([rowid], [entity], [ref_ext], [ref_int], [employee], [fk_establishment], [datec], [tms], [fk_user_creat], [fk_user_modif], [login], [pass], [pass_crypted], [pass_temp], [api_key], [gender], [civility], [lastname], [firstname], [address], [zip], [town], [fk_state], [fk_country], [job], [skype], [office_phone], [office_fax], [user_mobile], [email], [signature], [admin], [module_comm], [module_compta], [fk_soc], [fk_socpeople], [fk_member], [fk_user], [note_public], [note], [model_pdf], [datelastlogin], [datepreviouslogin], [egroupware_id], [ldap_sid], [openid], [statut], [photo], [lang], [color], [barcode], [fk_barcode_type], [accountancy_code], [nb_holiday], [thm], [tjm], [salary], [salaryextra], [dateemployment], [weeklyhours], [import_key], [TipoUsuario], [idEmpresa], [rucProveedor]) VALUES (19, 0, NULL, NULL, NULL, NULL, NULL, CAST(N'2020-01-02T23:37:50.7633333' AS DateTime2), NULL, NULL, N'soporte16', N'soporte16', N'736f706f7274653136', NULL, NULL, NULL, NULL, N'soporte16', N'soporte16', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'soporte16', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'REG     ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL)
INSERT [dbo].[llxny_user] ([rowid], [entity], [ref_ext], [ref_int], [employee], [fk_establishment], [datec], [tms], [fk_user_creat], [fk_user_modif], [login], [pass], [pass_crypted], [pass_temp], [api_key], [gender], [civility], [lastname], [firstname], [address], [zip], [town], [fk_state], [fk_country], [job], [skype], [office_phone], [office_fax], [user_mobile], [email], [signature], [admin], [module_comm], [module_compta], [fk_soc], [fk_socpeople], [fk_member], [fk_user], [note_public], [note], [model_pdf], [datelastlogin], [datepreviouslogin], [egroupware_id], [ldap_sid], [openid], [statut], [photo], [lang], [color], [barcode], [fk_barcode_type], [accountancy_code], [nb_holiday], [thm], [tjm], [salary], [salaryextra], [dateemployment], [weeklyhours], [import_key], [TipoUsuario], [idEmpresa], [rucProveedor]) VALUES (20, 0, NULL, NULL, NULL, NULL, NULL, CAST(N'2020-01-04T17:39:23.2000000' AS DateTime2), NULL, NULL, N'soporte17', N'100', N'313030', NULL, NULL, NULL, NULL, N'soporte17', N'soporte17', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'soporte17', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'REG     ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL)
INSERT [dbo].[llxny_user] ([rowid], [entity], [ref_ext], [ref_int], [employee], [fk_establishment], [datec], [tms], [fk_user_creat], [fk_user_modif], [login], [pass], [pass_crypted], [pass_temp], [api_key], [gender], [civility], [lastname], [firstname], [address], [zip], [town], [fk_state], [fk_country], [job], [skype], [office_phone], [office_fax], [user_mobile], [email], [signature], [admin], [module_comm], [module_compta], [fk_soc], [fk_socpeople], [fk_member], [fk_user], [note_public], [note], [model_pdf], [datelastlogin], [datepreviouslogin], [egroupware_id], [ldap_sid], [openid], [statut], [photo], [lang], [color], [barcode], [fk_barcode_type], [accountancy_code], [nb_holiday], [thm], [tjm], [salary], [salaryextra], [dateemployment], [weeklyhours], [import_key], [TipoUsuario], [idEmpresa], [rucProveedor]) VALUES (21, 0, NULL, NULL, NULL, NULL, NULL, CAST(N'2020-01-04T17:40:16.3666667' AS DateTime2), NULL, NULL, N'soporte18', N'soporte18', N'736f706f7274653138', NULL, NULL, NULL, NULL, N'soporte18', N'soporte18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'soporte18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'REG     ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL)
INSERT [dbo].[llxny_user] ([rowid], [entity], [ref_ext], [ref_int], [employee], [fk_establishment], [datec], [tms], [fk_user_creat], [fk_user_modif], [login], [pass], [pass_crypted], [pass_temp], [api_key], [gender], [civility], [lastname], [firstname], [address], [zip], [town], [fk_state], [fk_country], [job], [skype], [office_phone], [office_fax], [user_mobile], [email], [signature], [admin], [module_comm], [module_compta], [fk_soc], [fk_socpeople], [fk_member], [fk_user], [note_public], [note], [model_pdf], [datelastlogin], [datepreviouslogin], [egroupware_id], [ldap_sid], [openid], [statut], [photo], [lang], [color], [barcode], [fk_barcode_type], [accountancy_code], [nb_holiday], [thm], [tjm], [salary], [salaryextra], [dateemployment], [weeklyhours], [import_key], [TipoUsuario], [idEmpresa], [rucProveedor]) VALUES (22, 0, NULL, NULL, NULL, NULL, NULL, CAST(N'2020-01-04T17:52:08.9666667' AS DateTime2), NULL, NULL, N'soporte19', N'soporte19', N'736f706f7274653139', NULL, NULL, NULL, NULL, N'soporte19', N'soporte19', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'soporte19', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'REG     ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL)
INSERT [dbo].[llxny_user] ([rowid], [entity], [ref_ext], [ref_int], [employee], [fk_establishment], [datec], [tms], [fk_user_creat], [fk_user_modif], [login], [pass], [pass_crypted], [pass_temp], [api_key], [gender], [civility], [lastname], [firstname], [address], [zip], [town], [fk_state], [fk_country], [job], [skype], [office_phone], [office_fax], [user_mobile], [email], [signature], [admin], [module_comm], [module_compta], [fk_soc], [fk_socpeople], [fk_member], [fk_user], [note_public], [note], [model_pdf], [datelastlogin], [datepreviouslogin], [egroupware_id], [ldap_sid], [openid], [statut], [photo], [lang], [color], [barcode], [fk_barcode_type], [accountancy_code], [nb_holiday], [thm], [tjm], [salary], [salaryextra], [dateemployment], [weeklyhours], [import_key], [TipoUsuario], [idEmpresa], [rucProveedor]) VALUES (23, 0, NULL, NULL, NULL, NULL, NULL, CAST(N'2020-01-04T17:52:09.0000000' AS DateTime2), NULL, NULL, N'soporte20', N'100', N'313030', NULL, NULL, NULL, NULL, N'soporte20', N'soporte20', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'soporte20', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'REG     ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL)
INSERT [dbo].[llxny_user] ([rowid], [entity], [ref_ext], [ref_int], [employee], [fk_establishment], [datec], [tms], [fk_user_creat], [fk_user_modif], [login], [pass], [pass_crypted], [pass_temp], [api_key], [gender], [civility], [lastname], [firstname], [address], [zip], [town], [fk_state], [fk_country], [job], [skype], [office_phone], [office_fax], [user_mobile], [email], [signature], [admin], [module_comm], [module_compta], [fk_soc], [fk_socpeople], [fk_member], [fk_user], [note_public], [note], [model_pdf], [datelastlogin], [datepreviouslogin], [egroupware_id], [ldap_sid], [openid], [statut], [photo], [lang], [color], [barcode], [fk_barcode_type], [accountancy_code], [nb_holiday], [thm], [tjm], [salary], [salaryextra], [dateemployment], [weeklyhours], [import_key], [TipoUsuario], [idEmpresa], [rucProveedor]) VALUES (24, 0, NULL, NULL, NULL, NULL, NULL, CAST(N'2020-01-04T17:53:20.0033333' AS DateTime2), NULL, NULL, N'soporte22', N'soporte22', N'736f706f7274653232', NULL, NULL, NULL, NULL, N'soporte22', N'soporte22', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'soporte22', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'REG     ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL)
INSERT [dbo].[llxny_user] ([rowid], [entity], [ref_ext], [ref_int], [employee], [fk_establishment], [datec], [tms], [fk_user_creat], [fk_user_modif], [login], [pass], [pass_crypted], [pass_temp], [api_key], [gender], [civility], [lastname], [firstname], [address], [zip], [town], [fk_state], [fk_country], [job], [skype], [office_phone], [office_fax], [user_mobile], [email], [signature], [admin], [module_comm], [module_compta], [fk_soc], [fk_socpeople], [fk_member], [fk_user], [note_public], [note], [model_pdf], [datelastlogin], [datepreviouslogin], [egroupware_id], [ldap_sid], [openid], [statut], [photo], [lang], [color], [barcode], [fk_barcode_type], [accountancy_code], [nb_holiday], [thm], [tjm], [salary], [salaryextra], [dateemployment], [weeklyhours], [import_key], [TipoUsuario], [idEmpresa], [rucProveedor]) VALUES (25, 0, NULL, NULL, NULL, NULL, NULL, CAST(N'2020-01-04T17:53:20.0600000' AS DateTime2), NULL, NULL, N'soporte21', N'soporte21', N'736f706f7274653231', NULL, NULL, NULL, NULL, N'soporte21', N'soporte21', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'soporte21', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'REG     ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL)
INSERT [dbo].[llxny_user] ([rowid], [entity], [ref_ext], [ref_int], [employee], [fk_establishment], [datec], [tms], [fk_user_creat], [fk_user_modif], [login], [pass], [pass_crypted], [pass_temp], [api_key], [gender], [civility], [lastname], [firstname], [address], [zip], [town], [fk_state], [fk_country], [job], [skype], [office_phone], [office_fax], [user_mobile], [email], [signature], [admin], [module_comm], [module_compta], [fk_soc], [fk_socpeople], [fk_member], [fk_user], [note_public], [note], [model_pdf], [datelastlogin], [datepreviouslogin], [egroupware_id], [ldap_sid], [openid], [statut], [photo], [lang], [color], [barcode], [fk_barcode_type], [accountancy_code], [nb_holiday], [thm], [tjm], [salary], [salaryextra], [dateemployment], [weeklyhours], [import_key], [TipoUsuario], [idEmpresa], [rucProveedor]) VALUES (26, 0, NULL, NULL, NULL, NULL, NULL, CAST(N'2020-01-04T17:55:17.9500000' AS DateTime2), NULL, NULL, N'soporte23', N'soporte23', N'736f706f7274653233', NULL, NULL, NULL, NULL, N'soporte23', N'soporte23', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'soporte23', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'REG     ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL)
INSERT [dbo].[llxny_user] ([rowid], [entity], [ref_ext], [ref_int], [employee], [fk_establishment], [datec], [tms], [fk_user_creat], [fk_user_modif], [login], [pass], [pass_crypted], [pass_temp], [api_key], [gender], [civility], [lastname], [firstname], [address], [zip], [town], [fk_state], [fk_country], [job], [skype], [office_phone], [office_fax], [user_mobile], [email], [signature], [admin], [module_comm], [module_compta], [fk_soc], [fk_socpeople], [fk_member], [fk_user], [note_public], [note], [model_pdf], [datelastlogin], [datepreviouslogin], [egroupware_id], [ldap_sid], [openid], [statut], [photo], [lang], [color], [barcode], [fk_barcode_type], [accountancy_code], [nb_holiday], [thm], [tjm], [salary], [salaryextra], [dateemployment], [weeklyhours], [import_key], [TipoUsuario], [idEmpresa], [rucProveedor]) VALUES (27, 0, NULL, NULL, NULL, NULL, NULL, CAST(N'2020-01-04T17:55:18.0800000' AS DateTime2), NULL, NULL, N'soporte24', N'soporte24', N'736f706f7274653234', NULL, NULL, NULL, NULL, N'soporte24', N'soporte24', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'soporte24', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'REG     ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL)
INSERT [dbo].[llxny_user] ([rowid], [entity], [ref_ext], [ref_int], [employee], [fk_establishment], [datec], [tms], [fk_user_creat], [fk_user_modif], [login], [pass], [pass_crypted], [pass_temp], [api_key], [gender], [civility], [lastname], [firstname], [address], [zip], [town], [fk_state], [fk_country], [job], [skype], [office_phone], [office_fax], [user_mobile], [email], [signature], [admin], [module_comm], [module_compta], [fk_soc], [fk_socpeople], [fk_member], [fk_user], [note_public], [note], [model_pdf], [datelastlogin], [datepreviouslogin], [egroupware_id], [ldap_sid], [openid], [statut], [photo], [lang], [color], [barcode], [fk_barcode_type], [accountancy_code], [nb_holiday], [thm], [tjm], [salary], [salaryextra], [dateemployment], [weeklyhours], [import_key], [TipoUsuario], [idEmpresa], [rucProveedor]) VALUES (28, 0, NULL, NULL, NULL, NULL, NULL, CAST(N'2020-01-04T17:56:10.6100000' AS DateTime2), NULL, NULL, N'soporte25', N'soporte25', N'736f706f7274653235', NULL, NULL, NULL, NULL, N'soporte25', N'soporte25', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'soporte25', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'REG     ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL)
INSERT [dbo].[llxny_user] ([rowid], [entity], [ref_ext], [ref_int], [employee], [fk_establishment], [datec], [tms], [fk_user_creat], [fk_user_modif], [login], [pass], [pass_crypted], [pass_temp], [api_key], [gender], [civility], [lastname], [firstname], [address], [zip], [town], [fk_state], [fk_country], [job], [skype], [office_phone], [office_fax], [user_mobile], [email], [signature], [admin], [module_comm], [module_compta], [fk_soc], [fk_socpeople], [fk_member], [fk_user], [note_public], [note], [model_pdf], [datelastlogin], [datepreviouslogin], [egroupware_id], [ldap_sid], [openid], [statut], [photo], [lang], [color], [barcode], [fk_barcode_type], [accountancy_code], [nb_holiday], [thm], [tjm], [salary], [salaryextra], [dateemployment], [weeklyhours], [import_key], [TipoUsuario], [idEmpresa], [rucProveedor]) VALUES (29, 0, NULL, NULL, NULL, NULL, NULL, CAST(N'2020-01-04T17:56:10.9866667' AS DateTime2), NULL, NULL, N'soporte26', N'soporte26', N'736f706f7274653236', NULL, NULL, NULL, NULL, N'soporte26', N'soporte26', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'soporte26', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'REG     ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL)
INSERT [dbo].[llxny_user] ([rowid], [entity], [ref_ext], [ref_int], [employee], [fk_establishment], [datec], [tms], [fk_user_creat], [fk_user_modif], [login], [pass], [pass_crypted], [pass_temp], [api_key], [gender], [civility], [lastname], [firstname], [address], [zip], [town], [fk_state], [fk_country], [job], [skype], [office_phone], [office_fax], [user_mobile], [email], [signature], [admin], [module_comm], [module_compta], [fk_soc], [fk_socpeople], [fk_member], [fk_user], [note_public], [note], [model_pdf], [datelastlogin], [datepreviouslogin], [egroupware_id], [ldap_sid], [openid], [statut], [photo], [lang], [color], [barcode], [fk_barcode_type], [accountancy_code], [nb_holiday], [thm], [tjm], [salary], [salaryextra], [dateemployment], [weeklyhours], [import_key], [TipoUsuario], [idEmpresa], [rucProveedor]) VALUES (30, 0, NULL, NULL, NULL, NULL, NULL, CAST(N'2020-01-04T18:14:39.2033333' AS DateTime2), NULL, NULL, N'soporte27', N'soporte27', N'736f706f7274653237', NULL, NULL, NULL, NULL, N'soporte27', N'soporte27', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'soporte27', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'REG     ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL)
INSERT [dbo].[llxny_user] ([rowid], [entity], [ref_ext], [ref_int], [employee], [fk_establishment], [datec], [tms], [fk_user_creat], [fk_user_modif], [login], [pass], [pass_crypted], [pass_temp], [api_key], [gender], [civility], [lastname], [firstname], [address], [zip], [town], [fk_state], [fk_country], [job], [skype], [office_phone], [office_fax], [user_mobile], [email], [signature], [admin], [module_comm], [module_compta], [fk_soc], [fk_socpeople], [fk_member], [fk_user], [note_public], [note], [model_pdf], [datelastlogin], [datepreviouslogin], [egroupware_id], [ldap_sid], [openid], [statut], [photo], [lang], [color], [barcode], [fk_barcode_type], [accountancy_code], [nb_holiday], [thm], [tjm], [salary], [salaryextra], [dateemployment], [weeklyhours], [import_key], [TipoUsuario], [idEmpresa], [rucProveedor]) VALUES (31, 0, NULL, NULL, NULL, NULL, NULL, CAST(N'2020-01-04T18:14:56.4733333' AS DateTime2), NULL, NULL, N'papa', N'papa', N'70617061', NULL, NULL, NULL, NULL, N'papa', N'papa', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'papa', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'REG     ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL)
INSERT [dbo].[llxny_user] ([rowid], [entity], [ref_ext], [ref_int], [employee], [fk_establishment], [datec], [tms], [fk_user_creat], [fk_user_modif], [login], [pass], [pass_crypted], [pass_temp], [api_key], [gender], [civility], [lastname], [firstname], [address], [zip], [town], [fk_state], [fk_country], [job], [skype], [office_phone], [office_fax], [user_mobile], [email], [signature], [admin], [module_comm], [module_compta], [fk_soc], [fk_socpeople], [fk_member], [fk_user], [note_public], [note], [model_pdf], [datelastlogin], [datepreviouslogin], [egroupware_id], [ldap_sid], [openid], [statut], [photo], [lang], [color], [barcode], [fk_barcode_type], [accountancy_code], [nb_holiday], [thm], [tjm], [salary], [salaryextra], [dateemployment], [weeklyhours], [import_key], [TipoUsuario], [idEmpresa], [rucProveedor]) VALUES (32, 0, NULL, NULL, NULL, NULL, NULL, CAST(N'2020-01-04T18:15:34.1633333' AS DateTime2), NULL, NULL, N'manuel', N'manuel', N'6d616e75656c', NULL, NULL, NULL, NULL, N'manuel', N'manuel', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'manuel', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'REG     ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL)
INSERT [dbo].[llxny_user] ([rowid], [entity], [ref_ext], [ref_int], [employee], [fk_establishment], [datec], [tms], [fk_user_creat], [fk_user_modif], [login], [pass], [pass_crypted], [pass_temp], [api_key], [gender], [civility], [lastname], [firstname], [address], [zip], [town], [fk_state], [fk_country], [job], [skype], [office_phone], [office_fax], [user_mobile], [email], [signature], [admin], [module_comm], [module_compta], [fk_soc], [fk_socpeople], [fk_member], [fk_user], [note_public], [note], [model_pdf], [datelastlogin], [datepreviouslogin], [egroupware_id], [ldap_sid], [openid], [statut], [photo], [lang], [color], [barcode], [fk_barcode_type], [accountancy_code], [nb_holiday], [thm], [tjm], [salary], [salaryextra], [dateemployment], [weeklyhours], [import_key], [TipoUsuario], [idEmpresa], [rucProveedor]) VALUES (33, 0, NULL, NULL, NULL, NULL, NULL, CAST(N'2020-01-04T18:15:34.4566667' AS DateTime2), NULL, NULL, N'edgar', N'123654', N'313233363534', NULL, NULL, NULL, NULL, N'condor', N'edgar', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'edccperu@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'REG     ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL)
INSERT [dbo].[llxny_user] ([rowid], [entity], [ref_ext], [ref_int], [employee], [fk_establishment], [datec], [tms], [fk_user_creat], [fk_user_modif], [login], [pass], [pass_crypted], [pass_temp], [api_key], [gender], [civility], [lastname], [firstname], [address], [zip], [town], [fk_state], [fk_country], [job], [skype], [office_phone], [office_fax], [user_mobile], [email], [signature], [admin], [module_comm], [module_compta], [fk_soc], [fk_socpeople], [fk_member], [fk_user], [note_public], [note], [model_pdf], [datelastlogin], [datepreviouslogin], [egroupware_id], [ldap_sid], [openid], [statut], [photo], [lang], [color], [barcode], [fk_barcode_type], [accountancy_code], [nb_holiday], [thm], [tjm], [salary], [salaryextra], [dateemployment], [weeklyhours], [import_key], [TipoUsuario], [idEmpresa], [rucProveedor]) VALUES (34, 0, NULL, NULL, NULL, NULL, NULL, CAST(N'2020-01-04T18:15:34.4666667' AS DateTime2), NULL, NULL, N'jorge', N'123654', N'313233363534', NULL, NULL, NULL, NULL, N'jorge luis', N'jorge luis', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'jorge luis', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'REG     ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL)
INSERT [dbo].[llxny_user] ([rowid], [entity], [ref_ext], [ref_int], [employee], [fk_establishment], [datec], [tms], [fk_user_creat], [fk_user_modif], [login], [pass], [pass_crypted], [pass_temp], [api_key], [gender], [civility], [lastname], [firstname], [address], [zip], [town], [fk_state], [fk_country], [job], [skype], [office_phone], [office_fax], [user_mobile], [email], [signature], [admin], [module_comm], [module_compta], [fk_soc], [fk_socpeople], [fk_member], [fk_user], [note_public], [note], [model_pdf], [datelastlogin], [datepreviouslogin], [egroupware_id], [ldap_sid], [openid], [statut], [photo], [lang], [color], [barcode], [fk_barcode_type], [accountancy_code], [nb_holiday], [thm], [tjm], [salary], [salaryextra], [dateemployment], [weeklyhours], [import_key], [TipoUsuario], [idEmpresa], [rucProveedor]) VALUES (35, 0, NULL, NULL, NULL, NULL, NULL, CAST(N'2020-01-04T18:17:34.0100000' AS DateTime2), NULL, NULL, N'pumahuacre', N'pumahuacre', N'70756d61687561637265', NULL, NULL, NULL, NULL, N'pumahuacre', N'pumahuacre', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'pumahuacre', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'REG     ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL)
INSERT [dbo].[llxny_user] ([rowid], [entity], [ref_ext], [ref_int], [employee], [fk_establishment], [datec], [tms], [fk_user_creat], [fk_user_modif], [login], [pass], [pass_crypted], [pass_temp], [api_key], [gender], [civility], [lastname], [firstname], [address], [zip], [town], [fk_state], [fk_country], [job], [skype], [office_phone], [office_fax], [user_mobile], [email], [signature], [admin], [module_comm], [module_compta], [fk_soc], [fk_socpeople], [fk_member], [fk_user], [note_public], [note], [model_pdf], [datelastlogin], [datepreviouslogin], [egroupware_id], [ldap_sid], [openid], [statut], [photo], [lang], [color], [barcode], [fk_barcode_type], [accountancy_code], [nb_holiday], [thm], [tjm], [salary], [salaryextra], [dateemployment], [weeklyhours], [import_key], [TipoUsuario], [idEmpresa], [rucProveedor]) VALUES (36, 0, NULL, NULL, NULL, NULL, NULL, CAST(N'2020-01-04T18:17:34.2666667' AS DateTime2), NULL, NULL, N'condor', N'123', N'313233', NULL, NULL, NULL, NULL, N'condor', N'condor', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'econdor@sunquyux.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'REG     ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL)
INSERT [dbo].[llxny_user] ([rowid], [entity], [ref_ext], [ref_int], [employee], [fk_establishment], [datec], [tms], [fk_user_creat], [fk_user_modif], [login], [pass], [pass_crypted], [pass_temp], [api_key], [gender], [civility], [lastname], [firstname], [address], [zip], [town], [fk_state], [fk_country], [job], [skype], [office_phone], [office_fax], [user_mobile], [email], [signature], [admin], [module_comm], [module_compta], [fk_soc], [fk_socpeople], [fk_member], [fk_user], [note_public], [note], [model_pdf], [datelastlogin], [datepreviouslogin], [egroupware_id], [ldap_sid], [openid], [statut], [photo], [lang], [color], [barcode], [fk_barcode_type], [accountancy_code], [nb_holiday], [thm], [tjm], [salary], [salaryextra], [dateemployment], [weeklyhours], [import_key], [TipoUsuario], [idEmpresa], [rucProveedor]) VALUES (37, 0, NULL, NULL, NULL, NULL, NULL, CAST(N'2020-01-04T18:17:34.2766667' AS DateTime2), NULL, NULL, N'quispe paredes', N'quispe paredes', N'7175697370652070617265646573', NULL, NULL, NULL, NULL, N'quispe paredes', N'quispe paredes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'quispe paredes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'REG     ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL)
INSERT [dbo].[llxny_user] ([rowid], [entity], [ref_ext], [ref_int], [employee], [fk_establishment], [datec], [tms], [fk_user_creat], [fk_user_modif], [login], [pass], [pass_crypted], [pass_temp], [api_key], [gender], [civility], [lastname], [firstname], [address], [zip], [town], [fk_state], [fk_country], [job], [skype], [office_phone], [office_fax], [user_mobile], [email], [signature], [admin], [module_comm], [module_compta], [fk_soc], [fk_socpeople], [fk_member], [fk_user], [note_public], [note], [model_pdf], [datelastlogin], [datepreviouslogin], [egroupware_id], [ldap_sid], [openid], [statut], [photo], [lang], [color], [barcode], [fk_barcode_type], [accountancy_code], [nb_holiday], [thm], [tjm], [salary], [salaryextra], [dateemployment], [weeklyhours], [import_key], [TipoUsuario], [idEmpresa], [rucProveedor]) VALUES (38, 0, NULL, NULL, NULL, NULL, NULL, CAST(N'2020-01-08T21:10:18.6166667' AS DateTime2), NULL, NULL, N'soporte28', N'soporte', N'736f706f727465', NULL, NULL, NULL, NULL, N'soporte', N'soporte', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'soporte', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'REG     ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL)
INSERT [dbo].[llxny_user] ([rowid], [entity], [ref_ext], [ref_int], [employee], [fk_establishment], [datec], [tms], [fk_user_creat], [fk_user_modif], [login], [pass], [pass_crypted], [pass_temp], [api_key], [gender], [civility], [lastname], [firstname], [address], [zip], [town], [fk_state], [fk_country], [job], [skype], [office_phone], [office_fax], [user_mobile], [email], [signature], [admin], [module_comm], [module_compta], [fk_soc], [fk_socpeople], [fk_member], [fk_user], [note_public], [note], [model_pdf], [datelastlogin], [datepreviouslogin], [egroupware_id], [ldap_sid], [openid], [statut], [photo], [lang], [color], [barcode], [fk_barcode_type], [accountancy_code], [nb_holiday], [thm], [tjm], [salary], [salaryextra], [dateemployment], [weeklyhours], [import_key], [TipoUsuario], [idEmpresa], [rucProveedor]) VALUES (39, 0, NULL, NULL, NULL, NULL, NULL, CAST(N'2020-01-08T21:10:57.3533333' AS DateTime2), NULL, NULL, N'soporte29', N'100', N'313030', NULL, NULL, NULL, NULL, N'soporte29', N'soporte29', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'soporte29', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'REG     ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL)
INSERT [dbo].[llxny_user] ([rowid], [entity], [ref_ext], [ref_int], [employee], [fk_establishment], [datec], [tms], [fk_user_creat], [fk_user_modif], [login], [pass], [pass_crypted], [pass_temp], [api_key], [gender], [civility], [lastname], [firstname], [address], [zip], [town], [fk_state], [fk_country], [job], [skype], [office_phone], [office_fax], [user_mobile], [email], [signature], [admin], [module_comm], [module_compta], [fk_soc], [fk_socpeople], [fk_member], [fk_user], [note_public], [note], [model_pdf], [datelastlogin], [datepreviouslogin], [egroupware_id], [ldap_sid], [openid], [statut], [photo], [lang], [color], [barcode], [fk_barcode_type], [accountancy_code], [nb_holiday], [thm], [tjm], [salary], [salaryextra], [dateemployment], [weeklyhours], [import_key], [TipoUsuario], [idEmpresa], [rucProveedor]) VALUES (40, 0, NULL, NULL, NULL, NULL, NULL, CAST(N'2020-01-08T21:11:56.8466667' AS DateTime2), NULL, NULL, N'soporte30', N'soporte30', N'736f706f7274653330', NULL, NULL, NULL, NULL, N'soporte30', N'soporte30', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'soporte30', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'REG     ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL)
INSERT [dbo].[llxny_user] ([rowid], [entity], [ref_ext], [ref_int], [employee], [fk_establishment], [datec], [tms], [fk_user_creat], [fk_user_modif], [login], [pass], [pass_crypted], [pass_temp], [api_key], [gender], [civility], [lastname], [firstname], [address], [zip], [town], [fk_state], [fk_country], [job], [skype], [office_phone], [office_fax], [user_mobile], [email], [signature], [admin], [module_comm], [module_compta], [fk_soc], [fk_socpeople], [fk_member], [fk_user], [note_public], [note], [model_pdf], [datelastlogin], [datepreviouslogin], [egroupware_id], [ldap_sid], [openid], [statut], [photo], [lang], [color], [barcode], [fk_barcode_type], [accountancy_code], [nb_holiday], [thm], [tjm], [salary], [salaryextra], [dateemployment], [weeklyhours], [import_key], [TipoUsuario], [idEmpresa], [rucProveedor]) VALUES (41, 0, NULL, NULL, NULL, NULL, NULL, CAST(N'2020-01-08T21:13:09.7700000' AS DateTime2), NULL, NULL, N'soporte31', N'100', N'313030', NULL, NULL, NULL, NULL, N'soporte30', N'soporte30', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'soporte30', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'REG     ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL)
INSERT [dbo].[llxny_user] ([rowid], [entity], [ref_ext], [ref_int], [employee], [fk_establishment], [datec], [tms], [fk_user_creat], [fk_user_modif], [login], [pass], [pass_crypted], [pass_temp], [api_key], [gender], [civility], [lastname], [firstname], [address], [zip], [town], [fk_state], [fk_country], [job], [skype], [office_phone], [office_fax], [user_mobile], [email], [signature], [admin], [module_comm], [module_compta], [fk_soc], [fk_socpeople], [fk_member], [fk_user], [note_public], [note], [model_pdf], [datelastlogin], [datepreviouslogin], [egroupware_id], [ldap_sid], [openid], [statut], [photo], [lang], [color], [barcode], [fk_barcode_type], [accountancy_code], [nb_holiday], [thm], [tjm], [salary], [salaryextra], [dateemployment], [weeklyhours], [import_key], [TipoUsuario], [idEmpresa], [rucProveedor]) VALUES (42, 0, NULL, NULL, NULL, NULL, NULL, CAST(N'2020-01-09T15:38:44.3433333' AS DateTime2), NULL, NULL, N'angel', N'angel', N'616e67656c', NULL, NULL, NULL, NULL, N'angel', N'angel', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'angel', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'REG     ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL)
INSERT [dbo].[llxny_user] ([rowid], [entity], [ref_ext], [ref_int], [employee], [fk_establishment], [datec], [tms], [fk_user_creat], [fk_user_modif], [login], [pass], [pass_crypted], [pass_temp], [api_key], [gender], [civility], [lastname], [firstname], [address], [zip], [town], [fk_state], [fk_country], [job], [skype], [office_phone], [office_fax], [user_mobile], [email], [signature], [admin], [module_comm], [module_compta], [fk_soc], [fk_socpeople], [fk_member], [fk_user], [note_public], [note], [model_pdf], [datelastlogin], [datepreviouslogin], [egroupware_id], [ldap_sid], [openid], [statut], [photo], [lang], [color], [barcode], [fk_barcode_type], [accountancy_code], [nb_holiday], [thm], [tjm], [salary], [salaryextra], [dateemployment], [weeklyhours], [import_key], [TipoUsuario], [idEmpresa], [rucProveedor]) VALUES (43, 0, NULL, NULL, NULL, NULL, NULL, CAST(N'2020-01-09T15:39:00.9966667' AS DateTime2), NULL, NULL, N'angel22', N'angel', N'616e67656c', NULL, NULL, NULL, NULL, N'angel', N'angel', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'angel', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'REG     ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL)
INSERT [dbo].[llxny_user] ([rowid], [entity], [ref_ext], [ref_int], [employee], [fk_establishment], [datec], [tms], [fk_user_creat], [fk_user_modif], [login], [pass], [pass_crypted], [pass_temp], [api_key], [gender], [civility], [lastname], [firstname], [address], [zip], [town], [fk_state], [fk_country], [job], [skype], [office_phone], [office_fax], [user_mobile], [email], [signature], [admin], [module_comm], [module_compta], [fk_soc], [fk_socpeople], [fk_member], [fk_user], [note_public], [note], [model_pdf], [datelastlogin], [datepreviouslogin], [egroupware_id], [ldap_sid], [openid], [statut], [photo], [lang], [color], [barcode], [fk_barcode_type], [accountancy_code], [nb_holiday], [thm], [tjm], [salary], [salaryextra], [dateemployment], [weeklyhours], [import_key], [TipoUsuario], [idEmpresa], [rucProveedor]) VALUES (44, 0, NULL, NULL, NULL, NULL, NULL, CAST(N'2020-01-09T15:39:41.5600000' AS DateTime2), NULL, NULL, N'benito', N'benito', N'62656e69746f', NULL, NULL, NULL, NULL, N'benito', N'benito', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'benito', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'REG     ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL)
INSERT [dbo].[llxny_user] ([rowid], [entity], [ref_ext], [ref_int], [employee], [fk_establishment], [datec], [tms], [fk_user_creat], [fk_user_modif], [login], [pass], [pass_crypted], [pass_temp], [api_key], [gender], [civility], [lastname], [firstname], [address], [zip], [town], [fk_state], [fk_country], [job], [skype], [office_phone], [office_fax], [user_mobile], [email], [signature], [admin], [module_comm], [module_compta], [fk_soc], [fk_socpeople], [fk_member], [fk_user], [note_public], [note], [model_pdf], [datelastlogin], [datepreviouslogin], [egroupware_id], [ldap_sid], [openid], [statut], [photo], [lang], [color], [barcode], [fk_barcode_type], [accountancy_code], [nb_holiday], [thm], [tjm], [salary], [salaryextra], [dateemployment], [weeklyhours], [import_key], [TipoUsuario], [idEmpresa], [rucProveedor]) VALUES (45, 0, NULL, NULL, NULL, NULL, NULL, CAST(N'2020-01-09T15:40:47.5333333' AS DateTime2), NULL, NULL, N'federico', N'federico', N'666564657269636f', NULL, NULL, NULL, NULL, N'federico', N'federico', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'federico', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'REG     ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL)
INSERT [dbo].[llxny_user] ([rowid], [entity], [ref_ext], [ref_int], [employee], [fk_establishment], [datec], [tms], [fk_user_creat], [fk_user_modif], [login], [pass], [pass_crypted], [pass_temp], [api_key], [gender], [civility], [lastname], [firstname], [address], [zip], [town], [fk_state], [fk_country], [job], [skype], [office_phone], [office_fax], [user_mobile], [email], [signature], [admin], [module_comm], [module_compta], [fk_soc], [fk_socpeople], [fk_member], [fk_user], [note_public], [note], [model_pdf], [datelastlogin], [datepreviouslogin], [egroupware_id], [ldap_sid], [openid], [statut], [photo], [lang], [color], [barcode], [fk_barcode_type], [accountancy_code], [nb_holiday], [thm], [tjm], [salary], [salaryextra], [dateemployment], [weeklyhours], [import_key], [TipoUsuario], [idEmpresa], [rucProveedor]) VALUES (46, 0, NULL, NULL, NULL, NULL, NULL, CAST(N'2020-01-09T15:41:13.9333333' AS DateTime2), NULL, NULL, N'federica', N'federica', N'6665646572696361', NULL, NULL, NULL, NULL, N'federica', N'federica', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'federica', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'REG     ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL)
INSERT [dbo].[llxny_user] ([rowid], [entity], [ref_ext], [ref_int], [employee], [fk_establishment], [datec], [tms], [fk_user_creat], [fk_user_modif], [login], [pass], [pass_crypted], [pass_temp], [api_key], [gender], [civility], [lastname], [firstname], [address], [zip], [town], [fk_state], [fk_country], [job], [skype], [office_phone], [office_fax], [user_mobile], [email], [signature], [admin], [module_comm], [module_compta], [fk_soc], [fk_socpeople], [fk_member], [fk_user], [note_public], [note], [model_pdf], [datelastlogin], [datepreviouslogin], [egroupware_id], [ldap_sid], [openid], [statut], [photo], [lang], [color], [barcode], [fk_barcode_type], [accountancy_code], [nb_holiday], [thm], [tjm], [salary], [salaryextra], [dateemployment], [weeklyhours], [import_key], [TipoUsuario], [idEmpresa], [rucProveedor]) VALUES (47, 0, NULL, NULL, NULL, NULL, NULL, CAST(N'2020-01-13T14:41:24.5166667' AS DateTime2), NULL, NULL, N'soporte222', N'soporte24', N'736f706f7274653234', NULL, NULL, NULL, NULL, N'soporte24', N'soporte24', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'soporte24', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'REG     ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL)
INSERT [dbo].[llxny_user] ([rowid], [entity], [ref_ext], [ref_int], [employee], [fk_establishment], [datec], [tms], [fk_user_creat], [fk_user_modif], [login], [pass], [pass_crypted], [pass_temp], [api_key], [gender], [civility], [lastname], [firstname], [address], [zip], [town], [fk_state], [fk_country], [job], [skype], [office_phone], [office_fax], [user_mobile], [email], [signature], [admin], [module_comm], [module_compta], [fk_soc], [fk_socpeople], [fk_member], [fk_user], [note_public], [note], [model_pdf], [datelastlogin], [datepreviouslogin], [egroupware_id], [ldap_sid], [openid], [statut], [photo], [lang], [color], [barcode], [fk_barcode_type], [accountancy_code], [nb_holiday], [thm], [tjm], [salary], [salaryextra], [dateemployment], [weeklyhours], [import_key], [TipoUsuario], [idEmpresa], [rucProveedor]) VALUES (48, 0, NULL, NULL, NULL, NULL, NULL, CAST(N'2020-01-13T14:44:00.2600000' AS DateTime2), NULL, NULL, N'soporte221', N'soporte221', N'736f706f727465323231', NULL, NULL, NULL, NULL, N'soporte221', N'soporte221', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'soporte221', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'REG     ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL)
SET IDENTITY_INSERT [dbo].[llxny_user] OFF
SET IDENTITY_INSERT [dbo].[Menu] ON 

INSERT [dbo].[Menu] ([MenuId], [NombreMenu], [URLPagina], [MenuPadreId], [Comentario], [IconName], [EstadoRegistro], [numeroOrden]) VALUES (1, N'Dashboard', N'DashboardHome', 0, N'Grafricos', N'fa-bar-chart', N'REG', 1)
INSERT [dbo].[Menu] ([MenuId], [NombreMenu], [URLPagina], [MenuPadreId], [Comentario], [IconName], [EstadoRegistro], [numeroOrden]) VALUES (1015, N'Proyecto', NULL, 0, NULL, NULL, N'REG', 2)
INSERT [dbo].[Menu] ([MenuId], [NombreMenu], [URLPagina], [MenuPadreId], [Comentario], [IconName], [EstadoRegistro], [numeroOrden]) VALUES (1016, N'Producto', N'Producto', 1015, NULL, NULL, N'REG', 3)
INSERT [dbo].[Menu] ([MenuId], [NombreMenu], [URLPagina], [MenuPadreId], [Comentario], [IconName], [EstadoRegistro], [numeroOrden]) VALUES (1017, N'Cliente', N'Cliente', 1015, NULL, NULL, N'REG', 4)
INSERT [dbo].[Menu] ([MenuId], [NombreMenu], [URLPagina], [MenuPadreId], [Comentario], [IconName], [EstadoRegistro], [numeroOrden]) VALUES (1018, N'Invoice', N'Invoice', 1015, NULL, NULL, N'REG', 5)
SET IDENTITY_INSERT [dbo].[Menu] OFF
SET IDENTITY_INSERT [dbo].[MultiTabla] ON 

INSERT [dbo].[MultiTabla] ([IdMultiTabla], [NombreTabla], [EstadoRegistro]) VALUES (1, N'TipoUsuario', N'REG')
INSERT [dbo].[MultiTabla] ([IdMultiTabla], [NombreTabla], [EstadoRegistro]) VALUES (2, N'TipoEmpresa', N'REG')
INSERT [dbo].[MultiTabla] ([IdMultiTabla], [NombreTabla], [EstadoRegistro]) VALUES (3, N'TipoInforme', N'REG')
INSERT [dbo].[MultiTabla] ([IdMultiTabla], [NombreTabla], [EstadoRegistro]) VALUES (4, N'TipoDocumento', N'REG')
INSERT [dbo].[MultiTabla] ([IdMultiTabla], [NombreTabla], [EstadoRegistro]) VALUES (5, N'EstadoCivil', N'REG')
INSERT [dbo].[MultiTabla] ([IdMultiTabla], [NombreTabla], [EstadoRegistro]) VALUES (6, N'Genero', N'REG')
INSERT [dbo].[MultiTabla] ([IdMultiTabla], [NombreTabla], [EstadoRegistro]) VALUES (7, N'RegionAnatomica', N'REG')
INSERT [dbo].[MultiTabla] ([IdMultiTabla], [NombreTabla], [EstadoRegistro]) VALUES (8, N'Unidad', N'REG')
INSERT [dbo].[MultiTabla] ([IdMultiTabla], [NombreTabla], [EstadoRegistro]) VALUES (9, N'FrecuenciaCambio', N'REG')
INSERT [dbo].[MultiTabla] ([IdMultiTabla], [NombreTabla], [EstadoRegistro]) VALUES (10, N'Gravedad Accidente', N'REG')
INSERT [dbo].[MultiTabla] ([IdMultiTabla], [NombreTabla], [EstadoRegistro]) VALUES (11, N'Grado Accidente', N'REG')
INSERT [dbo].[MultiTabla] ([IdMultiTabla], [NombreTabla], [EstadoRegistro]) VALUES (12, N'Falta de Control', N'REG')
INSERT [dbo].[MultiTabla] ([IdMultiTabla], [NombreTabla], [EstadoRegistro]) VALUES (13, N'CondicionIPERmatriz', N'REG')
SET IDENTITY_INSERT [dbo].[MultiTabla] OFF
SET IDENTITY_INSERT [dbo].[MultiTabla2] ON 

INSERT [dbo].[MultiTabla2] ([IdMultiTabla], [NombreTabla], [EstadoRegistro]) VALUES (1, N'TipoUsuario', N'REG')
INSERT [dbo].[MultiTabla2] ([IdMultiTabla], [NombreTabla], [EstadoRegistro]) VALUES (2, N'TipoEmpresa', N'REG')
INSERT [dbo].[MultiTabla2] ([IdMultiTabla], [NombreTabla], [EstadoRegistro]) VALUES (3, N'TipoInforme', N'REG')
INSERT [dbo].[MultiTabla2] ([IdMultiTabla], [NombreTabla], [EstadoRegistro]) VALUES (4, N'TipoDocumento', N'REG')
INSERT [dbo].[MultiTabla2] ([IdMultiTabla], [NombreTabla], [EstadoRegistro]) VALUES (5, N'EstadoCivil', N'REG')
INSERT [dbo].[MultiTabla2] ([IdMultiTabla], [NombreTabla], [EstadoRegistro]) VALUES (6, N'Genero', N'REG')
INSERT [dbo].[MultiTabla2] ([IdMultiTabla], [NombreTabla], [EstadoRegistro]) VALUES (7, N'RegionAnatomica', N'REG')
INSERT [dbo].[MultiTabla2] ([IdMultiTabla], [NombreTabla], [EstadoRegistro]) VALUES (8, N'Unidad', N'REG')
INSERT [dbo].[MultiTabla2] ([IdMultiTabla], [NombreTabla], [EstadoRegistro]) VALUES (9, N'FrecuenciaCambio', N'REG')
INSERT [dbo].[MultiTabla2] ([IdMultiTabla], [NombreTabla], [EstadoRegistro]) VALUES (10, N'Gravedad Accidente', N'REG')
INSERT [dbo].[MultiTabla2] ([IdMultiTabla], [NombreTabla], [EstadoRegistro]) VALUES (11, N'Grado Accidente', N'REG')
INSERT [dbo].[MultiTabla2] ([IdMultiTabla], [NombreTabla], [EstadoRegistro]) VALUES (12, N'Falta de Control', N'REG')
INSERT [dbo].[MultiTabla2] ([IdMultiTabla], [NombreTabla], [EstadoRegistro]) VALUES (13, N'CondicionIPERmatriz', N'REG')
INSERT [dbo].[MultiTabla2] ([IdMultiTabla], [NombreTabla], [EstadoRegistro]) VALUES (14, N'Tipo', N'REG')
SET IDENTITY_INSERT [dbo].[MultiTabla2] OFF
SET IDENTITY_INSERT [dbo].[MultitablaValor] ON 

INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (1, N'0', NULL, NULL, N'Usuario Normal', NULL, NULL, NULL, 1, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (2, N'1', NULL, NULL, N'Administrador Principal', NULL, NULL, NULL, 1, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (5, N'J', NULL, NULL, N'Juridica', NULL, NULL, NULL, 2, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (6, N'N', NULL, NULL, N'Natural', NULL, NULL, NULL, 2, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (8, N'2', NULL, NULL, N'Administrador Empresas', NULL, NULL, NULL, 1, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (10, N'1', NULL, NULL, N'Informal', NULL, NULL, NULL, 3, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (11, N'2', NULL, NULL, N'Planeada', NULL, NULL, NULL, 3, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (12, N'3', NULL, NULL, N'Especial', NULL, NULL, NULL, 3, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (13, N'1', NULL, NULL, N'DNI', NULL, NULL, NULL, 4, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (14, N'2', NULL, NULL, N'RUC', NULL, NULL, NULL, 4, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (16, N'1', NULL, NULL, N'SOLTERO', NULL, NULL, NULL, 5, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (17, N'2', NULL, NULL, N'CASADO', NULL, NULL, NULL, 5, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (18, N'3', NULL, NULL, N'DIVORCIADO', NULL, NULL, NULL, 5, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (19, N'4', NULL, NULL, N'VIUDO', NULL, NULL, NULL, 5, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (21, N'M', NULL, NULL, N'MASCULINO', NULL, NULL, NULL, 6, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (22, N'F', NULL, NULL, N'FEMENINO', NULL, NULL, NULL, 6, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (23, N'1', N'0', NULL, N'Cabeza', NULL, NULL, NULL, 7, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (24, N'2', N'1', NULL, N'Casco contra impacto', NULL, NULL, NULL, 7, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (25, N'3', N'1', NULL, N'Casco dieléctrico', NULL, NULL, NULL, 7, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (26, N'4', N'1', NULL, N'Barbiquejo', NULL, NULL, NULL, 7, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (27, N'5', N'1', NULL, N'Linterna de manos libres', NULL, NULL, NULL, 7, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (28, N'6', N'1', NULL, N'Cofia', NULL, NULL, NULL, 7, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (29, N'7', N'1', NULL, N'Gorros', NULL, NULL, NULL, 7, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (30, N'8', N'1', NULL, N'Sombreros', NULL, NULL, NULL, 7, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (31, N'9', N'1', NULL, N'Otros', NULL, NULL, NULL, 7, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (32, N'10', N'0', NULL, N'Ojos y cara', NULL, NULL, NULL, 7, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (33, N'11', N'10', NULL, N'Anteojos de protección', NULL, NULL, NULL, 7, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (34, N'12', N'10', NULL, N'Gafas de montura', NULL, NULL, NULL, 7, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (35, N'13', N'10', NULL, N'Goggles', NULL, NULL, NULL, 7, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (36, N'14', N'10', NULL, N'Pantalla facial', NULL, NULL, NULL, 7, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (37, N'15', N'10', NULL, N'Careta para soldador', NULL, NULL, NULL, 7, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (38, N'16', N'10', NULL, N'Gafas para soldador', NULL, NULL, NULL, 7, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (39, N'17', N'10', NULL, N'otros', NULL, NULL, NULL, 7, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (40, N'18', N'0', NULL, N'Oidos', NULL, NULL, NULL, 7, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (41, N'19', N'18', NULL, N'Protectores auditivos tipo «tapones»', NULL, NULL, NULL, 7, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (42, N'20', N'18', NULL, N'Protectores auditivos desechables', NULL, NULL, NULL, 7, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (43, N'21', N'18', NULL, N'Protectores auditivos tipo «orejeras»', NULL, NULL, NULL, 7, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (44, N'22', N'18', NULL, N'Protectores auditivos acoplables a los cascos', NULL, NULL, NULL, 7, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (45, N'23', N'18', NULL, N'Protectores auditivos con aparatos de intercomunicación', NULL, NULL, NULL, 7, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (46, N'24', N'18', NULL, N'otros', NULL, NULL, NULL, 7, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (47, N'25', N'0', NULL, N'Aparato respiratorio', NULL, NULL, NULL, 7, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (48, N'26', N'25', NULL, N'Respirador contra partículas (desechable)', NULL, NULL, NULL, 7, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (49, N'27', N'25', NULL, N'Respirador doble vía', NULL, NULL, NULL, 7, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (50, N'28', N'25', NULL, N'Filtros contra Vapores Organicos & Gases Acido', NULL, NULL, NULL, 7, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (51, N'29', N'25', NULL, N'Filtros contra Gases Ácidos ', NULL, NULL, NULL, 7, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (52, N'30', N'25', NULL, N'Retenedor', NULL, NULL, NULL, 7, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (53, N'31', N'25', NULL, N'Prefiltro', NULL, NULL, NULL, 7, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (54, N'32', N'25', NULL, N'Respirador autónomo', NULL, NULL, NULL, 7, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (55, N'33', N'25', NULL, N'Respirador con casco o pantalla para soldadura', NULL, NULL, NULL, 7, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (56, N'34', N'25', NULL, N'Equipos de submarinismo', NULL, NULL, NULL, 7, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (57, N'35', N'25', NULL, N'otros', NULL, NULL, NULL, 7, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (58, N'36', N'0', NULL, N'Manos y brazos', NULL, NULL, NULL, 7, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (59, N'37', N'36', NULL, N'Guantes contra las agresiones mecánicas', NULL, NULL, NULL, 7, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (60, N'38', N'36', NULL, N'Guantes contra sustancias químicas', NULL, NULL, NULL, 7, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (61, N'39', N'36', NULL, N'Guantes para uso eléctrico', NULL, NULL, NULL, 7, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (62, N'40', N'36', NULL, N'Guantes contra altas temperaturas', NULL, NULL, NULL, 7, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (63, N'41', N'36', NULL, N'Guantes dieléctricos', NULL, NULL, NULL, 7, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (64, N'42', N'36', NULL, N'Mangas', NULL, NULL, NULL, 7, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (65, N'43', N'36', NULL, N'Otros', NULL, NULL, NULL, 7, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (66, N'44', N'0', NULL, N'Tronco y abdomen', NULL, NULL, NULL, 7, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (67, N'45', N'44', NULL, N'Chaleco reflectivos', NULL, NULL, NULL, 7, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (68, N'46', N'44', NULL, N'Chaleco contra las agresiones mecánicas', NULL, NULL, NULL, 7, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (69, N'47', N'44', NULL, N'Chaleco contra las agresiones químicas', NULL, NULL, NULL, 7, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (70, N'48', N'44', NULL, N'Chalecos salvavidas', NULL, NULL, NULL, 7, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (71, N'49', N'44', NULL, N'Mandil contra altas temperaturas', NULL, NULL, NULL, 7, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (72, N'50', N'44', NULL, N'Mandil contra sustancias químicas', NULL, NULL, NULL, 7, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (73, N'51', N'44', NULL, N'Mandiles de protección contra los rayos X', NULL, NULL, NULL, 7, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (74, N'52', N'44', NULL, N'Overol', NULL, NULL, NULL, 7, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (75, N'53', N'44', NULL, N'Bata', NULL, NULL, NULL, 7, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (76, N'54', N'44', NULL, N'Otros', NULL, NULL, NULL, 7, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (77, N'55', N'0', NULL, N'Pie y piernas', NULL, NULL, NULL, 7, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (78, N'56', N'55', NULL, N'Calzado de seguridad (contra impactos)', NULL, NULL, NULL, 7, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (79, N'57', N'55', NULL, N'Cubrecalzado de protección contra el calor', NULL, NULL, NULL, 7, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (80, N'58', N'55', NULL, N'Cubrecalzado de protección contra el frio', NULL, NULL, NULL, 7, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (81, N'59', N'55', NULL, N'Calzado dieléctrico', NULL, NULL, NULL, 7, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (82, N'60', N'55', NULL, N'Calzado contra sustancias químicas', NULL, NULL, NULL, 7, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (83, N'61', N'55', NULL, N'Polainas', NULL, NULL, NULL, 7, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (84, N'62', N'55', NULL, N'Botas impermeables', NULL, NULL, NULL, 7, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (85, N'63', N'55', NULL, N'Rodilleras', NULL, NULL, NULL, 7, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (86, N'64', N'55', NULL, N'Otros', NULL, NULL, NULL, 7, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (87, N'65', N'0', NULL, N'Piel', NULL, NULL, NULL, 7, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (88, N'66', N'65', NULL, N' Cremas de protección y pomadas', NULL, NULL, NULL, 7, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (89, N'67', N'0', NULL, N'Cuerpo', NULL, NULL, NULL, 7, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (90, N'68', N'67', NULL, N'Arnes', NULL, NULL, NULL, 7, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (91, N'69', N'67', NULL, N'Cinturones de sujeción', NULL, NULL, NULL, 7, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (92, N'70', N'67', NULL, N'Dispositivos anticaídas con amortiguador', NULL, NULL, NULL, 7, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (93, N'71', N'67', NULL, N'Ropa antigás', NULL, NULL, NULL, 7, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (94, N'72', N'67', NULL, N'Ropa de protección', NULL, NULL, NULL, 7, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (95, N'73', N'67', NULL, N'Ropa de protección contra las agresiones mecánicas', NULL, NULL, NULL, 7, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (96, N'74', N'67', NULL, N'Ropa de protección contra las agresiones químicas', NULL, NULL, NULL, 7, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (97, N'75', N'67', NULL, N'Ropa de protección contra la contaminación radiactiva', NULL, NULL, NULL, 7, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (98, N'76', N'0', NULL, N'Otros', NULL, NULL, NULL, 7, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (101, N'1', NULL, NULL, N'UNIDAD', NULL, NULL, NULL, 8, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (102, N'2', NULL, NULL, N'PAR', NULL, NULL, NULL, 8, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (103, N'3', NULL, NULL, N'CAJA', NULL, NULL, NULL, 8, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (104, N'1', NULL, NULL, N'Mensual', NULL, NULL, NULL, 9, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (105, N'2', NULL, NULL, N'Quincenal', NULL, NULL, NULL, 9, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (106, N'3', NULL, NULL, N'Trimestral', NULL, NULL, NULL, 9, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (107, N'4', NULL, NULL, N'Semestral', NULL, NULL, NULL, 9, NULL, N'REG')
GO
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (108, N'5', NULL, NULL, N'Anual', NULL, NULL, NULL, 9, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (109, N'1', NULL, NULL, N'Leve', NULL, NULL, NULL, 10, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (110, N'2', NULL, NULL, N'Incapacitante', NULL, NULL, NULL, 10, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (111, N'3', NULL, NULL, N'Mortal', NULL, NULL, NULL, 10, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (112, N'1', NULL, NULL, N'Parcial Temporal', NULL, NULL, NULL, 11, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (113, N'2', NULL, NULL, N'Parcial Permanente', NULL, NULL, NULL, 11, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (114, N'3', NULL, NULL, N'Total Temporal', NULL, NULL, NULL, 11, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (115, N'4', NULL, NULL, N'Total Permanente', NULL, NULL, NULL, 11, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (117, N'P', NULL, NULL, N'Programa inadecuados / insistentes', N'P', NULL, NULL, 12, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (118, N'E', NULL, NULL, N'Estandares inesistentes', N'E', NULL, NULL, 12, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (119, N'C', NULL, NULL, N'Incumplimiento de los estandares', N'C', NULL, NULL, 12, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (120, N'1', NULL, NULL, N'Rutinario', NULL, NULL, NULL, 13, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (122, N'2', NULL, NULL, N'No Rutinario', NULL, NULL, NULL, 13, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (123, N'3', NULL, NULL, N'Emergencia', NULL, NULL, NULL, 13, NULL, N'REG')
INSERT [dbo].[MultitablaValor] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (124, N'3', NULL, NULL, N'Proveedor', NULL, NULL, NULL, 1, NULL, N'REG')
SET IDENTITY_INSERT [dbo].[MultitablaValor] OFF
SET IDENTITY_INSERT [dbo].[MultitablaValor2] ON 

INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (1, N'REG', NULL, NULL, N'REGISTRADO', NULL, NULL, NULL, 1, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (2, N'ANU', NULL, NULL, N'ANULADO', NULL, NULL, NULL, 1, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (3, N'PEN', NULL, NULL, N'SOLES', NULL, NULL, NULL, 2, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (4, N'USD', NULL, NULL, N'DOLARES', NULL, NULL, NULL, 2, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (5, N'6', NULL, NULL, N'R.U.C', NULL, NULL, NULL, 3, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (6, N'1', NULL, NULL, N'DNI', NULL, NULL, NULL, 3, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (7, N'4', NULL, NULL, N'Carnet de extranjeria', NULL, NULL, NULL, 3, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (8, N'7', NULL, NULL, N'Pasaporte', NULL, NULL, NULL, 3, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (9, N'0', NULL, NULL, N'Doc.Trib.No.Dom.Sin.RUC', NULL, NULL, NULL, 3, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (10, N'1', NULL, NULL, N'Mercaderia', NULL, NULL, NULL, 4, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (11, N'2', NULL, NULL, N'Servicio', NULL, NULL, NULL, 4, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (12, N'3', NULL, NULL, N'Otros', NULL, NULL, NULL, 4, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (13, N'18', NULL, NULL, N'18 %', NULL, NULL, NULL, 5, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (14, N'0', NULL, NULL, N'0 %', NULL, NULL, NULL, 5, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (15, N'NIU', NULL, NULL, N'UNIDAD (BIENES)', NULL, NULL, NULL, 6, 1, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (16, N'ZZ', NULL, NULL, N'UNIDAD (SERVICIOS)', NULL, NULL, NULL, 6, 2, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (17, N'KGM', NULL, NULL, N'KILOGRAMO', NULL, NULL, NULL, 6, 3, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (18, N'PK', NULL, NULL, N'PAQUETE', NULL, NULL, NULL, 6, 4, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (19, N'MT', NULL, NULL, N'METRO', NULL, NULL, NULL, 6, 5, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (20, N'LTR', NULL, NULL, N'LITRO', NULL, NULL, NULL, 6, 6, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (21, N'4A', NULL, NULL, N'BOBINAS', NULL, NULL, NULL, 6, 7, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (22, N'BE', NULL, NULL, N'FARDO', NULL, NULL, NULL, 6, 8, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (23, N'BG', NULL, NULL, N'BOLSA', NULL, NULL, NULL, 6, 9, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (24, N'BJ', NULL, NULL, N'BALDE', NULL, NULL, NULL, 6, 10, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (25, N'BLL', NULL, NULL, N'BARRILES', NULL, NULL, NULL, 6, 11, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (26, N'BO', NULL, NULL, N'BOTELLAS', NULL, NULL, NULL, 6, 12, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (27, N'BX', NULL, NULL, N'CAJA', NULL, NULL, NULL, 6, 13, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (28, N'C62', NULL, NULL, N'PIEZAS', NULL, NULL, NULL, 6, 14, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (29, N'CA', NULL, NULL, N'LATAS', NULL, NULL, NULL, 6, 15, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (30, N'CEN', NULL, NULL, N'CIENTO DE UNIDADES', NULL, NULL, NULL, 6, 16, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (31, N'CJ', NULL, NULL, N'CONOS', NULL, NULL, NULL, 6, 17, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (32, N'CMK', NULL, NULL, N'CENTIMETRO CUADRADO', NULL, NULL, NULL, 6, 18, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (33, N'CMQ', NULL, NULL, N'CENTIMETRO CUBICO', NULL, NULL, NULL, 6, 19, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (34, N'CMT', NULL, NULL, N'CENTIMETRO LINEAL', NULL, NULL, NULL, 6, 20, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (35, N'CT', NULL, NULL, N'CARTONES', NULL, NULL, NULL, 6, 21, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (36, N'CY', NULL, NULL, N'CILINDRO', NULL, NULL, NULL, 6, 22, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (37, N'DR', NULL, NULL, N'TAMBOR', NULL, NULL, NULL, 6, 23, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (38, N'DZN', NULL, NULL, N'DOCENA', NULL, NULL, NULL, 6, 24, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (39, N'DZP', NULL, NULL, N'DOCENA POR 10**6', NULL, NULL, NULL, 6, 25, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (40, N'FOT', NULL, NULL, N'PIES', NULL, NULL, NULL, 6, 26, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (41, N'FTK', NULL, NULL, N'PIES CUADRADOS', NULL, NULL, NULL, 6, 27, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (42, N'FTQ', NULL, NULL, N'PIES CUBICOS', NULL, NULL, NULL, 6, 28, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (43, N'GLI', NULL, NULL, N'GALON INGLES (4,545956L)', NULL, NULL, NULL, 6, 29, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (44, N'GLL', NULL, NULL, N'US GALON (3,7843 L)', NULL, NULL, NULL, 6, 30, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (45, N'GR', NULL, NULL, N'GRAMOS', NULL, NULL, NULL, 6, 31, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (46, N'GRM', NULL, NULL, N'GRAMO', NULL, NULL, NULL, 6, 32, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (47, N'GRO', NULL, NULL, N'GRUESA', NULL, NULL, NULL, 6, 33, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (48, N'HLT', NULL, NULL, N'HECTOLITRO', NULL, NULL, NULL, 6, 34, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (49, N'INH', NULL, NULL, N'PULGADAS', NULL, NULL, NULL, 6, 35, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (50, N'KL', NULL, NULL, N'KILO', NULL, NULL, NULL, 6, 36, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (51, N'KT', NULL, NULL, N'KIT', NULL, NULL, NULL, 6, 37, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (52, N'KTM', NULL, NULL, N'KILOMETRO', NULL, NULL, NULL, 6, 38, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (53, N'KWH', NULL, NULL, N'KILOVATIO HORA', NULL, NULL, NULL, 6, 39, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (54, N'LBR', NULL, NULL, N'LIBRAS', NULL, NULL, NULL, 6, 40, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (55, N'LEF', NULL, NULL, N'HOJA', NULL, NULL, NULL, 6, 41, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (56, N'LT', NULL, NULL, N'LITRO', NULL, NULL, NULL, 6, 42, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (57, N'LTN', NULL, NULL, N'TONELADA LARGA', NULL, NULL, NULL, 6, 43, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (58, N'MGM', NULL, NULL, N'MILIGRAMOS', NULL, NULL, NULL, 6, 44, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (59, N'MLL', NULL, NULL, N'MILLARES', NULL, NULL, NULL, 6, 45, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (60, N'MLT', NULL, NULL, N'MILILITRO', NULL, NULL, NULL, 6, 46, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (61, N'MMK', NULL, NULL, N'MILIMETRO CUADRADO', NULL, NULL, NULL, 6, 47, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (62, N'MMQ', NULL, NULL, N'MILIMETRO CUBICO', NULL, NULL, NULL, 6, 48, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (63, N'MMT', NULL, NULL, N'MILIMETRO', NULL, NULL, NULL, 6, 49, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (64, N'MTK', NULL, NULL, N'METRO CUADRADO', NULL, NULL, NULL, 6, 50, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (65, N'MTQ', NULL, NULL, N'METRO CUBICO', NULL, NULL, NULL, 6, 51, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (66, N'MTR', NULL, NULL, N'METRO', NULL, NULL, NULL, 6, 52, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (67, N'MWH', NULL, NULL, N'MEGAWATT HORA', NULL, NULL, NULL, 6, 53, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (68, N'ONZ', NULL, NULL, N'ONZAS', NULL, NULL, NULL, 6, 54, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (69, N'PF', NULL, NULL, N'PALETAS', NULL, NULL, NULL, 6, 55, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (70, N'PG', NULL, NULL, N'PLACAS', NULL, NULL, NULL, 6, 56, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (71, N'PR', NULL, NULL, N'PAR', NULL, NULL, NULL, 6, 57, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (72, N'RM', NULL, NULL, N'RESMA', NULL, NULL, NULL, 6, 58, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (73, N'SET', NULL, NULL, N'JUEGO', NULL, NULL, NULL, 6, 59, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (74, N'ST', NULL, NULL, N'PLIEGO', NULL, NULL, NULL, 6, 60, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (75, N'STN', NULL, NULL, N'TONELADA CORTA', NULL, NULL, NULL, 6, 61, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (76, N'TNE', NULL, NULL, N'TONELADAS', NULL, NULL, NULL, 6, 62, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (77, N'TU', NULL, NULL, N'TUBOS', NULL, NULL, NULL, 6, 63, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (78, N'UM', NULL, NULL, N'MILLON DE UNIDADES', NULL, NULL, NULL, 6, 64, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (79, N'UN', NULL, NULL, N'UNIDAD', NULL, NULL, NULL, 6, 65, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (80, N'YDK', NULL, NULL, N'YARDA CUADRADA', NULL, NULL, NULL, 6, 66, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (81, N'YRD', NULL, NULL, N'YARDA', NULL, NULL, NULL, 6, 67, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (82, N'1', NULL, NULL, N'mg', NULL, NULL, NULL, 7, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (83, N'2', NULL, NULL, N'g', NULL, NULL, NULL, 7, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (84, N'3', NULL, NULL, N'kg', NULL, NULL, NULL, 7, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (85, N'4', NULL, NULL, N'tonelada', NULL, NULL, NULL, 7, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (86, N'5', NULL, NULL, N'onza', NULL, NULL, NULL, 7, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (87, N'6', NULL, NULL, N'libra', NULL, NULL, NULL, 7, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (88, N'1', NULL, NULL, N'mm', NULL, NULL, NULL, 8, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (89, N'2', NULL, NULL, N'cm', NULL, NULL, NULL, 8, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (90, N'3', NULL, NULL, N'dm', NULL, NULL, NULL, 8, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (91, N'4', NULL, NULL, N'm', NULL, NULL, NULL, 8, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (92, N'5', NULL, NULL, N'pie', NULL, NULL, NULL, 8, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (93, N'6', NULL, NULL, N'pulgada', NULL, NULL, NULL, 8, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (94, N'1', NULL, NULL, N'mm²', NULL, NULL, NULL, 9, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (95, N'2', NULL, NULL, N'cm²', NULL, NULL, NULL, 9, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (96, N'3', NULL, NULL, N'dm²', NULL, NULL, NULL, 9, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (97, N'4', NULL, NULL, N'm²', NULL, NULL, NULL, 9, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (98, N'5', NULL, NULL, N'ft²', NULL, NULL, NULL, 9, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (99, N'6', NULL, NULL, N'in²', NULL, NULL, NULL, 9, NULL, N'REG')
GO
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (100, N'1', NULL, NULL, N'mm³ (µl)²', NULL, NULL, NULL, 10, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (101, N'2', NULL, NULL, N'cm³ (ml)', NULL, NULL, NULL, 10, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (102, N'3', NULL, NULL, N'dm³ (L)', NULL, NULL, NULL, 10, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (103, N'4', NULL, NULL, N'm³', NULL, NULL, NULL, 10, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (104, N'5', NULL, NULL, N'ft³', NULL, NULL, NULL, 10, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (105, N'6', NULL, NULL, N'in³', NULL, NULL, NULL, 10, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (106, N'7', NULL, NULL, N'onza', NULL, NULL, NULL, 10, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (107, N'8', NULL, NULL, N'galón', NULL, NULL, NULL, 10, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (108, N'75', NULL, NULL, N'Negro', NULL, NULL, NULL, 11, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (109, N'76', NULL, NULL, N'Inoxidables', NULL, NULL, NULL, 11, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (110, NULL, N'COMPRA', NULL, N'Compras', NULL, NULL, NULL, 12, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (111, NULL, N'VENTA', NULL, N'Ventas', NULL, NULL, NULL, 12, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (112, N'1', NULL, NULL, N'Pesona Natural', NULL, NULL, NULL, 13, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (113, N'2', NULL, NULL, N'Persona Jurídica o Entidades', NULL, NULL, NULL, 13, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (114, N'3', NULL, NULL, N'Sujeto no domiciliado', NULL, NULL, NULL, 13, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (115, N'4', NULL, NULL, N'Adquirente - Ticket', NULL, NULL, NULL, 13, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (116, N'1', NULL, NULL, N'MAYORISTA', NULL, NULL, NULL, 14, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (117, N'2', NULL, NULL, N'MINORISTA', NULL, NULL, NULL, 14, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (118, N'1', NULL, NULL, N'BODEGAS', NULL, NULL, NULL, 15, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (119, N'2', NULL, NULL, N'MERCADOS', NULL, NULL, NULL, 15, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (120, N'3', NULL, NULL, N'EMPRESAS', NULL, NULL, NULL, 15, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (121, N'4', NULL, NULL, N'FABRICAS', NULL, NULL, NULL, 15, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (122, N'5', NULL, NULL, N'AUTOSERVICIOS', NULL, NULL, NULL, 15, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (123, N'6', NULL, NULL, N'COLEGIOS', NULL, NULL, NULL, 15, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (124, N'7', NULL, NULL, N'SUCURSAL', NULL, NULL, NULL, 15, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (125, N'8', NULL, NULL, N'FARMACIAS', NULL, NULL, NULL, 15, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (126, N'9', NULL, NULL, N'INSTITUCIONES', NULL, NULL, NULL, 15, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (127, N'10', NULL, NULL, N'OTROS', NULL, NULL, NULL, 15, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (128, N'11', NULL, NULL, N'CALDEROS', NULL, NULL, NULL, 15, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (129, N'12', NULL, NULL, N'RESTAURANTES', NULL, NULL, NULL, 15, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (130, N'13', NULL, NULL, N'HOTELES', NULL, NULL, NULL, 15, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (131, N'14', NULL, NULL, N'UNIVERSIDADES', NULL, NULL, NULL, 15, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (132, N'15', NULL, NULL, N'AMBULANTES', NULL, NULL, NULL, 15, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (133, N'16', NULL, NULL, N'PANADEROS', NULL, NULL, NULL, 15, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (134, N'17', NULL, NULL, N'LICORERIAS', NULL, NULL, NULL, 15, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (135, N'18', NULL, NULL, N'KIOSKOS', NULL, NULL, NULL, 15, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (136, N'19', NULL, NULL, N'MAYORISTA', NULL, NULL, NULL, 15, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (137, N'20', NULL, NULL, N'FERRETERIA', NULL, NULL, NULL, 15, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (138, N'21', NULL, NULL, N'HOSPITALES', NULL, NULL, NULL, 15, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (139, N'22', NULL, NULL, N'PERFUMERIAS', NULL, NULL, NULL, 15, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (140, N'23', NULL, NULL, N'SERVICIOS HIGIENICOS', NULL, NULL, NULL, 15, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (141, N'24', NULL, NULL, N'INSTITUTOS', NULL, NULL, NULL, 15, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (142, N'25', NULL, NULL, N'SERVICIOS', NULL, NULL, NULL, 15, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (143, N'26', NULL, NULL, N'MINIMARKET', NULL, NULL, NULL, 15, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (144, N'27', NULL, NULL, N'CONSTRUCTORA', NULL, NULL, NULL, 15, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (145, N'28', NULL, NULL, N'TRANSPORTE', NULL, NULL, NULL, 15, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (146, N'29', NULL, NULL, N'TURISMO', NULL, NULL, NULL, 15, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (147, N'1', N'7', NULL, N'Crédito 7 días', NULL, NULL, NULL, 16, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (148, N'2', N'2', NULL, N'Credito 2 dias', NULL, NULL, NULL, 16, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (149, N'3', N'45', NULL, N'Credito 45 dias', NULL, NULL, NULL, 16, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (150, N'4', N'21', NULL, N'Credito 21 dias', NULL, NULL, NULL, 16, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (151, N'5', N'60', NULL, N'Credito 60 dias', NULL, NULL, NULL, 16, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (152, N'6', N'30', NULL, N'Crédito 30 dias', NULL, NULL, NULL, 16, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (153, N'7', N'15', NULL, N'Credito 15 dias', NULL, NULL, NULL, 16, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (154, N'8', N'4', NULL, N'Creditos 4 dias', NULL, NULL, NULL, 16, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (155, N'9', N'28', NULL, N'Creditos 28 dias', NULL, NULL, NULL, 16, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (156, N'10', N'37', NULL, N'Credito 37 dias', NULL, NULL, NULL, 16, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (157, N'11', N'46', NULL, N'Credito 46 dias', NULL, NULL, NULL, 16, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (158, N'12', N'72', NULL, N'Credito 72 dias', NULL, NULL, NULL, 16, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (159, N'1', NULL, NULL, N'EFECTIVO', NULL, NULL, NULL, 17, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (160, N'2', NULL, NULL, N'CHEQUE', NULL, NULL, NULL, 17, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (161, N'3', NULL, NULL, N'CHEQUE DIFERIDO', NULL, NULL, NULL, 17, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (162, N'4', NULL, NULL, N'DEPOSITO EN CUENTA', NULL, NULL, NULL, 17, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (163, N'5', NULL, NULL, N'TARJETA DE CREDITO', NULL, NULL, NULL, 17, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (164, N'6', NULL, NULL, N'TRANSF.A CTA.A CTA.DE TERCEROS (Cargo en Cta.)', NULL, NULL, NULL, 17, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (165, N'7', NULL, NULL, N'TRANSFERENCIAS AL EXTERIOR', NULL, NULL, NULL, 17, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (166, N'8', NULL, NULL, N'CERTIFICADO BANCARIO', NULL, NULL, NULL, 17, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (167, N'9', NULL, NULL, N'CARGO EN CUENTA', NULL, NULL, NULL, 17, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (168, N'1', NULL, NULL, N'Contado', NULL, NULL, NULL, 18, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (169, N'2', NULL, NULL, N'A la entrega', NULL, NULL, NULL, 18, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (170, N'3', NULL, NULL, N'50/50', NULL, NULL, NULL, 18, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (171, N'4', NULL, NULL, N'10 días', NULL, NULL, NULL, 18, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (172, N'5', NULL, NULL, N'10 días a fin de mes', NULL, NULL, NULL, 18, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (173, N'6', NULL, NULL, N'Orden', NULL, NULL, NULL, 18, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (174, NULL, N'PRODUCTOS', NULL, N'Productos', NULL, NULL, NULL, 12, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (175, N'1', NULL, NULL, N'Activo', NULL, NULL, NULL, 19, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (176, N'2', NULL, NULL, N'Firmado', NULL, NULL, NULL, 19, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (177, N'3', NULL, NULL, N'No Firmado', NULL, NULL, NULL, 19, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (178, N'4', NULL, NULL, N'Facturado', NULL, NULL, NULL, 19, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (179, N'01', NULL, NULL, N'Factura', NULL, NULL, NULL, 20, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (180, N'03', NULL, NULL, N'Boleta de Venta', NULL, NULL, NULL, 20, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (181, N'07', NULL, NULL, N'Nota Crédito', NULL, NULL, NULL, 20, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (182, N'08', NULL, NULL, N'Nota Débito', NULL, NULL, NULL, 20, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (183, N'10', NULL, NULL, N'Comunicación de Bajas', NULL, NULL, NULL, 20, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (184, N'09', NULL, NULL, N'Resumen Diario de Boletas', NULL, NULL, NULL, 20, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (185, N'20', NULL, NULL, N'Comprobante de Retención', NULL, NULL, NULL, 20, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (186, N'40', NULL, NULL, N'Comprobante de Percepción', NULL, NULL, NULL, 20, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (187, N'32', NULL, NULL, N'Guía de Remisión', NULL, NULL, NULL, 20, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (188, N'12', NULL, NULL, N'Ticket de máquina registradora', NULL, NULL, NULL, 20, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (189, N'0', NULL, NULL, N'Pendiente', NULL, NULL, NULL, 21, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (190, N'1', NULL, NULL, N'Autorizado', NULL, NULL, NULL, 21, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (191, N'4', NULL, NULL, N'Pendiente de aprobación', NULL, NULL, NULL, 21, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (192, N'3', NULL, NULL, N'Anulado', NULL, NULL, NULL, 21, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (193, N'2', NULL, NULL, N'Rechazado', NULL, NULL, NULL, 21, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (194, N'5', NULL, NULL, N'Erróneos', NULL, NULL, NULL, 21, NULL, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (195, N'1', N'03', NULL, N'Boleta de Venta', NULL, NULL, NULL, 22, 2, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (196, N'2', N'03', NULL, N'Boleta de Venta de anticipo', NULL, NULL, NULL, 22, 6, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (197, N'3', N'03', NULL, N'Boleta de Venta de anticipo final', NULL, NULL, NULL, 22, 7, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (198, N'4', N'01', NULL, N'Factura', NULL, NULL, NULL, 22, 1, N'REG')
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (199, N'5', N'01', NULL, N'Factura de anticipo', NULL, NULL, NULL, 22, 3, N'REG')
GO
INSERT [dbo].[MultitablaValor2] ([IdValor], [Codigo], [CodigoCorto], [CodigoLargo], [Descripcion], [DescripcionCorta], [DescripcionLarga], [Icono], [IdMultiTabla], [Orden], [EstadoRegistro]) VALUES (200, N'6', N'01', NULL, N'Factura de anticipo final', NULL, NULL, NULL, 22, 4, N'REG')
SET IDENTITY_INSERT [dbo].[MultitablaValor2] OFF
INSERT [dbo].[Permisos] ([MenuId], [UsuarioId], [PadreId]) VALUES (1, 1, 0)
INSERT [dbo].[Permisos] ([MenuId], [UsuarioId], [PadreId]) VALUES (2, 1, 0)
INSERT [dbo].[Permisos] ([MenuId], [UsuarioId], [PadreId]) VALUES (1015, 1, 0)
INSERT [dbo].[Permisos] ([MenuId], [UsuarioId], [PadreId]) VALUES (1016, 1, 1015)
INSERT [dbo].[Permisos] ([MenuId], [UsuarioId], [PadreId]) VALUES (1017, 1, 1015)
INSERT [dbo].[Permisos] ([MenuId], [UsuarioId], [PadreId]) VALUES (1018, 1, 1015)
SET IDENTITY_INSERT [dbo].[Producto] ON 

INSERT [dbo].[Producto] ([IdProducto], [Descripcion], [Precio], [Estado]) VALUES (1, N'webCam', CAST(120 AS Decimal(8, 0)), N'A')
INSERT [dbo].[Producto] ([IdProducto], [Descripcion], [Precio], [Estado]) VALUES (2, N'Disco Duro', CAST(50 AS Decimal(8, 0)), N'A')
SET IDENTITY_INSERT [dbo].[Producto] OFF
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'8348be10-7aac-453a-8e56-4ebf918062d3', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'6ad5d82c-dd16-4123-995c-1f2b42a622c7', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'65d13e9b-392a-4c39-a2fa-6f1dadbcc59d', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'c7c704bb-e1c2-4f0a-97c9-7bc3d2f33b38', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'1a77935a-2477-4177-864d-96851df90150', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'4e4df25b-141a-4337-af15-0349c2e6d5f8', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'c649a3d8-5827-44e9-8487-110d7b0c005c', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'26a4f675-35a8-4ae4-8bbb-b35824a0587a', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'fcec57c8-00de-41ee-a7ef-74b231b1e71a', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'39fc975f-dabf-4720-bb99-40c095bad8ad', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'41913f8c-17bb-46ec-b16a-cdfb7534db03', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'c4c2341b-ae6d-4c7a-a2b2-f26066f1bb47', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'46c2c64f-5246-44f5-8164-77d817139d6f', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'617e1dce-6018-473f-906b-c743d94e8950', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'3801dd2a-73fc-4c7d-9225-ae18ffbf08c4', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'73fc6323-4e99-4802-866a-848e5223f8d4', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'c562e81b-642f-46c0-9e8e-ff842391cd3c', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'7350f857-0d8b-4306-8e11-49605d433da8', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'e259ad66-2787-459d-8c83-18c96d62f146', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'9a378860-f577-40ee-9d89-cd5557602928', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'9a86eb5b-c626-4ab3-8533-e8243f89c345', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'050cfe71-4a46-48ca-a3a0-93c5fd6032e2', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'836bbfec-614e-4fbe-a8cc-e7365961fe50', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'44dfd5d6-65b0-42a6-9922-1d2ae024ce24', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'83db6d78-6239-46df-927e-71b083ab36a8', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'52520372-a50c-496c-b99e-56790fd8b44c', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'e6b5d9c4-0b8d-40be-a59f-1712b7f43659', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'f574eb8e-e0ad-4a7c-b674-2a060510c786', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'56656b82-1d4e-42c1-bb9c-66e194912f57', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'2110e890-d056-4e0a-8398-38530e5be5cf', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'ba8a0df9-57f6-4e03-ba48-17f43abb3a11', N'soporte', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'2c6b9e13-078b-4d53-a3ae-a2d2b7fb8df0', N'soporte', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'38abd619-9bf5-4b86-9870-8388b3e35fb7', N'soporte', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'5d87a847-173b-4b7d-ac77-436b7cba396a', N'soporte', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'26e468e3-5064-4300-9280-47aebc15da4f', N'soporte', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'1aa4875a-de39-4ba0-ac1c-761bce6a6ff1', N'soporte', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'1b2bb733-3c55-48ff-a1bd-345079f5b2ba', N'soporte1', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'c28beb26-0e8c-4d65-9cc7-a9a41bfeb4ee', N'soporte2', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'27f6687f-d022-4946-9789-5e3db0a76ad6', N'soporte2', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'9dec1036-32d3-45e9-a067-ce3e9885c367', N'soporte1', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'5817d468-dd7b-4018-9bfc-4d1ec9531411', N'soporte2', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'd9a1310b-9264-4da1-9b1b-4ac61cebea12', N'soporte2', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'7173af17-3bcf-48dd-8de8-9db3c7cda6ca', N'soporte1', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'02c70dc6-5964-463c-826c-7116db29a74c', N'soporte2', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'392514a3-9528-4400-9c2e-8e152355a56b', N'soporte', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'c52e4003-bdc0-415f-a6f5-3fa685600160', N'soporte', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'e326c925-dae7-4aaf-814c-fa7a6ba66280', N'soporte', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'5713db06-9e29-48ce-b528-0da54f65b19e', N'soporte', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'a9b12784-57dd-4087-bad6-2407e11316de', N'soporte', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'd947691d-6bd5-4d8e-808b-0e4722162faa', N'soporte', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'abfcea86-476c-496b-a68c-8e1499cf8941', N'soporte', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'4c159300-eb99-4373-89e1-9648c4097894', N'soporte', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'd8cc5756-1c06-4422-9ec6-7bea4672dc1c', N'soporte', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'a227c001-7193-4a26-8fd4-a4c1b6ce5165', N'soporte', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'cca9f5ed-1701-4a46-b7f6-d7fd8cfb6513', N'soporte', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'3aa9d222-d428-4a2b-9ae3-0cdb848716c4', N'soporte', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'd516c29f-535d-4ad4-8698-950cdd9e644f', N'soporte', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'37bf3d12-338d-4eb8-af68-776e48063cac', N'soporte', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'bed50636-f60d-4162-b045-852d324b9faa', N'soporte', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'ea735bba-e223-41a3-b49d-cb5a275cd88a', N'soporte', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'bde16676-8220-45c6-a04e-4dbfd3445b3d', N'soporte', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'70ffdccd-92fa-4503-b8fc-310104ae5f32', N'soporte', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'001e350c-f961-4004-92c2-d82c82160d69', N'soporte', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'cd8ef7bd-cb0e-4643-bff4-1767d5e30020', N'soporte', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'f478c333-81d2-4365-8cd3-94b8ed4f3eb7', N'soporte', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'013e8161-2e3b-4f09-bcc6-8b90cef5d9a5', N'soporte', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'05caefc4-4d93-4c4c-8453-36f9b24dddb9', N'soporte', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'c14819f4-137b-42b9-8faf-77fcd142c3b0', N'soporte', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'c9e4d2ac-32d6-4e92-a8e9-7f00a21739db', N'soporte', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'f498fe56-9865-44cd-92e6-52220eb05cbc', N'soporte', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'fef2a0e2-a70f-42cc-99ef-2aa7e1781a87', N'soporte', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'91310da0-8801-4148-b843-88b93f269cac', N'soporte', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'89124113-1895-4f10-9910-7e8490be3a58', N'soporte', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'd130d8b0-b0d5-49b8-9297-004539103282', N'soporte', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'7535669b-eee4-4ea1-84fd-c67fec2ac639', N'soporte', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'1287e593-bc6e-4da5-92a9-d367200452d2', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'9f34977d-aa7d-4c09-8d53-6396e3bce455', N'soporte', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'1f02d712-8dba-4704-a69e-23cadb0481e4', N'soporte', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'7b29d7ba-c50d-4781-a7fe-910f40a98672', N'soporte', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'10e00961-353c-4a08-92c8-f1e863d39d03', N'soporte', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'69f1a4d3-9c9a-4974-9296-b1cf4fdc1dd8', N'soporte', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'1a6e85fa-af64-4f4f-a471-3f459d0c026c', N'soporte', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'41495430-e0d6-4a23-80ef-3a38a4ff698e', N'soporte', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'b51a9e43-9fd1-4824-b099-1959e0f308a9', N'soporte', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'380834e6-f236-4ab8-a160-722ca6869f23', N'soporte', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'c7a5617f-0dce-4d67-bda5-bc2f75aa7180', N'soporte', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'766747a0-19b7-4281-9f61-44a25ca26f81', N'soporte', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'a8bc0874-1837-4cb1-af57-3e5b576e3e58', N'soporte', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'ea281aea-b916-4ec3-b60a-28c3a04a689d', N'soporte', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'82422778-b1a5-41ff-ab3a-0d595acb82c9', N'soporte', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'990d51fc-e0dd-438f-9c27-08a7b7476038', N'soporte', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'1c3ce914-2761-4d25-a332-e9536d994d3d', N'soporte', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'28114bb5-2702-4604-b8fc-7960385ab79b', N'soporte', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'97a1fdb6-acb1-4836-afb3-7922759c95a5', N'soporte', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'a2dcb237-2e76-4708-98a1-5a76241a71f7', N'soporte', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'4a81a69b-c4b2-4469-af1e-86368fc0dc92', N'soporte', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'66247a68-9746-4784-823e-17e8e148a019', N'soporte', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'd13c384c-ddca-4899-b380-014a7aa414b8', N'soporte', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'ba7fc057-9807-4448-8d2c-fe32dd24e553', N'soporte', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'9f2e4edb-b9a2-4d63-90ec-e906c3cf32a8', N'soporte', N'ANU', NULL)
GO
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'ada382fe-7a76-4a30-b1dd-be22ae3f5790', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'dc4d66b5-ce2b-4564-8cb7-041689eca86b', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'fd17f710-7b62-4a41-a12e-71b0a4f03ae1', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'e4c6a684-7047-47c9-9ea3-4f0636e4b0ac', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'03aa463e-64a3-42a5-bfde-e5f76f87868d', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'54869271-083b-4754-b795-2bdea6289147', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'24570c87-24c9-4a2c-9aef-1e8c69e666fb', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'ebeed26a-936d-4da7-818d-13e7fea41497', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'396de6ac-4d6d-42a0-9485-3f34e239049c', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'9ef9114c-3649-4c6e-8f92-2aaa24cd19ff', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'3fe57dd4-f1b4-485c-ad71-53fcd324fdf3', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'4edd01be-1fb6-4e33-b0b7-3045c6889ac6', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'c80704d1-c0c3-4fba-b06a-ddb62d3ffe41', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'31ad91df-cd08-4fd7-b5ba-2688cfbad17c', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'08f01a48-e0b3-49b2-bab0-fd76b8bf124b', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'a927fef8-8cc0-4a8c-8648-07cbba478b7e', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'a6ef6c47-e162-4057-b92b-8afedc17a973', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'cb780c75-dfbf-453a-a84e-7062e07a7bf6', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'dbe1a7ba-a943-4fbb-9de0-aadbb9e0aeab', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'326bc9c4-0121-4ee2-b3a5-e17a89dca6da', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'68398f6b-239a-4f13-af66-9d1fb10f42b5', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'3768b4c2-ffc3-400c-91cc-f7870da46862', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'78c9a0c0-a8e8-48b1-96d3-8b09f3bd263f', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'ba0d9ab7-2c38-4379-b1de-bb8fefa541fe', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'a25cc438-022f-43b1-a9e3-01d6cfc0520b', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'bc838d75-9442-457a-85f5-f17c6ab7151b', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'f3d3844a-d132-4ca4-9eed-0965800b3835', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'0d5f2fad-8ed3-45f1-bfcf-369b7628c42a', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'00b19611-0f3f-47b7-87c7-b5a96cfba818', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'29b28424-0026-4618-b5db-61e08e7a66f7', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'cd39e70f-95ee-4b5f-9f4a-99a063ca57a8', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'497ad8d4-3819-42ac-810d-7b3f51e56837', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'3ffbf455-6449-4ced-812e-825a9a059265', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'eb691917-c24b-41a0-9dec-5e9636b2c085', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'e8938c17-dd92-4a50-9bcc-524b7687541f', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'ea944469-9654-4c13-a98e-2e434f1b34b8', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'f90191f1-8e0d-4d55-bd74-ba4e13d25f46', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'38e362e2-3013-4cb5-8ad5-a300a5dd2618', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'4ccced6b-eb3d-4c79-a5e7-69425a76b991', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'b67a9ef6-4537-4691-a6ff-56aa20f305c3', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'0c518183-b875-45ad-a095-38df1b1e5fb5', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'e6517d63-60ae-4854-acf9-c45ab668d3e3', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'9a93e93b-5e3f-49b9-949e-3ff07e84335f', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'f208036d-9da2-4337-aa8c-efe17948d827', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'3ac40190-7865-4fda-b2bb-3a7227068924', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'5df856dd-2c06-430f-a5e7-d2773c6a2628', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'1ed91857-9cb4-4e75-8c3b-7d39874f481b', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'e0cd552f-93bd-4333-9884-3c3fbe67077f', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'28dcf92e-8796-4589-abaa-e563c3374eae', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'13264188-1e4f-4ab2-bb98-4ebc2f286406', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'5d8b4e48-729d-45b8-a665-935d18525e89', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'54f54f2a-93f1-4198-8eba-64d133bef1e7', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'c11e69f4-c34e-4071-8610-973da003acbc', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'603d4d7e-49b5-4dc8-8930-e86de748b3c9', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'0567f515-7a4a-4d67-ab7b-36e3bc84f230', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'c71afae7-15bb-44a4-b9df-d335ec404f13', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'1045dbac-f6ef-4bf8-8f8f-cd4cb695115d', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'2bdb0028-0da4-440e-9237-194159ff4c32', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'21504d23-6be8-409f-b8c8-f0e35a7e1229', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'6b4813d3-15c9-48df-b8f2-170ea63f80aa', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'e71b2089-1d0c-42c6-9ed4-f79d7d174faf', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'4bdb66f2-c85c-4eff-80eb-861e48ef8af4', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'fb13c42b-95a8-4985-8b93-0d0a0d31f713', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'0f251b72-63eb-471f-bc12-43b3428dd6ce', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'd1441bae-78e9-4982-bc12-a1fc54bdd00b', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'0acca1e3-8908-4d8f-af6b-3a65f6d10cf7', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'6091c803-1857-42ad-8578-82fdb3121975', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'a9eeaa48-8bc3-40f8-aa3f-108123aba7ec', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'31593573-7efa-4d7d-9642-9684669693a8', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'83b406cb-bf33-4b25-bbb4-4526ebb9c354', N'soporte', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'cd8e82d0-094f-47dc-a045-20f9d64944ec', N'soporte', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'98c2f074-3f6d-4ade-b8fa-e4ef7912ec24', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'34e93189-4d14-43b1-a389-c85d410910d9', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'db4b21af-44e9-496f-a307-e5a0fd53fc6a', N'soporte', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'9e3f58c9-f7da-4c80-a204-359bcc5972f4', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'ca0ae510-18a2-47fc-859e-8be922692bd0', N'soporte', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'1847474b-03b3-49a6-827b-121df95fb359', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'df9ee15f-3ae8-498f-92b2-23a5c7fb151e', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'033eaf64-c471-4667-901e-0164c5640b73', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'fbdc0634-a849-4ac3-be3a-eed0a7063193', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'2c502f73-4a35-4e87-b166-8bf4e56e4de5', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'ad2659f8-3bd8-412e-a015-a2b36ede5ca0', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'd5316be8-6dd0-41bd-af65-20762ec13c62', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'b8a26603-c42a-42e5-9c33-17fbe52428c8', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'07dcbd53-51c4-44c2-b3c6-40b57a04d68f', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'2afc35c2-99e7-4b6b-a0bb-d87efb372904', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'00106e35-08fe-4b3d-ada6-50bbdbf94bfa', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'1c5541dd-6b6a-4e3d-8ed7-9f6b5db27591', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'dc0e47b4-d2eb-4915-bf27-9af0d1b2867a', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'c372c6e9-c34a-4b27-821b-b4da14870806', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'95b9f884-3098-4c95-93dd-c4ced0854706', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'70b5d2e2-1ff1-423a-bca3-d41d70a9846e', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'8333b511-6ade-4731-83e3-162f6d3393ac', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'c279c1ef-42ff-4008-8cd5-8e881f6c63e4', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'a7735faf-ce54-47d8-aeb7-d3c7d911492b', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'2e8d06a3-c932-45d6-8ee1-569535dfc508', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'e4e02875-4259-4e4a-99ee-9032b0f01690', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'36c97d0f-194b-4817-b30e-c11200d213f7', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'94286ab1-542c-4801-8636-f9772a9983cf', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'bdcfc30d-487d-4fd1-809a-33d4ff06a64c', N'mpuma', N'ANU', NULL)
GO
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'9b4e1299-1ebb-491c-b54a-61eb6add1ce7', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'aeb354d9-4eae-426a-b275-e315d26b62db', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'f9e0f8be-22dd-4917-95d5-d16322d7eddf', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'501385e5-93f4-4cee-a289-f20fd9038d6a', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'034a5032-a52f-4c14-95d8-039189835d09', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'2aa83fb8-b199-48ac-8609-63088a133786', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'dbdcbf33-4b87-4518-8668-bc550b31bce7', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'cc310dec-94d9-4b69-8f0d-542b17dd3908', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'ff25516e-2ce3-43da-b862-61c935aea233', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'19a20b11-f559-45d5-a9f5-98753796da90', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'20c50632-8707-42f7-a535-17c616d72870', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'cac601c5-f28e-4357-84a6-491052cd7e99', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'2a0dd7f1-734f-4853-81fd-fd0e7e3e9c7b', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'4ab35e1b-672f-48d2-9b10-efe52b2100ac', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'1ef6a9d0-4c37-443d-bd3c-86633694b018', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'16d308a5-7300-4eb8-af37-e86cab53cd13', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'7cfb6183-f26a-44c5-8ea5-76cb3b416273', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'bfbc0cf9-7e3e-4ef9-a50f-a15f8573bbfa', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'7c002e8d-5600-404b-b0a5-d862197adf60', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'34d0d96c-55ab-49cc-be1a-a953447154a0', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'cefad27a-3294-48eb-8c3b-0d2e4b057a42', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'42e35185-f99e-41e8-85f4-e6bec582e349', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'0d295d1f-054b-4b70-aa1d-b5f86aea4095', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'0cc35866-ac86-4b29-bec1-61691a5669e4', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'ca5f8932-9c90-44b0-aef8-e9fe1eaf7ec0', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'17653a82-3a9c-49eb-bbe7-a3dc19526204', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'6042c3f0-daf1-4950-a396-85c9b8024d11', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'f254cc0e-f5f7-4723-9459-96a2e55250e3', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'a2437ac6-a970-4bbc-a995-9e318c393d75', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'98cf06d4-df23-4448-9a39-db1ed10846db', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'78901d65-9a77-461c-8499-da82c26aafc0', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'9fab2ab0-62ac-46b6-a015-64301c8ed96b', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'd5f9bad2-99e1-41b3-bb1b-b9c9e87a7b94', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'b84febef-c254-42db-9abb-8f1cf76b4cda', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'51c37e30-d41c-4248-b182-75687d11bf76', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'ffc61289-3f15-48f6-afb9-12dd6e4295b8', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'cb772db0-1087-496f-a6c5-616b774be3da', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'e781fdd4-8ef4-4e43-b6fc-e104f01e03bf', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'117d9f73-c22b-40bf-b9d0-0835cfbcd3ef', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'd815f5df-2574-4a8b-866f-0fe8cf0387b4', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'fc9b84a6-c916-403c-9c56-74a2cef01cb8', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'403538e2-ea2d-4de1-befd-1b2e1fa85533', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'1918b846-1ac0-4d86-b4af-c9db22552d4e', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'd37ec4de-7b93-4b06-bfd8-b7e3bcccfbc3', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'a4e8d494-575f-4822-b6ed-7b9eab25f965', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'd38ff57d-9ccf-4e02-abc8-97af2c2620b7', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'd3cb2414-e109-447c-b368-735005db3f39', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'52e7700b-89d9-4fff-a50b-62728786e983', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'6e7ce80b-48eb-4857-8511-81d596c85c71', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'ae0e8ebb-f140-44cc-ac0e-d8d9b41c12db', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'9994327d-86db-4a73-8217-328ad1e91d03', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'40ede32c-2399-45c2-ba14-3492acfde2ce', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'044e7925-9726-49d7-bcb0-6ad59da99400', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'41015209-0e3c-407c-923d-936e4afb297c', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'92d09d25-66c4-48e2-9508-af86dbfd191d', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'd1af5b2f-2b54-422f-b8fa-88885b7b2b4a', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'70b9d16b-b0ce-404a-8928-0da4f1b4ede1', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'1d679d2b-b68f-49cc-8d05-093f8f2dd76d', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'402b563b-617d-4f35-beaf-8c6acd99971e', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'bee6354b-bca8-4384-a0f2-e85a80c3f188', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'3b5a1d5b-8bae-41c0-8ec5-b1a110c86a34', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'adfdb443-d32e-4da0-b1cd-e2eb9b3408c1', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'b379ee04-bc37-4a30-ad9b-62c7a12b998e', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'7fbfdad1-7de2-467f-ac49-b243c5ce5aac', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'6d825c15-7f68-420a-97f3-c04170b8138b', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'e4bb7151-55f5-4c7a-ae1a-b4c03b5e3a8c', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'54ac5029-3eb3-4055-a5dc-af577da0f175', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'e7d43882-4c2e-4e2a-adbc-6902dcced5ee', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'd42faea2-9bb2-40cb-8b1f-5500e6734373', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'04f8c47b-76fe-44ce-9cde-92c34c9cb469', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'c736db9b-8b84-4675-8166-b9a763dd52ae', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'a9d568a2-939d-405f-afb1-29aa693c2ab1', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'76fd9066-ec76-442f-ada4-1dff1052ff2f', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'67b8dcec-c823-4d93-a7df-8a8f633ad3e3', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'72a178cd-96f1-40e1-804c-0d0d5d1ddae9', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'896a8203-5940-4c3a-9e39-b39f9f7b316d', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'c54150f5-c260-4386-915d-d400d35d13dc', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'fc9e2521-f300-4cce-b71e-9249245753c0', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'e7146180-1bc4-4779-9ce5-c65aaee298b8', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'7eda46f6-98cd-4d75-9ca1-b5098b1a072a', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'743f7747-01bb-45fa-a74e-57ef1dbc611e', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'87467648-700a-4972-b7d9-ded8650229f1', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'e5b22224-0611-4283-ac89-5bb9c53665f5', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'7c0747ae-455b-49f4-86fd-39339b8fccd7', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'dcc7ee91-430e-4ad0-85eb-5b5253485dc8', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'2c39bc68-c2a4-480f-b0e1-b7c009a18e0c', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'a2e9acf1-6a4d-458d-9d52-a36fc5baeb32', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'dc0cac85-dea4-430a-9278-43db6bdfbd7b', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'58f96a15-1140-434c-94f3-2ef68dc0341e', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'd3277499-4bef-45e6-b997-2b4755c6233f', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'ff1640f3-b1b7-4ebd-b77e-b29814409724', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'3ab91e7f-65c3-45fd-9c63-ebbcb06ff629', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'ea320a8f-a9ed-4dd2-adfd-bc73f66a8f82', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'a8c69390-7f40-447d-be76-022737c8ae0c', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'934533ef-2a45-43df-a3c2-65a221776e72', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'd9b12999-3bf1-4b22-ab64-4bf91ab0eecb', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'aa357a24-b139-4707-a86d-c46539f2a01b', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'82a43638-4386-42fe-951c-d183bc4dab4a', N'mpuma ', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'132d601f-6f81-4081-9f6c-7f101c5a226d', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'0ebe0323-268f-4fd3-8347-51195ee0f1ca', N'mpuma', N'ANU', NULL)
GO
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'686725a0-156c-41d8-88b0-c2b607e1e12c', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'b6d94fd9-c4ff-462c-b3f6-02ee1b0f0764', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'9683b7d2-968c-4740-bc0a-83786b99f597', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'e0f60562-9bc5-4881-aee7-61371dc6c372', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'8b001dfe-6696-4078-be43-3042a92df044', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'8995b5a4-f045-47ba-81b1-2a71d133a5f7', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'0f096d05-4723-4810-be0a-19efc27d4017', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'1f80d584-ea98-422e-a20d-82b884d8f07c', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'b5de4699-3e55-4286-b269-2b32bdb3ca1b', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'a7b11691-2963-42fc-869c-1a11a95394d0', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'a0ddf0c8-9289-408d-a9c1-e538cb4a6b3c', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'9ea6b34a-8e95-41e0-9457-4f7857dd42be', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'1295f519-14c2-4482-bb05-b9ed6c219eb1', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'ad7b7a45-74a5-40d6-bd2c-2be3d244bc5f', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'bb94b0a4-550e-40e8-96e2-204c479acd3d', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'59c6fc36-2ccf-4c3b-9849-331635d73643', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'12f539b3-1668-490e-a88b-b921bc162a5e', N'mpuma', N'REG', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'4683758c-acfa-422f-93bb-52e1ada9087d', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'02d7e66b-7973-4649-a278-31c72409dfb6', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'60455d0a-c16e-48f9-8200-b4b0e8ba102f', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'dbaba161-ce60-4a0c-88f1-5c1918c99eb4', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'11852d9b-484e-4676-9298-1c6b20609309', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'51c53949-7fdb-44a2-8db5-8fe34930448b', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'55f9a24a-4063-4a69-bf9e-916de3487e0c', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'5cd09807-870c-4078-9674-e7e83963586c', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'a3ad00a5-2d81-4227-8e2c-f88cb1740557', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'84ce55ae-6c5f-4ac6-b224-65df702e53ce', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'3988bc7f-b5f4-4dd3-b356-2a74c39c061b', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Token] ([GUIDToken], [CodigoUsuario], [EstadoRegistro], [FechaRegistro]) VALUES (N'e49e5079-f1c8-4243-8160-674995d32c40', N'mpuma', N'ANU', NULL)
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'00', N'00', N'AMAZONAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'01', N'00', N'CHACHAPOYAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'01', N'01', N'CHACHAPOYAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'01', N'02', N'ASUNCION')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'01', N'03', N'BALSAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'01', N'04', N'CHETO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'01', N'05', N'CHILIQUIN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'01', N'06', N'CHUQUIBAMBA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'01', N'07', N'GRANADA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'01', N'08', N'HUANCAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'01', N'09', N'LA JALCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'01', N'10', N'LEIMEBAMBA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'01', N'11', N'LEVANTO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'01', N'12', N'MAGDALENA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'01', N'13', N'MARISCAL CASTILLA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'01', N'14', N'MOLINOPAMPA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'01', N'15', N'MONTEVIDEO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'01', N'16', N'OLLEROS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'01', N'17', N'QUINJALCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'01', N'18', N'SAN FRANCISCO DE DAGUAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'01', N'19', N'SAN ISIDRO DE MAINO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'01', N'20', N'SOLOCO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'01', N'21', N'SONCHE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'02', N'00', N'BAGUA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'02', N'01', N'BAGUA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'02', N'02', N'ARAMANGO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'02', N'03', N'COPALLIN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'02', N'04', N'EL PARCO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'02', N'05', N'IMAZA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'02', N'06', N'LA PECA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'03', N'00', N'BONGARA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'03', N'01', N'JUMBILLA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'03', N'02', N'CHISQUILLA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'03', N'03', N'CHURUJA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'03', N'04', N'COROSHA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'03', N'05', N'CUISPES')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'03', N'06', N'FLORIDA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'03', N'07', N'JAZAN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'03', N'08', N'RECTA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'03', N'09', N'SAN CARLOS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'03', N'10', N'SHIPASBAMBA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'03', N'11', N'VALERA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'03', N'12', N'YAMBRASBAMBA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'04', N'00', N'CONDORCANQUI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'04', N'01', N'NIEVA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'04', N'02', N'EL CENEPA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'04', N'03', N'RIO SANTIAGO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'05', N'00', N'LUYA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'05', N'01', N'LAMUD')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'05', N'02', N'CAMPORREDONDO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'05', N'03', N'COCABAMBA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'05', N'04', N'COLCAMAR')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'05', N'05', N'CONILA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'05', N'06', N'INGUILPATA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'05', N'07', N'LONGUITA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'05', N'08', N'LONYA CHICO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'05', N'09', N'LUYA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'05', N'10', N'LUYA VIEJO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'05', N'11', N'MARIA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'05', N'12', N'OCALLI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'05', N'13', N'OCUMAL')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'05', N'14', N'PISUQUIA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'05', N'15', N'PROVIDENCIA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'05', N'16', N'SAN CRISTOBAL')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'05', N'17', N'SAN FRANCISCO DE YESO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'05', N'18', N'SAN JERONIMO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'05', N'19', N'SAN JUAN DE LOPECANCHA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'05', N'20', N'SANTA CATALINA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'05', N'21', N'SANTO TOMAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'05', N'22', N'TINGO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'05', N'23', N'TRITA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'06', N'00', N'RODRIGUEZ DE MENDOZA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'06', N'01', N'SAN NICOLAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'06', N'02', N'CHIRIMOTO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'06', N'03', N'COCHAMAL')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'06', N'04', N'HUAMBO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'06', N'05', N'LIMABAMBA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'06', N'06', N'LONGAR')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'06', N'07', N'MARISCAL BENAVIDES')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'06', N'08', N'MILPUC')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'06', N'09', N'OMIA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'06', N'10', N'SANTA ROSA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'06', N'11', N'TOTORA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'06', N'12', N'VISTA ALEGRE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'07', N'00', N'UTCUBAMBA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'07', N'01', N'BAGUA GRANDE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'07', N'02', N'CAJARURO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'07', N'03', N'CUMBA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'07', N'04', N'EL MILAGRO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'07', N'05', N'JAMALCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'07', N'06', N'LONYA GRANDE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'01', N'07', N'07', N'YAMON')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'00', N'00', N'ANCASH')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'01', N'00', N'HUARAZ')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'01', N'01', N'HUARAZ')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'01', N'02', N'COCHABAMBA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'01', N'03', N'COLCABAMBA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'01', N'04', N'HUANCHAY')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'01', N'05', N'INDEPENDENCIA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'01', N'06', N'JANGAS')
GO
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'01', N'07', N'LA LIBERTAD')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'01', N'08', N'OLLEROS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'01', N'09', N'PAMPAS GRANDE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'01', N'10', N'PARIACOTO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'01', N'11', N'PIRA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'01', N'12', N'TARICA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'02', N'00', N'AIJA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'02', N'01', N'AIJA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'02', N'02', N'CORIS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'02', N'03', N'HUACLLAN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'02', N'04', N'LA MERCED')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'02', N'05', N'SUCCHA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'03', N'00', N'ANTONIO RAYMONDI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'03', N'01', N'LLAMELLIN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'03', N'02', N'ACZO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'03', N'03', N'CHACCHO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'03', N'04', N'CHINGAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'03', N'05', N'MIRGAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'03', N'06', N'SAN JUAN DE RONTOY')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'04', N'00', N'ASUNCION')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'04', N'01', N'CHACAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'04', N'02', N'ACOCHACA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'05', N'00', N'BOLOGNESI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'05', N'01', N'CHIQUIAN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'05', N'02', N'ABELARDO PARDO LEZAMETA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'05', N'03', N'ANTONIO RAYMONDI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'05', N'04', N'AQUIA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'05', N'05', N'CAJACAY')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'05', N'06', N'CANIS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'05', N'07', N'COLQUIOC')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'05', N'08', N'HUALLANCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'05', N'09', N'HUASTA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'05', N'10', N'HUAYLLACAYAN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'05', N'11', N'LA PRIMAVERA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'05', N'12', N'MANGAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'05', N'13', N'PACLLON')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'05', N'14', N'SAN MIGUEL DE CORPANQUI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'05', N'15', N'TICLLOS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'06', N'00', N'CARHUAZ')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'06', N'01', N'CARHUAZ')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'06', N'02', N'ACOPAMPA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'06', N'03', N'AMASHCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'06', N'04', N'ANTA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'06', N'05', N'ATAQUERO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'06', N'06', N'MARCARA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'06', N'07', N'PARIAHUANCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'06', N'08', N'SAN MIGUEL DE ACO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'06', N'09', N'SHILLA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'06', N'10', N'TINCO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'06', N'11', N'YUNGAR')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'07', N'00', N'CARLOS FERMIN FITZCARRALD')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'07', N'01', N'SAN LUIS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'07', N'02', N'SAN NICOLAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'07', N'03', N'YAUYA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'08', N'00', N'CASMA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'08', N'01', N'CASMA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'08', N'02', N'BUENA VISTA ALTA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'08', N'03', N'COMANDANTE NOEL')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'08', N'04', N'YAUTAN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'09', N'00', N'CORONGO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'09', N'01', N'CORONGO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'09', N'02', N'ACO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'09', N'03', N'BAMBAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'09', N'04', N'CUSCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'09', N'05', N'LA PAMPA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'09', N'06', N'YANAC')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'09', N'07', N'YUPAN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'10', N'00', N'HUARI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'10', N'01', N'HUARI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'10', N'02', N'ANRA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'10', N'03', N'CAJAY')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'10', N'04', N'CHAVIN DE HUANTAR')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'10', N'05', N'HUACACHI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'10', N'06', N'HUACCHIS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'10', N'07', N'HUACHIS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'10', N'08', N'HUANTAR')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'10', N'09', N'MASIN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'10', N'10', N'PAUCAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'10', N'11', N'PONTO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'10', N'12', N'RAHUAPAMPA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'10', N'13', N'RAPAYAN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'10', N'14', N'SAN MARCOS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'10', N'15', N'SAN PEDRO DE CHANA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'10', N'16', N'UCO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'11', N'00', N'HUARMEY')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'11', N'01', N'HUARMEY')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'11', N'02', N'COCHAPETI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'11', N'03', N'CULEBRAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'11', N'04', N'HUAYAN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'11', N'05', N'MALVAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'12', N'00', N'HUAYLAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'12', N'01', N'CARAZ')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'12', N'02', N'HUALLANCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'12', N'03', N'HUATA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'12', N'04', N'HUAYLAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'12', N'05', N'MATO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'12', N'06', N'PAMPAROMAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'12', N'07', N'PUEBLO LIBRE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'12', N'08', N'SANTA CRUZ')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'12', N'09', N'SANTO TORIBIO')
GO
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'12', N'10', N'YURACMARCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'13', N'00', N'MARISCAL LUZURIAGA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'13', N'01', N'PISCOBAMBA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'13', N'02', N'CASCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'13', N'03', N'ELEAZAR GUZMAN BARRON')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'13', N'04', N'FIDEL OLIVAS ESCUDERO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'13', N'05', N'LLAMA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'13', N'06', N'LLUMPA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'13', N'07', N'LUCMA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'13', N'08', N'MUSGA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'14', N'00', N'OCROS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'14', N'01', N'OCROS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'14', N'02', N'ACAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'14', N'03', N'CAJAMARQUILLA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'14', N'04', N'CARHUAPAMPA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'14', N'05', N'COCHAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'14', N'06', N'CONGAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'14', N'07', N'LLIPA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'14', N'08', N'SAN CRISTOBAL DE RAJAN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'14', N'09', N'SAN PEDRO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'14', N'10', N'SANTIAGO DE CHILCAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'15', N'00', N'PALLASCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'15', N'01', N'CABANA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'15', N'02', N'BOLOGNESI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'15', N'03', N'CONCHUCOS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'15', N'04', N'HUACASCHUQUE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'15', N'05', N'HUANDOVAL')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'15', N'06', N'LACABAMBA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'15', N'07', N'LLAPO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'15', N'08', N'PALLASCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'15', N'09', N'PAMPAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'15', N'10', N'SANTA ROSA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'15', N'11', N'TAUCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'16', N'00', N'POMABAMBA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'16', N'01', N'POMABAMBA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'16', N'02', N'HUAYLLAN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'16', N'03', N'PAROBAMBA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'16', N'04', N'QUINUABAMBA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'17', N'00', N'RECUAY')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'17', N'01', N'RECUAY')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'17', N'02', N'CATAC')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'17', N'03', N'COTAPARACO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'17', N'04', N'HUAYLLAPAMPA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'17', N'05', N'LLACLLIN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'17', N'06', N'MARCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'17', N'07', N'PAMPAS CHICO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'17', N'08', N'PARARIN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'17', N'09', N'TAPACOCHA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'17', N'10', N'TICAPAMPA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'18', N'00', N'SANTA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'18', N'01', N'CHIMBOTE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'18', N'02', N'CACERES DEL PERU')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'18', N'03', N'COISHCO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'18', N'04', N'MACATE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'18', N'05', N'MORO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'18', N'06', N'NEPEÑA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'18', N'07', N'SAMANCO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'18', N'08', N'SANTA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'18', N'09', N'NUEVO CHIMBOTE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'19', N'00', N'SIHUAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'19', N'01', N'SIHUAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'19', N'02', N'ACOBAMBA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'19', N'03', N'ALFONSO UGARTE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'19', N'04', N'CASHAPAMPA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'19', N'05', N'CHINGALPO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'19', N'06', N'HUAYLLABAMBA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'19', N'07', N'QUICHES')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'19', N'08', N'RAGASH')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'19', N'09', N'SAN JUAN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'19', N'10', N'SICSIBAMBA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'20', N'00', N'YUNGAY')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'20', N'01', N'YUNGAY')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'20', N'02', N'CASCAPARA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'20', N'03', N'MANCOS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'20', N'04', N'MATACOTO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'20', N'05', N'QUILLO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'20', N'06', N'RANRAHIRCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'20', N'07', N'SHUPLUY')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'02', N'20', N'08', N'YANAMA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'00', N'00', N'APURIMAC')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'01', N'00', N'ABANCAY')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'01', N'01', N'ABANCAY')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'01', N'02', N'CHACOCHE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'01', N'03', N'CIRCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'01', N'04', N'CURAHUASI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'01', N'05', N'HUANIPACA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'01', N'06', N'LAMBRAMA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'01', N'07', N'PICHIRHUA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'01', N'08', N'SAN PEDRO DE CACHORA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'01', N'09', N'TAMBURCO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'02', N'00', N'ANDAHUAYLAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'02', N'01', N'ANDAHUAYLAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'02', N'02', N'ANDARAPA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'02', N'03', N'CHIARA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'02', N'04', N'HUANCARAMA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'02', N'05', N'HUANCARAY')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'02', N'06', N'HUAYANA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'02', N'07', N'KISHUARA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'02', N'08', N'PACOBAMBA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'02', N'09', N'PACUCHA')
GO
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'02', N'10', N'PAMPACHIRI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'02', N'11', N'POMACOCHA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'02', N'12', N'SAN ANTONIO DE CACHI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'02', N'13', N'SAN JERONIMO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'02', N'14', N'SAN MIGUEL DE CHACCRAMPA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'02', N'15', N'SANTA MARIA DE CHICMO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'02', N'16', N'TALAVERA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'02', N'17', N'TUMAY HUARACA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'02', N'18', N'TURPO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'02', N'19', N'KAQUIABAMBA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'02', N'20', N'JOSE MARIA ARGUEDAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'03', N'00', N'ANTABAMBA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'03', N'01', N'ANTABAMBA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'03', N'02', N'EL ORO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'03', N'03', N'HUAQUIRCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'03', N'04', N'JUAN ESPINOZA MEDRANO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'03', N'05', N'OROPESA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'03', N'06', N'PACHACONAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'03', N'07', N'SABAINO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'04', N'00', N'AYMARAES')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'04', N'01', N'CHALHUANCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'04', N'02', N'CAPAYA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'04', N'03', N'CARAYBAMBA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'04', N'04', N'CHAPIMARCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'04', N'05', N'COLCABAMBA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'04', N'06', N'COTARUSE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'04', N'07', N'HUAYLLO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'04', N'08', N'JUSTO APU SAHUARAURA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'04', N'09', N'LUCRE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'04', N'10', N'POCOHUANCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'04', N'11', N'SAN JUAN DE CHACÑA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'04', N'12', N'SAÑAYCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'04', N'13', N'SORAYA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'04', N'14', N'TAPAIRIHUA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'04', N'15', N'TINTAY')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'04', N'16', N'TORAYA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'04', N'17', N'YANACA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'05', N'00', N'COTABAMBAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'05', N'01', N'TAMBOBAMBA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'05', N'02', N'COTABAMBAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'05', N'03', N'COYLLURQUI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'05', N'04', N'HAQUIRA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'05', N'05', N'MARA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'05', N'06', N'CHALLHUAHUACHO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'06', N'00', N'CHINCHEROS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'06', N'01', N'CHINCHEROS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'06', N'02', N'ANCO_HUALLO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'06', N'03', N'COCHARCAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'06', N'04', N'HUACCANA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'06', N'05', N'OCOBAMBA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'06', N'06', N'ONGOY')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'06', N'07', N'URANMARCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'06', N'08', N'RANRACANCHA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'06', N'09', N'ROCCHACC')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'06', N'10', N'EL PORVENIR')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'07', N'00', N'GRAU')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'07', N'01', N'CHUQUIBAMBILLA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'07', N'02', N'CURPAHUASI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'07', N'03', N'GAMARRA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'07', N'04', N'HUAYLLATI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'07', N'05', N'MAMARA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'07', N'06', N'MICAELA BASTIDAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'07', N'07', N'PATAYPAMPA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'07', N'08', N'PROGRESO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'07', N'09', N'SAN ANTONIO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'07', N'10', N'SANTA ROSA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'07', N'11', N'TURPAY')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'07', N'12', N'VILCABAMBA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'07', N'13', N'VIRUNDO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'03', N'07', N'14', N'CURASCO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'00', N'00', N'AREQUIPA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'01', N'00', N'AREQUIPA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'01', N'01', N'AREQUIPA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'01', N'02', N'ALTO SELVA ALEGRE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'01', N'03', N'CAYMA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'01', N'04', N'CERRO COLORADO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'01', N'05', N'CHARACATO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'01', N'06', N'CHIGUATA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'01', N'07', N'JACOBO HUNTER')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'01', N'08', N'LA JOYA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'01', N'09', N'MARIANO MELGAR')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'01', N'10', N'MIRAFLORES')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'01', N'11', N'MOLLEBAYA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'01', N'12', N'PAUCARPATA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'01', N'13', N'POCSI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'01', N'14', N'POLOBAYA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'01', N'15', N'QUEQUEÑA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'01', N'16', N'SABANDIA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'01', N'17', N'SACHACA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'01', N'18', N'SAN JUAN DE SIGUAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'01', N'19', N'SAN JUAN DE TARUCANI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'01', N'20', N'SANTA ISABEL DE SIGUAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'01', N'21', N'SANTA RITA DE SIGUAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'01', N'22', N'SOCABAYA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'01', N'23', N'TIABAYA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'01', N'24', N'UCHUMAYO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'01', N'25', N'VITOR')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'01', N'26', N'YANAHUARA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'01', N'27', N'YARABAMBA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'01', N'28', N'YURA')
GO
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'01', N'29', N'JOSE LUIS BUSTAMANTE Y RIVERO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'02', N'00', N'CAMANA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'02', N'01', N'CAMANA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'02', N'02', N'JOSE MARIA QUIMPER')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'02', N'03', N'MARIANO NICOLAS VALCARCEL')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'02', N'04', N'MARISCAL CACERES')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'02', N'05', N'NICOLAS DE PIEROLA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'02', N'06', N'OCOÑA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'02', N'07', N'QUILCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'02', N'08', N'SAMUEL PASTOR')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'03', N'00', N'CARAVELI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'03', N'01', N'CARAVELI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'03', N'02', N'ACARI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'03', N'03', N'ATICO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'03', N'04', N'ATIQUIPA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'03', N'05', N'BELLA UNION')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'03', N'06', N'CAHUACHO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'03', N'07', N'CHALA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'03', N'08', N'CHAPARRA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'03', N'09', N'HUANUHUANU')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'03', N'10', N'JAQUI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'03', N'11', N'LOMAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'03', N'12', N'QUICACHA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'03', N'13', N'YAUCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'04', N'00', N'CASTILLA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'04', N'01', N'APLAO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'04', N'02', N'ANDAGUA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'04', N'03', N'AYO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'04', N'04', N'CHACHAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'04', N'05', N'CHILCAYMARCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'04', N'06', N'CHOCO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'04', N'07', N'HUANCARQUI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'04', N'08', N'MACHAGUAY')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'04', N'09', N'ORCOPAMPA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'04', N'10', N'PAMPACOLCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'04', N'11', N'TIPAN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'04', N'12', N'UÑON')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'04', N'13', N'URACA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'04', N'14', N'VIRACO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'05', N'00', N'CAYLLOMA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'05', N'01', N'CHIVAY')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'05', N'03', N'CABANACONDE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'05', N'04', N'CALLALLI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'05', N'05', N'CAYLLOMA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'05', N'06', N'COPORAQUE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'05', N'07', N'HUAMBO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'05', N'08', N'HUANCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'05', N'09', N'ICHUPAMPA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'05', N'10', N'LARI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'05', N'11', N'LLUTA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'05', N'12', N'MACA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'05', N'13', N'MADRIGAL')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'05', N'14', N'SAN ANTONIO DE CHUCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'05', N'15', N'SIBAYO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'05', N'16', N'TAPAY')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'05', N'17', N'TISCO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'05', N'18', N'TUTI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'05', N'19', N'YANQUE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'05', N'20', N'MAJES')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'06', N'00', N'CONDESUYOS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'06', N'01', N'CHUQUIBAMBA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'06', N'02', N'ANDARAY')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'06', N'03', N'CAYARANI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'06', N'04', N'CHICHAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'06', N'05', N'IRAY')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'06', N'06', N'RIO GRANDE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'06', N'07', N'SALAMANCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'06', N'08', N'YANAQUIHUA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'07', N'00', N'ISLAY')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'07', N'01', N'MOLLENDO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'07', N'02', N'COCACHACRA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'07', N'03', N'DEAN VALDIVIA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'07', N'04', N'ISLAY')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'07', N'05', N'MEJIA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'07', N'06', N'PUNTA DE BOMBON')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'08', N'00', N'LA UNIÒN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'08', N'01', N'COTAHUASI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'08', N'02', N'ALCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'08', N'03', N'CHARCANA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'08', N'04', N'HUAYNACOTAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'08', N'05', N'PAMPAMARCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'08', N'06', N'PUYCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'08', N'07', N'QUECHUALLA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'08', N'08', N'SAYLA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'08', N'09', N'TAURIA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'08', N'10', N'TOMEPAMPA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'04', N'08', N'11', N'TORO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'00', N'00', N'AYACUCHO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'01', N'00', N'HUAMANGA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'01', N'01', N'AYACUCHO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'01', N'02', N'ACOCRO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'01', N'03', N'ACOS VINCHOS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'01', N'04', N'CARMEN ALTO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'01', N'05', N'CHIARA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'01', N'06', N'OCROS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'01', N'07', N'PACAYCASA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'01', N'08', N'QUINUA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'01', N'09', N'SAN JOSE DE TICLLAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'01', N'10', N'SAN JUAN BAUTISTA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'01', N'11', N'SANTIAGO DE PISCHA')
GO
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'01', N'12', N'SOCOS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'01', N'13', N'TAMBILLO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'01', N'14', N'VINCHOS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'01', N'15', N'JESUS NAZARENO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'01', N'16', N'ANDRES AVELINO CACERES DORREGARAY')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'02', N'00', N'CANGALLO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'02', N'01', N'CANGALLO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'02', N'02', N'CHUSCHI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'02', N'03', N'LOS MOROCHUCOS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'02', N'04', N'MARIA PARADO DE BELLIDO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'02', N'05', N'PARAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'02', N'06', N'TOTOS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'03', N'00', N'HUANCA SANCOS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'03', N'01', N'SANCOS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'03', N'02', N'CARAPO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'03', N'03', N'SACSAMARCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'03', N'04', N'SANTIAGO DE LUCANAMARCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'04', N'00', N'HUANTA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'04', N'01', N'HUANTA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'04', N'02', N'AYAHUANCO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'04', N'03', N'HUAMANGUILLA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'04', N'04', N'IGUAIN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'04', N'05', N'LURICOCHA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'04', N'06', N'SANTILLANA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'04', N'07', N'SIVIA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'04', N'08', N'LLOCHEGUA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'04', N'09', N'CANAYRE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'04', N'10', N'UCHURACCAY')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'04', N'11', N'PUCACOLPA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'04', N'12', N'CHACA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'05', N'00', N'LA MAR')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'05', N'01', N'SAN MIGUEL')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'05', N'02', N'ANCO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'05', N'03', N'AYNA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'05', N'04', N'CHILCAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'05', N'05', N'CHUNGUI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'05', N'06', N'LUIS CARRANZA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'05', N'07', N'SANTA ROSA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'05', N'08', N'TAMBO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'05', N'09', N'SAMUGARI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'05', N'10', N'ANCHIHUAY')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'06', N'00', N'LUCANAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'06', N'01', N'PUQUIO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'06', N'02', N'AUCARA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'06', N'03', N'CABANA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'06', N'04', N'CARMEN SALCEDO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'06', N'05', N'CHAVIÑA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'06', N'06', N'CHIPAO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'06', N'07', N'HUAC-HUAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'06', N'08', N'LARAMATE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'06', N'09', N'LEONCIO PRADO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'06', N'10', N'LLAUTA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'06', N'11', N'LUCANAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'06', N'12', N'OCAÑA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'06', N'13', N'OTOCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'06', N'14', N'SAISA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'06', N'15', N'SAN CRISTOBAL')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'06', N'16', N'SAN JUAN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'06', N'17', N'SAN PEDRO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'06', N'18', N'SAN PEDRO DE PALCO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'06', N'19', N'SANCOS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'06', N'20', N'SANTA ANA DE HUAYCAHUACHO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'06', N'21', N'SANTA LUCIA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'07', N'00', N'PARINACOCHAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'07', N'01', N'CORACORA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'07', N'02', N'CHUMPI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'07', N'03', N'CORONEL CASTAÑEDA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'07', N'04', N'PACAPAUSA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'07', N'05', N'PULLO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'07', N'06', N'PUYUSCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'07', N'07', N'SAN FRANCISCO DE RAVACAYCO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'07', N'08', N'UPAHUACHO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'08', N'00', N'PÀUCAR DEL SARA SARA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'08', N'01', N'PAUSA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'08', N'02', N'COLTA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'08', N'03', N'CORCULLA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'08', N'04', N'LAMPA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'08', N'05', N'MARCABAMBA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'08', N'06', N'OYOLO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'08', N'07', N'PARARCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'08', N'08', N'SAN JAVIER DE ALPABAMBA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'08', N'09', N'SAN JOSE DE USHUA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'08', N'10', N'SARA SARA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'09', N'00', N'SUCRE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'09', N'01', N'QUEROBAMBA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'09', N'02', N'BELEN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'09', N'03', N'CHALCOS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'09', N'04', N'CHILCAYOC')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'09', N'05', N'HUACAÑA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'09', N'06', N'MORCOLLA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'09', N'07', N'PAICO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'09', N'08', N'SAN PEDRO DE LARCAY')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'09', N'09', N'SAN SALVADOR DE QUIJE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'09', N'10', N'SANTIAGO DE PAUCARAY')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'09', N'11', N'SORAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'10', N'00', N'VICTOR FAJARDO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'10', N'01', N'HUANCAPI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'10', N'02', N'ALCAMENCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'10', N'03', N'APONGO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'10', N'04', N'ASQUIPATA')
GO
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'10', N'05', N'CANARIA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'10', N'06', N'CAYARA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'10', N'07', N'COLCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'10', N'08', N'HUAMANQUIQUIA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'10', N'09', N'HUANCARAYLLA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'10', N'10', N'HUAYA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'10', N'11', N'SARHUA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'10', N'12', N'VILCANCHOS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'11', N'00', N'VILCAS HUAMAN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'11', N'01', N'VILCAS HUAMAN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'11', N'02', N'ACCOMARCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'11', N'03', N'CARHUANCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'11', N'04', N'CONCEPCION')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'11', N'05', N'HUAMBALPA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'11', N'06', N'INDEPENDENCIA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'11', N'07', N'SAURAMA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'05', N'11', N'08', N'VISCHONGO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'00', N'00', N'CAJAMARCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'01', N'00', N'CAJAMARCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'01', N'01', N'CAJAMARCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'01', N'02', N'ASUNCION')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'01', N'03', N'CHETILLA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'01', N'04', N'COSPAN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'01', N'05', N'ENCAÑADA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'01', N'06', N'JESUS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'01', N'07', N'LLACANORA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'01', N'08', N'LOS BAÑOS DEL INCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'01', N'09', N'MAGDALENA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'01', N'10', N'MATARA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'01', N'11', N'NAMORA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'01', N'12', N'SAN JUAN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'02', N'00', N'CAJABAMBA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'02', N'01', N'CAJABAMBA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'02', N'02', N'CACHACHI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'02', N'03', N'CONDEBAMBA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'02', N'04', N'SITACOCHA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'03', N'00', N'CELENDIN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'03', N'01', N'CELENDIN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'03', N'02', N'CHUMUCH')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'03', N'03', N'CORTEGANA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'03', N'04', N'HUASMIN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'03', N'05', N'JORGE CHAVEZ')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'03', N'06', N'JOSE GALVEZ')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'03', N'07', N'MIGUEL IGLESIAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'03', N'08', N'OXAMARCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'03', N'09', N'SOROCHUCO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'03', N'10', N'SUCRE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'03', N'11', N'UTCO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'03', N'12', N'LA LIBERTAD DE PALLAN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'04', N'00', N'CHOTA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'04', N'01', N'CHOTA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'04', N'02', N'ANGUIA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'04', N'03', N'CHADIN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'04', N'04', N'CHIGUIRIP')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'04', N'05', N'CHIMBAN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'04', N'06', N'CHOROPAMPA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'04', N'07', N'COCHABAMBA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'04', N'08', N'CONCHAN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'04', N'09', N'HUAMBOS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'04', N'10', N'LAJAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'04', N'11', N'LLAMA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'04', N'12', N'MIRACOSTA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'04', N'13', N'PACCHA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'04', N'14', N'PION')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'04', N'15', N'QUEROCOTO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'04', N'16', N'SAN JUAN DE LICUPIS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'04', N'17', N'TACABAMBA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'04', N'18', N'TOCMOCHE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'04', N'19', N'CHALAMARCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'05', N'00', N'CONTUMAZA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'05', N'01', N'CONTUMAZA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'05', N'02', N'CHILETE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'05', N'03', N'CUPISNIQUE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'05', N'04', N'GUZMANGO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'05', N'05', N'SAN BENITO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'05', N'06', N'SANTA CRUZ DE TOLEDO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'05', N'07', N'TANTARICA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'05', N'08', N'YONAN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'06', N'00', N'CUTERVO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'06', N'01', N'CUTERVO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'06', N'02', N'CALLAYUC')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'06', N'03', N'CHOROS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'06', N'04', N'CUJILLO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'06', N'05', N'LA RAMADA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'06', N'06', N'PIMPINGOS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'06', N'07', N'QUEROCOTILLO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'06', N'08', N'SAN ANDRES DE CUTERVO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'06', N'09', N'SAN JUAN DE CUTERVO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'06', N'10', N'SAN LUIS DE LUCMA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'06', N'11', N'SANTA CRUZ')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'06', N'12', N'SANTO DOMINGO DE LA CAPILLA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'06', N'13', N'SANTO TOMAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'06', N'14', N'SOCOTA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'06', N'15', N'TORIBIO CASANOVA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'07', N'00', N'HUALGAYOC')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'07', N'01', N'BAMBAMARCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'07', N'02', N'CHUGUR')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'07', N'03', N'HUALGAYOC')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'08', N'00', N'JAEN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'08', N'01', N'JAEN')
GO
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'08', N'02', N'BELLAVISTA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'08', N'03', N'CHONTALI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'08', N'04', N'COLASAY')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'08', N'05', N'HUABAL')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'08', N'06', N'LAS PIRIAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'08', N'07', N'POMAHUACA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'08', N'08', N'PUCARA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'08', N'09', N'SALLIQUE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'08', N'10', N'SAN FELIPE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'08', N'11', N'SAN JOSE DEL ALTO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'08', N'12', N'SANTA ROSA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'09', N'00', N'SAN IGNACIO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'09', N'01', N'SAN IGNACIO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'09', N'02', N'CHIRINOS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'09', N'03', N'HUARANGO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'09', N'04', N'LA COIPA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'09', N'05', N'NAMBALLE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'09', N'06', N'SAN JOSE DE LOURDES')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'09', N'07', N'TABACONAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'10', N'00', N'SAN MARCOS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'10', N'01', N'PEDRO GALVEZ')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'10', N'02', N'CHANCAY')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'10', N'03', N'EDUARDO VILLANUEVA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'10', N'04', N'GREGORIO PITA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'10', N'05', N'ICHOCAN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'10', N'06', N'JOSE MANUEL QUIROZ')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'10', N'07', N'JOSE SABOGAL')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'11', N'00', N'SAN MIGUEL')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'11', N'01', N'SAN MIGUEL')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'11', N'02', N'BOLIVAR')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'11', N'03', N'CALQUIS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'11', N'04', N'CATILLUC')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'11', N'05', N'EL PRADO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'11', N'06', N'LA FLORIDA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'11', N'07', N'LLAPA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'11', N'08', N'NANCHOC')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'11', N'09', N'NIEPOS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'11', N'10', N'SAN GREGORIO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'11', N'11', N'SAN SILVESTRE DE COCHAN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'11', N'12', N'TONGOD')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'11', N'13', N'UNION AGUA BLANCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'12', N'00', N'SAN PABLO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'12', N'01', N'SAN PABLO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'12', N'02', N'SAN BERNARDINO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'12', N'03', N'SAN LUIS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'12', N'04', N'TUMBADEN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'13', N'00', N'SANTA CRUZ')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'13', N'01', N'SANTA CRUZ')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'13', N'02', N'ANDABAMBA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'13', N'03', N'CATACHE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'13', N'04', N'CHANCAYBAÑOS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'13', N'05', N'LA ESPERANZA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'13', N'06', N'NINABAMBA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'13', N'07', N'PULAN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'13', N'08', N'SAUCEPAMPA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'13', N'09', N'SEXI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'13', N'10', N'UTICYACU')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'06', N'13', N'11', N'YAUYUCAN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'07', N'00', N'00', N'CALLAO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'07', N'01', N'00', N'PROV. CONST. DEL CALLAO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'07', N'01', N'01', N'CALLAO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'07', N'01', N'02', N'BELLAVISTA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'07', N'01', N'03', N'CARMEN DE LA LEGUA REYNOSO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'07', N'01', N'04', N'LA PERLA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'07', N'01', N'05', N'LA PUNTA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'07', N'01', N'06', N'VENTANILLA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'07', N'01', N'07', N'MI PERU')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'00', N'00', N'CUSCO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'01', N'00', N'CUSCO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'01', N'01', N'CUSCO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'01', N'02', N'CCORCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'01', N'03', N'POROY')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'01', N'04', N'SAN JERONIMO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'01', N'05', N'SAN SEBASTIAN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'01', N'06', N'SANTIAGO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'01', N'07', N'SAYLLA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'01', N'08', N'WANCHAQ')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'02', N'00', N'ACOMAYO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'02', N'01', N'ACOMAYO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'02', N'02', N'ACOPIA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'02', N'03', N'ACOS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'02', N'04', N'MOSOC LLACTA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'02', N'05', N'POMACANCHI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'02', N'06', N'RONDOCAN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'02', N'07', N'SANGARARA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'03', N'00', N'ANTA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'03', N'01', N'ANTA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'03', N'02', N'ANCAHUASI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'03', N'03', N'CACHIMAYO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'03', N'04', N'CHINCHAYPUJIO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'03', N'05', N'HUAROCONDO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'03', N'06', N'LIMATAMBO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'03', N'07', N'MOLLEPATA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'03', N'08', N'PUCYURA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'03', N'09', N'ZURITE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'04', N'00', N'CALCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'04', N'01', N'CALCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'04', N'02', N'COYA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'04', N'03', N'LAMAY')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'04', N'04', N'LARES')
GO
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'04', N'05', N'PISAC')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'04', N'06', N'SAN SALVADOR')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'04', N'07', N'TARAY')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'04', N'08', N'YANATILE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'05', N'00', N'CANAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'05', N'01', N'YANAOCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'05', N'02', N'CHECCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'05', N'03', N'KUNTURKANKI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'05', N'04', N'LANGUI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'05', N'05', N'LAYO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'05', N'06', N'PAMPAMARCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'05', N'07', N'QUEHUE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'05', N'08', N'TUPAC AMARU')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'06', N'00', N'CANCHIS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'06', N'01', N'SICUANI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'06', N'02', N'CHECACUPE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'06', N'03', N'COMBAPATA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'06', N'04', N'MARANGANI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'06', N'05', N'PITUMARCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'06', N'06', N'SAN PABLO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'06', N'07', N'SAN PEDRO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'06', N'08', N'TINTA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'07', N'00', N'CHUMBIVILCAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'07', N'01', N'SANTO TOMAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'07', N'02', N'CAPACMARCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'07', N'03', N'CHAMACA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'07', N'04', N'COLQUEMARCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'07', N'05', N'LIVITACA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'07', N'06', N'LLUSCO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'07', N'07', N'QUIÑOTA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'07', N'08', N'VELILLE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'08', N'00', N'ESPINAR')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'08', N'01', N'ESPINAR')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'08', N'02', N'CONDOROMA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'08', N'03', N'COPORAQUE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'08', N'04', N'OCORURO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'08', N'05', N'PALLPATA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'08', N'06', N'PICHIGUA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'08', N'07', N'SUYCKUTAMBO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'08', N'08', N'ALTO PICHIGUA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'09', N'00', N'LA CONVENCION')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'09', N'01', N'SANTA ANA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'09', N'02', N'ECHARATE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'09', N'03', N'HUAYOPATA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'09', N'04', N'MARANURA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'09', N'05', N'OCOBAMBA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'09', N'06', N'QUELLOUNO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'09', N'07', N'KIMBIRI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'09', N'08', N'SANTA TERESA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'09', N'09', N'VILCABAMBA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'09', N'10', N'PICHARI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'09', N'11', N'INKAWASI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'09', N'12', N'VILLA VIRGEN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'09', N'13', N'VILLA KINTIARINA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'10', N'00', N'PARURO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'10', N'01', N'PARURO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'10', N'02', N'ACCHA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'10', N'03', N'CCAPI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'10', N'04', N'COLCHA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'10', N'05', N'HUANOQUITE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'10', N'06', N'OMACHA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'10', N'07', N'PACCARITAMBO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'10', N'08', N'PILLPINTO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'10', N'09', N'YAURISQUE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'11', N'00', N'PAUCARTAMBO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'11', N'01', N'PAUCARTAMBO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'11', N'02', N'CAICAY')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'11', N'03', N'CHALLABAMBA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'11', N'04', N'COLQUEPATA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'11', N'05', N'HUANCARANI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'11', N'06', N'KOSÑIPATA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'12', N'00', N'QUISPICANCHI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'12', N'01', N'URCOS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'12', N'02', N'ANDAHUAYLILLAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'12', N'03', N'CAMANTI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'12', N'04', N'CCARHUAYO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'12', N'05', N'CCATCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'12', N'06', N'CUSIPATA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'12', N'07', N'HUARO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'12', N'08', N'LUCRE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'12', N'09', N'MARCAPATA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'12', N'10', N'OCONGATE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'12', N'11', N'OROPESA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'12', N'12', N'QUIQUIJANA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'13', N'00', N'URUBAMBA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'13', N'01', N'URUBAMBA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'13', N'02', N'CHINCHERO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'13', N'03', N'HUAYLLABAMBA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'13', N'04', N'MACHUPICCHU')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'13', N'05', N'MARAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'13', N'06', N'OLLANTAYTAMBO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'08', N'13', N'07', N'YUCAY')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'00', N'00', N'HUANCAVELICA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'01', N'00', N'HUANCAVELICA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'01', N'01', N'HUANCAVELICA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'01', N'02', N'ACOBAMBILLA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'01', N'03', N'ACORIA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'01', N'04', N'CONAYCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'01', N'05', N'CUENCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'01', N'06', N'HUACHOCOLPA')
GO
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'01', N'07', N'HUAYLLAHUARA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'01', N'08', N'IZCUCHACA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'01', N'09', N'LARIA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'01', N'10', N'MANTA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'01', N'11', N'MARISCAL CACERES')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'01', N'12', N'MOYA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'01', N'13', N'NUEVO OCCORO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'01', N'14', N'PALCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'01', N'15', N'PILCHACA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'01', N'16', N'VILCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'01', N'17', N'YAULI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'01', N'18', N'ASCENSION')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'01', N'19', N'HUANDO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'02', N'00', N'ACOBAMBA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'02', N'01', N'ACOBAMBA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'02', N'02', N'ANDABAMBA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'02', N'03', N'ANTA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'02', N'04', N'CAJA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'02', N'05', N'MARCAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'02', N'06', N'PAUCARA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'02', N'07', N'POMACOCHA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'02', N'08', N'ROSARIO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'03', N'00', N'ANGARAES')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'03', N'01', N'LIRCAY')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'03', N'02', N'ANCHONGA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'03', N'03', N'CALLANMARCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'03', N'04', N'CCOCHACCASA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'03', N'05', N'CHINCHO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'03', N'06', N'CONGALLA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'03', N'07', N'HUANCA-HUANCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'03', N'08', N'HUAYLLAY GRANDE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'03', N'09', N'JULCAMARCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'03', N'10', N'SAN ANTONIO DE ANTAPARCO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'03', N'11', N'SANTO TOMAS DE PATA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'03', N'12', N'SECCLLA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'04', N'00', N'CASTROVIRREYNA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'04', N'01', N'CASTROVIRREYNA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'04', N'02', N'ARMA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'04', N'03', N'AURAHUA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'04', N'04', N'CAPILLAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'04', N'05', N'CHUPAMARCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'04', N'06', N'COCAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'04', N'07', N'HUACHOS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'04', N'08', N'HUAMATAMBO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'04', N'09', N'MOLLEPAMPA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'04', N'10', N'SAN JUAN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'04', N'11', N'SANTA ANA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'04', N'12', N'TANTARA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'04', N'13', N'TICRAPO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'05', N'00', N'CHURCAMPA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'05', N'01', N'CHURCAMPA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'05', N'02', N'ANCO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'05', N'03', N'CHINCHIHUASI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'05', N'04', N'EL CARMEN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'05', N'05', N'LA MERCED')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'05', N'06', N'LOCROJA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'05', N'07', N'PAUCARBAMBA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'05', N'08', N'SAN MIGUEL DE MAYOCC')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'05', N'09', N'SAN PEDRO DE CORIS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'05', N'10', N'PACHAMARCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'05', N'11', N'COSME')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'06', N'00', N'HUAYTARA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'06', N'01', N'HUAYTARA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'06', N'02', N'AYAVI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'06', N'03', N'CORDOVA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'06', N'04', N'HUAYACUNDO ARMA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'06', N'05', N'LARAMARCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'06', N'06', N'OCOYO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'06', N'07', N'PILPICHACA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'06', N'08', N'QUERCO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'06', N'09', N'QUITO-ARMA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'06', N'10', N'SAN ANTONIO DE CUSICANCHA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'06', N'11', N'SAN FRANCISCO DE SANGAYAICO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'06', N'12', N'SAN ISIDRO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'06', N'13', N'SANTIAGO DE CHOCORVOS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'06', N'14', N'SANTIAGO DE QUIRAHUARA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'06', N'15', N'SANTO DOMINGO DE CAPILLAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'06', N'16', N'TAMBO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'07', N'00', N'TAYACAJA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'07', N'01', N'PAMPAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'07', N'02', N'ACOSTAMBO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'07', N'03', N'ACRAQUIA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'07', N'04', N'AHUAYCHA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'07', N'05', N'COLCABAMBA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'07', N'06', N'DANIEL HERNANDEZ')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'07', N'07', N'HUACHOCOLPA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'07', N'09', N'HUARIBAMBA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'07', N'10', N'ÑAHUIMPUQUIO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'07', N'11', N'PAZOS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'07', N'13', N'QUISHUAR')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'07', N'14', N'SALCABAMBA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'07', N'15', N'SALCAHUASI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'07', N'16', N'SAN MARCOS DE ROCCHAC')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'07', N'17', N'SURCUBAMBA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'07', N'18', N'TINTAY PUNCU')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'07', N'19', N'QUICHUAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'07', N'20', N'ANDAYMARCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'07', N'21', N'ROBLE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'09', N'07', N'22', N'PICHOS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'00', N'00', N'HUANUCO')
GO
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'01', N'00', N'HUANUCO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'01', N'01', N'HUANUCO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'01', N'02', N'AMARILIS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'01', N'03', N'CHINCHAO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'01', N'04', N'CHURUBAMBA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'01', N'05', N'MARGOS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'01', N'06', N'QUISQUI (KICHKI)')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'01', N'07', N'SAN FRANCISCO DE CAYRAN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'01', N'08', N'SAN PEDRO DE CHAULAN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'01', N'09', N'SANTA MARIA DEL VALLE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'01', N'10', N'YARUMAYO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'01', N'11', N'PILLCO MARCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'01', N'12', N'YACUS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'01', N'13', N'SAN PABLO DE PILLAO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'02', N'00', N'AMBO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'02', N'01', N'AMBO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'02', N'02', N'CAYNA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'02', N'03', N'COLPAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'02', N'04', N'CONCHAMARCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'02', N'05', N'HUACAR')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'02', N'06', N'SAN FRANCISCO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'02', N'07', N'SAN RAFAEL')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'02', N'08', N'TOMAY KICHWA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'03', N'00', N'DOS DE MAYO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'03', N'01', N'LA UNION')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'03', N'07', N'CHUQUIS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'03', N'11', N'MARIAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'03', N'13', N'PACHAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'03', N'16', N'QUIVILLA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'03', N'17', N'RIPAN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'03', N'21', N'SHUNQUI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'03', N'22', N'SILLAPATA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'03', N'23', N'YANAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'04', N'00', N'HUACAYBAMBA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'04', N'01', N'HUACAYBAMBA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'04', N'02', N'CANCHABAMBA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'04', N'03', N'COCHABAMBA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'04', N'04', N'PINRA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'05', N'00', N'HUAMALIES')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'05', N'01', N'LLATA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'05', N'02', N'ARANCAY')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'05', N'03', N'CHAVIN DE PARIARCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'05', N'04', N'JACAS GRANDE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'05', N'05', N'JIRCAN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'05', N'06', N'MIRAFLORES')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'05', N'07', N'MONZON')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'05', N'08', N'PUNCHAO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'05', N'09', N'PUÑOS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'05', N'10', N'SINGA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'05', N'11', N'TANTAMAYO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'06', N'00', N'LEONCIO PRADO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'06', N'01', N'RUPA-RUPA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'06', N'02', N'DANIEL ALOMIA ROBLES')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'06', N'03', N'HERMILIO VALDIZAN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'06', N'04', N'JOSE CRESPO Y CASTILLO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'06', N'05', N'LUYANDO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'06', N'06', N'MARIANO DAMASO BERAUN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'06', N'07', N'PUCAYACU')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'06', N'08', N'CASTILLO GRANDE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'07', N'00', N'MARAÑON')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'07', N'01', N'HUACRACHUCO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'07', N'02', N'CHOLON')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'07', N'03', N'SAN BUENAVENTURA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'07', N'04', N'LA MORADA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'07', N'05', N'SANTA ROSA DE ALTO YANAJANCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'08', N'00', N'PACHITEA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'08', N'01', N'PANAO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'08', N'02', N'CHAGLLA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'08', N'03', N'MOLINO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'08', N'04', N'UMARI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'09', N'00', N'PUERTO INCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'09', N'01', N'PUERTO INCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'09', N'02', N'CODO DEL POZUZO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'09', N'03', N'HONORIA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'09', N'04', N'TOURNAVISTA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'09', N'05', N'YUYAPICHIS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'10', N'00', N'LAURICOCHA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'10', N'01', N'JESUS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'10', N'02', N'BAÑOS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'10', N'03', N'JIVIA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'10', N'04', N'QUEROPALCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'10', N'05', N'RONDOS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'10', N'06', N'SAN FRANCISCO DE ASIS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'10', N'07', N'SAN MIGUEL DE CAURI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'11', N'00', N'YAROWILCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'11', N'01', N'CHAVINILLO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'11', N'02', N'CAHUAC')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'11', N'03', N'CHACABAMBA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'11', N'04', N'APARICIO POMARES')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'11', N'05', N'JACAS CHICO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'11', N'06', N'OBAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'11', N'07', N'PAMPAMARCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'10', N'11', N'08', N'CHORAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'11', N'00', N'00', N'ICA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'11', N'01', N'00', N'ICA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'11', N'01', N'01', N'ICA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'11', N'01', N'02', N'LA TINGUIÑA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'11', N'01', N'03', N'LOS AQUIJES')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'11', N'01', N'04', N'OCUCAJE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'11', N'01', N'05', N'PACHACUTEC')
GO
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'11', N'01', N'06', N'PARCONA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'11', N'01', N'07', N'PUEBLO NUEVO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'11', N'01', N'08', N'SALAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'11', N'01', N'09', N'SAN JOSE DE LOS MOLINOS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'11', N'01', N'10', N'SAN JUAN BAUTISTA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'11', N'01', N'11', N'SANTIAGO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'11', N'01', N'12', N'SUBTANJALLA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'11', N'01', N'13', N'TATE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'11', N'01', N'14', N'YAUCA DEL ROSARIO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'11', N'02', N'00', N'CHINCHA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'11', N'02', N'01', N'CHINCHA ALTA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'11', N'02', N'02', N'ALTO LARAN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'11', N'02', N'03', N'CHAVIN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'11', N'02', N'04', N'CHINCHA BAJA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'11', N'02', N'05', N'EL CARMEN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'11', N'02', N'06', N'GROCIO PRADO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'11', N'02', N'07', N'PUEBLO NUEVO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'11', N'02', N'08', N'SAN JUAN DE YANAC')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'11', N'02', N'09', N'SAN PEDRO DE HUACARPANA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'11', N'02', N'10', N'SUNAMPE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'11', N'02', N'11', N'TAMBO DE MORA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'11', N'03', N'00', N'NAZCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'11', N'03', N'01', N'NASCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'11', N'03', N'02', N'CHANGUILLO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'11', N'03', N'03', N'EL INGENIO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'11', N'03', N'04', N'MARCONA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'11', N'03', N'05', N'VISTA ALEGRE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'11', N'04', N'00', N'PALPA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'11', N'04', N'01', N'PALPA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'11', N'04', N'02', N'LLIPATA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'11', N'04', N'03', N'RIO GRANDE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'11', N'04', N'04', N'SANTA CRUZ')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'11', N'04', N'05', N'TIBILLO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'11', N'05', N'00', N'PISCO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'11', N'05', N'01', N'PISCO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'11', N'05', N'02', N'HUANCANO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'11', N'05', N'03', N'HUMAY')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'11', N'05', N'04', N'INDEPENDENCIA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'11', N'05', N'05', N'PARACAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'11', N'05', N'06', N'SAN ANDRES')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'11', N'05', N'07', N'SAN CLEMENTE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'11', N'05', N'08', N'TUPAC AMARU INCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'00', N'00', N'JUNIN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'01', N'00', N'HUANCAYO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'01', N'01', N'HUANCAYO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'01', N'04', N'CARHUACALLANGA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'01', N'05', N'CHACAPAMPA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'01', N'06', N'CHICCHE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'01', N'07', N'CHILCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'01', N'08', N'CHONGOS ALTO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'01', N'11', N'CHUPURO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'01', N'12', N'COLCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'01', N'13', N'CULLHUAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'01', N'14', N'EL TAMBO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'01', N'16', N'HUACRAPUQUIO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'01', N'17', N'HUALHUAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'01', N'19', N'HUANCAN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'01', N'20', N'HUASICANCHA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'01', N'21', N'HUAYUCACHI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'01', N'22', N'INGENIO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'01', N'24', N'PARIAHUANCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'01', N'25', N'PILCOMAYO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'01', N'26', N'PUCARA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'01', N'27', N'QUICHUAY')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'01', N'28', N'QUILCAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'01', N'29', N'SAN AGUSTIN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'01', N'30', N'SAN JERONIMO DE TUNAN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'01', N'32', N'SAÑO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'01', N'33', N'SAPALLANGA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'01', N'34', N'SICAYA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'01', N'35', N'SANTO DOMINGO DE ACOBAMBA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'01', N'36', N'VIQUES')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'02', N'00', N'CONCEPCION')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'02', N'01', N'CONCEPCION')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'02', N'02', N'ACO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'02', N'03', N'ANDAMARCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'02', N'04', N'CHAMBARA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'02', N'05', N'COCHAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'02', N'06', N'COMAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'02', N'07', N'HEROINAS TOLEDO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'02', N'08', N'MANZANARES')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'02', N'09', N'MARISCAL CASTILLA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'02', N'10', N'MATAHUASI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'02', N'11', N'MITO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'02', N'12', N'NUEVE DE JULIO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'02', N'13', N'ORCOTUNA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'02', N'14', N'SAN JOSE DE QUERO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'02', N'15', N'SANTA ROSA DE OCOPA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'03', N'00', N'CHANCHAMAYO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'03', N'01', N'CHANCHAMAYO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'03', N'02', N'PERENE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'03', N'03', N'PICHANAQUI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'03', N'04', N'SAN LUIS DE SHUARO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'03', N'05', N'SAN RAMON')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'03', N'06', N'VITOC')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'04', N'00', N'JAUJA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'04', N'01', N'JAUJA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'04', N'02', N'ACOLLA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'04', N'03', N'APATA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'04', N'04', N'ATAURA')
GO
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'04', N'05', N'CANCHAYLLO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'04', N'06', N'CURICACA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'04', N'07', N'EL MANTARO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'04', N'08', N'HUAMALI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'04', N'09', N'HUARIPAMPA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'04', N'10', N'HUERTAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'04', N'11', N'JANJAILLO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'04', N'12', N'JULCAN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'04', N'13', N'LEONOR ORDOÑEZ')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'04', N'14', N'LLOCLLAPAMPA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'04', N'15', N'MARCO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'04', N'16', N'MASMA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'04', N'17', N'MASMA CHICCHE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'04', N'18', N'MOLINOS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'04', N'19', N'MONOBAMBA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'04', N'20', N'MUQUI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'04', N'21', N'MUQUIYAUYO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'04', N'22', N'PACA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'04', N'23', N'PACCHA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'04', N'24', N'PANCAN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'04', N'25', N'PARCO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'04', N'26', N'POMACANCHA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'04', N'27', N'RICRAN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'04', N'28', N'SAN LORENZO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'04', N'29', N'SAN PEDRO DE CHUNAN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'04', N'30', N'SAUSA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'04', N'31', N'SINCOS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'04', N'32', N'TUNAN MARCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'04', N'33', N'YAULI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'04', N'34', N'YAUYOS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'05', N'00', N'JUNIN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'05', N'01', N'JUNIN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'05', N'02', N'CARHUAMAYO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'05', N'03', N'ONDORES')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'05', N'04', N'ULCUMAYO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'06', N'00', N'SATIPO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'06', N'01', N'SATIPO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'06', N'02', N'COVIRIALI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'06', N'03', N'LLAYLLA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'06', N'04', N'MAZAMARI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'06', N'05', N'PAMPA HERMOSA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'06', N'06', N'PANGOA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'06', N'07', N'RIO NEGRO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'06', N'08', N'RIO TAMBO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'06', N'09', N'VIZCATAN DEL ENE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'07', N'00', N'TARMA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'07', N'01', N'TARMA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'07', N'02', N'ACOBAMBA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'07', N'03', N'HUARICOLCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'07', N'04', N'HUASAHUASI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'07', N'05', N'LA UNION')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'07', N'06', N'PALCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'07', N'07', N'PALCAMAYO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'07', N'08', N'SAN PEDRO DE CAJAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'07', N'09', N'TAPO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'08', N'00', N'YAULI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'08', N'01', N'LA OROYA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'08', N'02', N'CHACAPALPA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'08', N'03', N'HUAY-HUAY')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'08', N'04', N'MARCAPOMACOCHA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'08', N'05', N'MOROCOCHA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'08', N'06', N'PACCHA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'08', N'07', N'SANTA BARBARA DE CARHUACAYAN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'08', N'08', N'SANTA ROSA DE SACCO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'08', N'09', N'SUITUCANCHA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'08', N'10', N'YAULI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'09', N'00', N'CHUPACA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'09', N'01', N'CHUPACA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'09', N'02', N'AHUAC')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'09', N'03', N'CHONGOS BAJO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'09', N'04', N'HUACHAC')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'09', N'05', N'HUAMANCACA CHICO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'09', N'06', N'SAN JUAN DE ISCOS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'09', N'07', N'SAN JUAN DE JARPA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'09', N'08', N'TRES DE DICIEMBRE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'12', N'09', N'09', N'YANACANCHA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'00', N'00', N'LA LIBERTAD')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'01', N'00', N'TRUJILLO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'01', N'01', N'TRUJILLO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'01', N'02', N'EL PORVENIR')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'01', N'03', N'FLORENCIA DE MORA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'01', N'04', N'HUANCHACO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'01', N'05', N'LA ESPERANZA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'01', N'06', N'LAREDO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'01', N'07', N'MOCHE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'01', N'08', N'POROTO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'01', N'09', N'SALAVERRY')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'01', N'10', N'SIMBAL')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'01', N'11', N'VICTOR LARCO HERRERA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'02', N'00', N'ASCOPE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'02', N'01', N'ASCOPE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'02', N'02', N'CHICAMA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'02', N'03', N'CHOCOPE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'02', N'04', N'MAGDALENA DE CAO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'02', N'05', N'PAIJAN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'02', N'06', N'RAZURI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'02', N'07', N'SANTIAGO DE CAO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'02', N'08', N'CASA GRANDE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'03', N'00', N'BOLIVAR')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'03', N'01', N'BOLIVAR')
GO
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'03', N'02', N'BAMBAMARCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'03', N'03', N'CONDORMARCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'03', N'04', N'LONGOTEA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'03', N'05', N'UCHUMARCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'03', N'06', N'UCUNCHA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'04', N'00', N'CHEPEN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'04', N'01', N'CHEPEN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'04', N'02', N'PACANGA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'04', N'03', N'PUEBLO NUEVO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'05', N'00', N'JULCAN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'05', N'01', N'JULCAN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'05', N'02', N'CALAMARCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'05', N'03', N'CARABAMBA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'05', N'04', N'HUASO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'06', N'00', N'OTUZCO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'06', N'01', N'OTUZCO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'06', N'02', N'AGALLPAMPA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'06', N'04', N'CHARAT')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'06', N'05', N'HUARANCHAL')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'06', N'06', N'LA CUESTA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'06', N'08', N'MACHE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'06', N'10', N'PARANDAY')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'06', N'11', N'SALPO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'06', N'13', N'SINSICAP')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'06', N'14', N'USQUIL')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'07', N'00', N'PACASMAYO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'07', N'01', N'SAN PEDRO DE LLOC')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'07', N'02', N'GUADALUPE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'07', N'03', N'JEQUETEPEQUE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'07', N'04', N'PACASMAYO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'07', N'05', N'SAN JOSE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'08', N'00', N'PATAZ')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'08', N'01', N'TAYABAMBA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'08', N'02', N'BULDIBUYO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'08', N'03', N'CHILLIA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'08', N'04', N'HUANCASPATA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'08', N'05', N'HUAYLILLAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'08', N'06', N'HUAYO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'08', N'07', N'ONGON')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'08', N'08', N'PARCOY')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'08', N'09', N'PATAZ')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'08', N'10', N'PIAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'08', N'11', N'SANTIAGO DE CHALLAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'08', N'12', N'TAURIJA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'08', N'13', N'URPAY')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'09', N'00', N'SANCHEZ CARRION')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'09', N'01', N'HUAMACHUCO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'09', N'02', N'CHUGAY')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'09', N'03', N'COCHORCO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'09', N'04', N'CURGOS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'09', N'05', N'MARCABAL')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'09', N'06', N'SANAGORAN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'09', N'07', N'SARIN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'09', N'08', N'SARTIMBAMBA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'10', N'00', N'SANTIAGO DE CHUCO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'10', N'01', N'SANTIAGO DE CHUCO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'10', N'02', N'ANGASMARCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'10', N'03', N'CACHICADAN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'10', N'04', N'MOLLEBAMBA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'10', N'05', N'MOLLEPATA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'10', N'06', N'QUIRUVILCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'10', N'07', N'SANTA CRUZ DE CHUCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'10', N'08', N'SITABAMBA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'11', N'00', N'GRAN CHIMU')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'11', N'01', N'CASCAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'11', N'02', N'LUCMA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'11', N'03', N'MARMOT')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'11', N'04', N'SAYAPULLO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'12', N'00', N'VIRU')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'12', N'01', N'VIRU')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'12', N'02', N'CHAO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'13', N'12', N'03', N'GUADALUPITO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'14', N'00', N'00', N'LAMBAYEQUE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'14', N'01', N'00', N'CHICLAYO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'14', N'01', N'01', N'CHICLAYO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'14', N'01', N'02', N'CHONGOYAPE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'14', N'01', N'03', N'ETEN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'14', N'01', N'04', N'ETEN PUERTO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'14', N'01', N'05', N'JOSE LEONARDO ORTIZ')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'14', N'01', N'06', N'LA VICTORIA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'14', N'01', N'07', N'LAGUNAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'14', N'01', N'08', N'MONSEFU')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'14', N'01', N'09', N'NUEVA ARICA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'14', N'01', N'10', N'OYOTUN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'14', N'01', N'11', N'PICSI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'14', N'01', N'12', N'PIMENTEL')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'14', N'01', N'13', N'REQUE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'14', N'01', N'14', N'SANTA ROSA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'14', N'01', N'15', N'SAÑA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'14', N'01', N'16', N'CAYALTI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'14', N'01', N'17', N'PATAPO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'14', N'01', N'18', N'POMALCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'14', N'01', N'19', N'PUCALA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'14', N'01', N'20', N'TUMAN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'14', N'02', N'00', N'FERREÑAFE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'14', N'02', N'01', N'FERREÑAFE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'14', N'02', N'02', N'CAÑARIS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'14', N'02', N'03', N'INCAHUASI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'14', N'02', N'04', N'MANUEL ANTONIO MESONES MURO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'14', N'02', N'05', N'PITIPO')
GO
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'14', N'02', N'06', N'PUEBLO NUEVO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'14', N'03', N'00', N'LAMBAYEQUE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'14', N'03', N'01', N'LAMBAYEQUE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'14', N'03', N'02', N'CHOCHOPE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'14', N'03', N'03', N'ILLIMO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'14', N'03', N'04', N'JAYANCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'14', N'03', N'05', N'MOCHUMI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'14', N'03', N'06', N'MORROPE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'14', N'03', N'07', N'MOTUPE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'14', N'03', N'08', N'OLMOS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'14', N'03', N'09', N'PACORA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'14', N'03', N'10', N'SALAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'14', N'03', N'11', N'SAN JOSE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'14', N'03', N'12', N'TUCUME')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'00', N'00', N'LIMA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'01', N'00', N'LIMA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'01', N'01', N'LIMA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'01', N'02', N'ANCON')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'01', N'03', N'ATE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'01', N'0301', N'ATE!')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'01', N'0302', N'SANTA CLARA!')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'01', N'0303', N'HUAYCAN!')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'01', N'0304', N'HUACHIPA!')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'01', N'0305', N'VITARTE!')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'01', N'04', N'BARRANCO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'01', N'05', N'BREÑA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'01', N'06', N'CARABAYLLO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'01', N'07', N'CHACLACAYO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'01', N'08', N'CHORRILLOS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'01', N'09', N'CIENEGUILLA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'01', N'10', N'COMAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'01', N'11', N'EL AGUSTINO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'01', N'12', N'INDEPENDENCIA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'01', N'13', N'JESUS MARIA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'01', N'14', N'LA MOLINA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'01', N'15', N'LA VICTORIA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'01', N'16', N'LINCE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'01', N'17', N'LOS OLIVOS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'01', N'18', N'LURIGANCHO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'01', N'19', N'LURIN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'01', N'20', N'MAGDALENA DEL MAR')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'01', N'21', N'PUEBLO LIBRE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'01', N'22', N'MIRAFLORES')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'01', N'23', N'PACHACAMAC')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'01', N'24', N'PUCUSANA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'01', N'25', N'PUENTE PIEDRA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'01', N'26', N'PUNTA HERMOSA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'01', N'27', N'PUNTA NEGRA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'01', N'28', N'RIMAC')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'01', N'29', N'SAN BARTOLO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'01', N'30', N'SAN BORJA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'01', N'31', N'SAN ISIDRO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'01', N'32', N'SAN JUAN DE LURIGANCHO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'01', N'33', N'SAN JUAN DE MIRAFLORES')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'01', N'34', N'SAN LUIS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'01', N'35', N'SAN MARTIN DE PORRES')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'01', N'36', N'SAN MIGUEL')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'01', N'37', N'SANTA ANITA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'01', N'38', N'SANTA MARIA DEL MAR')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'01', N'39', N'SANTA ROSA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'01', N'40', N'SANTIAGO DE SURCO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'01', N'41', N'SURQUILLO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'01', N'42', N'VILLA EL SALVADOR')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'01', N'43', N'VILLA MARIA DEL TRIUNFO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'02', N'00', N'BARRANCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'02', N'01', N'BARRANCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'02', N'02', N'PARAMONGA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'02', N'03', N'PATIVILCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'02', N'04', N'SUPE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'02', N'05', N'SUPE PUERTO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'03', N'00', N'CAJATAMBO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'03', N'01', N'CAJATAMBO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'03', N'02', N'COPA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'03', N'03', N'GORGOR')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'03', N'04', N'HUANCAPON')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'03', N'05', N'MANAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'04', N'00', N'CANTA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'04', N'01', N'CANTA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'04', N'02', N'ARAHUAY')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'04', N'03', N'HUAMANTANGA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'04', N'04', N'HUAROS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'04', N'05', N'LACHAQUI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'04', N'06', N'SAN BUENAVENTURA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'04', N'07', N'SANTA ROSA DE QUIVES')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'05', N'00', N'CAÑETE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'05', N'01', N'SAN VICENTE DE CAÑETE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'05', N'02', N'ASIA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'05', N'03', N'CALANGO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'05', N'04', N'CERRO AZUL')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'05', N'05', N'CHILCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'05', N'06', N'COAYLLO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'05', N'07', N'IMPERIAL')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'05', N'08', N'LUNAHUANA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'05', N'09', N'MALA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'05', N'10', N'NUEVO IMPERIAL')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'05', N'11', N'PACARAN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'05', N'12', N'QUILMANA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'05', N'13', N'SAN ANTONIO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'05', N'14', N'SAN LUIS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'05', N'15', N'SANTA CRUZ DE FLORES')
GO
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'05', N'16', N'ZUÑIGA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'06', N'00', N'HUARAL')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'06', N'01', N'HUARAL')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'06', N'02', N'ATAVILLOS ALTO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'06', N'03', N'ATAVILLOS BAJO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'06', N'04', N'AUCALLAMA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'06', N'05', N'CHANCAY')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'06', N'06', N'IHUARI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'06', N'07', N'LAMPIAN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'06', N'08', N'PACARAOS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'06', N'09', N'SAN MIGUEL DE ACOS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'06', N'10', N'SANTA CRUZ DE ANDAMARCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'06', N'11', N'SUMBILCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'06', N'12', N'VEINTISIETE DE NOVIEMBRE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'07', N'00', N'HUAROCHIRI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'07', N'01', N'MATUCANA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'07', N'02', N'ANTIOQUIA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'07', N'03', N'CALLAHUANCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'07', N'04', N'CARAMPOMA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'07', N'05', N'CHICLA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'07', N'06', N'CUENCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'07', N'07', N'HUACHUPAMPA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'07', N'08', N'HUANZA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'07', N'09', N'HUAROCHIRI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'07', N'10', N'LAHUAYTAMBO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'07', N'11', N'LANGA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'07', N'12', N'LARAOS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'07', N'13', N'MARIATANA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'07', N'14', N'RICARDO PALMA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'07', N'15', N'SAN ANDRES DE TUPICOCHA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'07', N'16', N'SAN ANTONIO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'07', N'17', N'SAN BARTOLOME')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'07', N'18', N'SAN DAMIAN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'07', N'19', N'SAN JUAN DE IRIS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'07', N'20', N'SAN JUAN DE TANTARANCHE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'07', N'21', N'SAN LORENZO DE QUINTI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'07', N'22', N'SAN MATEO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'07', N'23', N'SAN MATEO DE OTAO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'07', N'24', N'SAN PEDRO DE CASTA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'07', N'25', N'SAN PEDRO DE HUANCAYRE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'07', N'26', N'SANGALLAYA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'07', N'27', N'SANTA CRUZ DE COCACHACRA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'07', N'28', N'SANTA EULALIA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'07', N'29', N'SANTIAGO DE ANCHUCAYA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'07', N'30', N'SANTIAGO DE TUNA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'07', N'31', N'SANTO DOMINGO DE LOS OLLEROS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'07', N'32', N'SURCO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'08', N'00', N'HUAURA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'08', N'01', N'HUACHO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'08', N'02', N'AMBAR')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'08', N'03', N'CALETA DE CARQUIN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'08', N'04', N'CHECRAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'08', N'05', N'HUALMAY')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'08', N'06', N'HUAURA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'08', N'07', N'LEONCIO PRADO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'08', N'08', N'PACCHO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'08', N'09', N'SANTA LEONOR')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'08', N'10', N'SANTA MARIA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'08', N'11', N'SAYAN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'08', N'12', N'VEGUETA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'09', N'00', N'OYON')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'09', N'01', N'OYON')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'09', N'02', N'ANDAJES')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'09', N'03', N'CAUJUL')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'09', N'04', N'COCHAMARCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'09', N'05', N'NAVAN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'09', N'06', N'PACHANGARA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'10', N'00', N'YAUYOS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'10', N'01', N'YAUYOS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'10', N'02', N'ALIS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'10', N'03', N'ALLAUCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'10', N'04', N'AYAVIRI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'10', N'05', N'AZANGARO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'10', N'06', N'CACRA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'10', N'07', N'CARANIA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'10', N'08', N'CATAHUASI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'10', N'09', N'CHOCOS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'10', N'10', N'COCHAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'10', N'11', N'COLONIA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'10', N'12', N'HONGOS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'10', N'13', N'HUAMPARA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'10', N'14', N'HUANCAYA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'10', N'15', N'HUANGASCAR')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'10', N'16', N'HUANTAN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'10', N'17', N'HUAÑEC')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'10', N'18', N'LARAOS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'10', N'19', N'LINCHA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'10', N'20', N'MADEAN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'10', N'21', N'MIRAFLORES')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'10', N'22', N'OMAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'10', N'23', N'PUTINZA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'10', N'24', N'QUINCHES')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'10', N'25', N'QUINOCAY')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'10', N'26', N'SAN JOAQUIN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'10', N'27', N'SAN PEDRO DE PILAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'10', N'28', N'TANTA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'10', N'29', N'TAURIPAMPA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'10', N'30', N'TOMAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'10', N'31', N'TUPE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'10', N'32', N'VIÑAC')
GO
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'15', N'10', N'33', N'VITIS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'16', N'00', N'00', N'LORETO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'16', N'01', N'00', N'MAYNAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'16', N'01', N'01', N'IQUITOS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'16', N'01', N'02', N'ALTO NANAY')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'16', N'01', N'03', N'FERNANDO LORES')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'16', N'01', N'04', N'INDIANA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'16', N'01', N'05', N'LAS AMAZONAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'16', N'01', N'06', N'MAZAN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'16', N'01', N'07', N'NAPO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'16', N'01', N'08', N'PUNCHANA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'16', N'01', N'10', N'TORRES CAUSANA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'16', N'01', N'12', N'BELEN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'16', N'01', N'13', N'SAN JUAN BAUTISTA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'16', N'02', N'00', N'ALTO AMAZONAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'16', N'02', N'01', N'YURIMAGUAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'16', N'02', N'02', N'BALSAPUERTO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'16', N'02', N'05', N'JEBEROS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'16', N'02', N'06', N'LAGUNAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'16', N'02', N'10', N'SANTA CRUZ')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'16', N'02', N'11', N'TENIENTE CESAR LOPEZ ROJAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'16', N'03', N'00', N'LORETO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'16', N'03', N'01', N'NAUTA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'16', N'03', N'02', N'PARINARI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'16', N'03', N'03', N'TIGRE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'16', N'03', N'04', N'TROMPETEROS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'16', N'03', N'05', N'URARINAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'16', N'04', N'00', N'MARISCAL RAMON CASTILLA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'16', N'04', N'01', N'RAMON CASTILLA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'16', N'04', N'02', N'PEBAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'16', N'04', N'03', N'YAVARI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'16', N'04', N'04', N'SAN PABLO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'16', N'05', N'00', N'REQUENA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'16', N'05', N'01', N'REQUENA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'16', N'05', N'02', N'ALTO TAPICHE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'16', N'05', N'03', N'CAPELO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'16', N'05', N'04', N'EMILIO SAN MARTIN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'16', N'05', N'05', N'MAQUIA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'16', N'05', N'06', N'PUINAHUA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'16', N'05', N'07', N'SAQUENA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'16', N'05', N'08', N'SOPLIN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'16', N'05', N'09', N'TAPICHE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'16', N'05', N'10', N'JENARO HERRERA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'16', N'05', N'11', N'YAQUERANA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'16', N'06', N'00', N'UCAYALI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'16', N'06', N'01', N'CONTAMANA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'16', N'06', N'02', N'INAHUAYA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'16', N'06', N'03', N'PADRE MARQUEZ')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'16', N'06', N'04', N'PAMPA HERMOSA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'16', N'06', N'05', N'SARAYACU')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'16', N'06', N'06', N'VARGAS GUERRA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'16', N'07', N'00', N'DATEM DEL MARAÑON')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'16', N'07', N'01', N'BARRANCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'16', N'07', N'02', N'CAHUAPANAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'16', N'07', N'03', N'MANSERICHE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'16', N'07', N'04', N'MORONA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'16', N'07', N'05', N'PASTAZA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'16', N'07', N'06', N'ANDOAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'16', N'08', N'00', N'PUTUMAYO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'16', N'08', N'01', N'PUTUMAYO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'16', N'08', N'02', N'ROSA PANDURO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'16', N'08', N'03', N'TENIENTE MANUEL CLAVERO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'16', N'08', N'04', N'YAGUAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'17', N'00', N'00', N'MADRE DE DIOS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'17', N'01', N'00', N'TAMBOPATA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'17', N'01', N'01', N'TAMBOPATA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'17', N'01', N'02', N'INAMBARI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'17', N'01', N'03', N'LAS PIEDRAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'17', N'01', N'04', N'LABERINTO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'17', N'02', N'00', N'MANU')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'17', N'02', N'01', N'MANU')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'17', N'02', N'02', N'FITZCARRALD')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'17', N'02', N'03', N'MADRE DE DIOS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'17', N'02', N'04', N'HUEPETUHE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'17', N'03', N'00', N'TAHUAMANU')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'17', N'03', N'01', N'IÑAPARI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'17', N'03', N'02', N'IBERIA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'17', N'03', N'03', N'TAHUAMANU')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'18', N'00', N'00', N'MOQUEGUA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'18', N'01', N'00', N'MARISCAL NIETO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'18', N'01', N'01', N'MOQUEGUA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'18', N'01', N'02', N'CARUMAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'18', N'01', N'03', N'CUCHUMBAYA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'18', N'01', N'04', N'SAMEGUA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'18', N'01', N'05', N'SAN CRISTOBAL')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'18', N'01', N'06', N'TORATA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'18', N'02', N'00', N'GENERAL SANCHEZ CERRO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'18', N'02', N'01', N'OMATE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'18', N'02', N'02', N'CHOJATA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'18', N'02', N'03', N'COALAQUE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'18', N'02', N'04', N'ICHUÑA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'18', N'02', N'05', N'LA CAPILLA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'18', N'02', N'06', N'LLOQUE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'18', N'02', N'07', N'MATALAQUE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'18', N'02', N'08', N'PUQUINA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'18', N'02', N'09', N'QUINISTAQUILLAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'18', N'02', N'10', N'UBINAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'18', N'02', N'11', N'YUNGA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'18', N'03', N'00', N'ILO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'18', N'03', N'01', N'ILO')
GO
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'18', N'03', N'02', N'EL ALGARROBAL')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'18', N'03', N'03', N'PACOCHA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'19', N'00', N'00', N'PASCO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'19', N'01', N'00', N'PASCO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'19', N'01', N'01', N'CHAUPIMARCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'19', N'01', N'02', N'HUACHON')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'19', N'01', N'03', N'HUARIACA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'19', N'01', N'04', N'HUAYLLAY')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'19', N'01', N'05', N'NINACACA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'19', N'01', N'06', N'PALLANCHACRA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'19', N'01', N'07', N'PAUCARTAMBO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'19', N'01', N'08', N'SAN FRANCISCO DE ASIS DE YARUSYACAN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'19', N'01', N'09', N'SIMON BOLIVAR')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'19', N'01', N'10', N'TICLACAYAN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'19', N'01', N'11', N'TINYAHUARCO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'19', N'01', N'12', N'VICCO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'19', N'01', N'13', N'YANACANCHA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'19', N'02', N'00', N'DANIEL ALCIDES CARRION')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'19', N'02', N'01', N'YANAHUANCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'19', N'02', N'02', N'CHACAYAN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'19', N'02', N'03', N'GOYLLARISQUIZGA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'19', N'02', N'04', N'PAUCAR')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'19', N'02', N'05', N'SAN PEDRO DE PILLAO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'19', N'02', N'06', N'SANTA ANA DE TUSI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'19', N'02', N'07', N'TAPUC')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'19', N'02', N'08', N'VILCABAMBA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'19', N'03', N'00', N'OXAPAMPA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'19', N'03', N'01', N'OXAPAMPA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'19', N'03', N'02', N'CHONTABAMBA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'19', N'03', N'03', N'HUANCABAMBA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'19', N'03', N'04', N'PALCAZU')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'19', N'03', N'05', N'POZUZO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'19', N'03', N'06', N'PUERTO BERMUDEZ')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'19', N'03', N'07', N'VILLA RICA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'19', N'03', N'08', N'CONSTITUCION')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'20', N'00', N'00', N'PIURA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'20', N'01', N'00', N'PIURA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'20', N'01', N'01', N'PIURA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'20', N'01', N'04', N'CASTILLA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'20', N'01', N'05', N'CATACAOS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'20', N'01', N'07', N'CURA MORI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'20', N'01', N'08', N'EL TALLAN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'20', N'01', N'09', N'LA ARENA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'20', N'01', N'10', N'LA UNION')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'20', N'01', N'11', N'LAS LOMAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'20', N'01', N'14', N'TAMBO GRANDE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'20', N'01', N'15', N'VEINTISEIS DE OCTUBRE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'20', N'02', N'00', N'AYABACA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'20', N'02', N'01', N'AYABACA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'20', N'02', N'02', N'FRIAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'20', N'02', N'03', N'JILILI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'20', N'02', N'04', N'LAGUNAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'20', N'02', N'05', N'MONTERO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'20', N'02', N'06', N'PACAIPAMPA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'20', N'02', N'07', N'PAIMAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'20', N'02', N'08', N'SAPILLICA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'20', N'02', N'09', N'SICCHEZ')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'20', N'02', N'10', N'SUYO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'20', N'03', N'00', N'HUANCABAMBA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'20', N'03', N'01', N'HUANCABAMBA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'20', N'03', N'02', N'CANCHAQUE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'20', N'03', N'03', N'EL CARMEN DE LA FRONTERA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'20', N'03', N'04', N'HUARMACA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'20', N'03', N'05', N'LALAQUIZ')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'20', N'03', N'06', N'SAN MIGUEL DE EL FAIQUE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'20', N'03', N'07', N'SONDOR')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'20', N'03', N'08', N'SONDORILLO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'20', N'04', N'00', N'MORROPON')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'20', N'04', N'01', N'CHULUCANAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'20', N'04', N'02', N'BUENOS AIRES')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'20', N'04', N'03', N'CHALACO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'20', N'04', N'04', N'LA MATANZA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'20', N'04', N'05', N'MORROPON')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'20', N'04', N'06', N'SALITRAL')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'20', N'04', N'07', N'SAN JUAN DE BIGOTE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'20', N'04', N'08', N'SANTA CATALINA DE MOSSA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'20', N'04', N'09', N'SANTO DOMINGO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'20', N'04', N'10', N'YAMANGO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'20', N'05', N'00', N'PAITA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'20', N'05', N'01', N'PAITA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'20', N'05', N'02', N'AMOTAPE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'20', N'05', N'03', N'ARENAL')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'20', N'05', N'04', N'COLAN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'20', N'05', N'05', N'LA HUACA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'20', N'05', N'06', N'TAMARINDO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'20', N'05', N'07', N'VICHAYAL')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'20', N'06', N'00', N'SULLANA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'20', N'06', N'01', N'SULLANA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'20', N'06', N'02', N'BELLAVISTA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'20', N'06', N'03', N'IGNACIO ESCUDERO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'20', N'06', N'04', N'LANCONES')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'20', N'06', N'05', N'MARCAVELICA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'20', N'06', N'06', N'MIGUEL CHECA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'20', N'06', N'07', N'QUERECOTILLO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'20', N'06', N'08', N'SALITRAL')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'20', N'07', N'00', N'TALARA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'20', N'07', N'01', N'PARIÑAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'20', N'07', N'02', N'EL ALTO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'20', N'07', N'03', N'LA BREA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'20', N'07', N'04', N'LOBITOS')
GO
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'20', N'07', N'05', N'LOS ORGANOS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'20', N'07', N'06', N'MANCORA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'20', N'08', N'00', N'SECHURA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'20', N'08', N'01', N'SECHURA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'20', N'08', N'02', N'BELLAVISTA DE LA UNION')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'20', N'08', N'03', N'BERNAL')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'20', N'08', N'04', N'CRISTO NOS VALGA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'20', N'08', N'05', N'VICE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'20', N'08', N'06', N'RINCONADA LLICUAR')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'00', N'00', N'PUNO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'01', N'00', N'PUNO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'01', N'01', N'PUNO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'01', N'02', N'ACORA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'01', N'03', N'AMANTANI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'01', N'04', N'ATUNCOLLA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'01', N'05', N'CAPACHICA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'01', N'06', N'CHUCUITO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'01', N'07', N'COATA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'01', N'08', N'HUATA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'01', N'09', N'MAÑAZO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'01', N'10', N'PAUCARCOLLA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'01', N'11', N'PICHACANI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'01', N'12', N'PLATERIA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'01', N'13', N'SAN ANTONIO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'01', N'14', N'TIQUILLACA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'01', N'15', N'VILQUE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'02', N'00', N'AZANGARO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'02', N'01', N'AZANGARO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'02', N'02', N'ACHAYA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'02', N'03', N'ARAPA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'02', N'04', N'ASILLO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'02', N'05', N'CAMINACA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'02', N'06', N'CHUPA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'02', N'07', N'JOSE DOMINGO CHOQUEHUANCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'02', N'08', N'MUÑANI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'02', N'09', N'POTONI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'02', N'10', N'SAMAN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'02', N'11', N'SAN ANTON')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'02', N'12', N'SAN JOSE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'02', N'13', N'SAN JUAN DE SALINAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'02', N'14', N'SANTIAGO DE PUPUJA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'02', N'15', N'TIRAPATA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'03', N'00', N'CARABAYA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'03', N'01', N'MACUSANI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'03', N'02', N'AJOYANI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'03', N'03', N'AYAPATA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'03', N'04', N'COASA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'03', N'05', N'CORANI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'03', N'06', N'CRUCERO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'03', N'07', N'ITUATA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'03', N'08', N'OLLACHEA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'03', N'09', N'SAN GABAN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'03', N'10', N'USICAYOS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'04', N'00', N'CHUCUITO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'04', N'01', N'JULI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'04', N'02', N'DESAGUADERO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'04', N'03', N'HUACULLANI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'04', N'04', N'KELLUYO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'04', N'05', N'PISACOMA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'04', N'06', N'POMATA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'04', N'07', N'ZEPITA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'05', N'00', N'EL COLLAO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'05', N'01', N'ILAVE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'05', N'02', N'CAPAZO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'05', N'03', N'PILCUYO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'05', N'04', N'SANTA ROSA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'05', N'05', N'CONDURIRI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'06', N'00', N'HUANCANE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'06', N'01', N'HUANCANE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'06', N'02', N'COJATA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'06', N'03', N'HUATASANI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'06', N'04', N'INCHUPALLA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'06', N'05', N'PUSI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'06', N'06', N'ROSASPATA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'06', N'07', N'TARACO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'06', N'08', N'VILQUE CHICO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'07', N'00', N'LAMPA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'07', N'01', N'LAMPA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'07', N'02', N'CABANILLA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'07', N'03', N'CALAPUJA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'07', N'04', N'NICASIO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'07', N'05', N'OCUVIRI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'07', N'06', N'PALCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'07', N'07', N'PARATIA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'07', N'08', N'PUCARA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'07', N'09', N'SANTA LUCIA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'07', N'10', N'VILAVILA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'08', N'00', N'MELGAR')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'08', N'01', N'AYAVIRI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'08', N'02', N'ANTAUTA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'08', N'03', N'CUPI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'08', N'04', N'LLALLI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'08', N'05', N'MACARI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'08', N'06', N'NUÑOA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'08', N'07', N'ORURILLO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'08', N'08', N'SANTA ROSA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'08', N'09', N'UMACHIRI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'09', N'00', N'MOHO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'09', N'01', N'MOHO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'09', N'02', N'CONIMA')
GO
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'09', N'03', N'HUAYRAPATA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'09', N'04', N'TILALI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'10', N'00', N'SAN ANTONIO DE PUTINA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'10', N'01', N'PUTINA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'10', N'02', N'ANANEA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'10', N'03', N'PEDRO VILCA APAZA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'10', N'04', N'QUILCAPUNCU')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'10', N'05', N'SINA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'11', N'00', N'SAN ROMAN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'11', N'01', N'JULIACA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'11', N'02', N'CABANA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'11', N'03', N'CABANILLAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'11', N'04', N'CARACOTO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'12', N'00', N'SANDIA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'12', N'01', N'SANDIA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'12', N'02', N'CUYOCUYO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'12', N'03', N'LIMBANI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'12', N'04', N'PATAMBUCO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'12', N'05', N'PHARA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'12', N'06', N'QUIACA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'12', N'07', N'SAN JUAN DEL ORO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'12', N'08', N'YANAHUAYA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'12', N'09', N'ALTO INAMBARI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'12', N'10', N'SAN PEDRO DE PUTINA PUNCO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'13', N'00', N'YUNGUYO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'13', N'01', N'YUNGUYO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'13', N'02', N'ANAPIA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'13', N'03', N'COPANI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'13', N'04', N'CUTURAPI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'13', N'05', N'OLLARAYA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'13', N'06', N'TINICACHI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'21', N'13', N'07', N'UNICACHI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'22', N'00', N'00', N'SAN MARTIN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'22', N'01', N'00', N'MOYOBAMBA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'22', N'01', N'01', N'MOYOBAMBA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'22', N'01', N'02', N'CALZADA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'22', N'01', N'03', N'HABANA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'22', N'01', N'04', N'JEPELACIO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'22', N'01', N'05', N'SORITOR')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'22', N'01', N'06', N'YANTALO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'22', N'02', N'00', N'BELLAVISTA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'22', N'02', N'01', N'BELLAVISTA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'22', N'02', N'02', N'ALTO BIAVO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'22', N'02', N'03', N'BAJO BIAVO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'22', N'02', N'04', N'HUALLAGA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'22', N'02', N'05', N'SAN PABLO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'22', N'02', N'06', N'SAN RAFAEL')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'22', N'03', N'00', N'EL DORADO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'22', N'03', N'01', N'SAN JOSE DE SISA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'22', N'03', N'02', N'AGUA BLANCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'22', N'03', N'03', N'SAN MARTIN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'22', N'03', N'04', N'SANTA ROSA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'22', N'03', N'05', N'SHATOJA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'22', N'04', N'00', N'HUALLAGA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'22', N'04', N'01', N'SAPOSOA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'22', N'04', N'02', N'ALTO SAPOSOA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'22', N'04', N'03', N'EL ESLABON')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'22', N'04', N'04', N'PISCOYACU')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'22', N'04', N'05', N'SACANCHE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'22', N'04', N'06', N'TINGO DE SAPOSOA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'22', N'05', N'00', N'LAMAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'22', N'05', N'01', N'LAMAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'22', N'05', N'02', N'ALONSO DE ALVARADO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'22', N'05', N'03', N'BARRANQUITA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'22', N'05', N'04', N'CAYNARACHI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'22', N'05', N'05', N'CUÑUMBUQUI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'22', N'05', N'06', N'PINTO RECODO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'22', N'05', N'07', N'RUMISAPA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'22', N'05', N'08', N'SAN ROQUE DE CUMBAZA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'22', N'05', N'09', N'SHANAO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'22', N'05', N'10', N'TABALOSOS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'22', N'05', N'11', N'ZAPATERO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'22', N'06', N'00', N'MARISCAL CACERES')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'22', N'06', N'01', N'JUANJUI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'22', N'06', N'02', N'CAMPANILLA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'22', N'06', N'03', N'HUICUNGO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'22', N'06', N'04', N'PACHIZA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'22', N'06', N'05', N'PAJARILLO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'22', N'07', N'00', N'PICOTA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'22', N'07', N'01', N'PICOTA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'22', N'07', N'02', N'BUENOS AIRES')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'22', N'07', N'03', N'CASPISAPA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'22', N'07', N'04', N'PILLUANA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'22', N'07', N'05', N'PUCACACA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'22', N'07', N'06', N'SAN CRISTOBAL')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'22', N'07', N'07', N'SAN HILARION')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'22', N'07', N'08', N'SHAMBOYACU')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'22', N'07', N'09', N'TINGO DE PONASA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'22', N'07', N'10', N'TRES UNIDOS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'22', N'08', N'00', N'RIOJA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'22', N'08', N'01', N'RIOJA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'22', N'08', N'02', N'AWAJUN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'22', N'08', N'03', N'ELIAS SOPLIN VARGAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'22', N'08', N'04', N'NUEVA CAJAMARCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'22', N'08', N'05', N'PARDO MIGUEL')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'22', N'08', N'06', N'POSIC')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'22', N'08', N'07', N'SAN FERNANDO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'22', N'08', N'08', N'YORONGOS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'22', N'08', N'09', N'YURACYACU')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'22', N'09', N'00', N'SAN MARTIN')
GO
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'22', N'09', N'01', N'TARAPOTO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'22', N'09', N'02', N'ALBERTO LEVEAU')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'22', N'09', N'03', N'CACATACHI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'22', N'09', N'04', N'CHAZUTA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'22', N'09', N'05', N'CHIPURANA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'22', N'09', N'06', N'EL PORVENIR')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'22', N'09', N'07', N'HUIMBAYOC')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'22', N'09', N'08', N'JUAN GUERRA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'22', N'09', N'09', N'LA BANDA DE SHILCAYO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'22', N'09', N'10', N'MORALES')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'22', N'09', N'11', N'PAPAPLAYA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'22', N'09', N'12', N'SAN ANTONIO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'22', N'09', N'13', N'SAUCE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'22', N'09', N'14', N'SHAPAJA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'22', N'10', N'00', N'TOCACHE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'22', N'10', N'01', N'TOCACHE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'22', N'10', N'02', N'NUEVO PROGRESO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'22', N'10', N'03', N'POLVORA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'22', N'10', N'04', N'SHUNTE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'22', N'10', N'05', N'UCHIZA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'23', N'00', N'00', N'TACNA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'23', N'01', N'00', N'TACNA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'23', N'01', N'01', N'TACNA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'23', N'01', N'02', N'ALTO DE LA ALIANZA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'23', N'01', N'03', N'CALANA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'23', N'01', N'04', N'CIUDAD NUEVA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'23', N'01', N'05', N'INCLAN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'23', N'01', N'06', N'PACHIA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'23', N'01', N'07', N'PALCA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'23', N'01', N'08', N'POCOLLAY')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'23', N'01', N'09', N'SAMA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'23', N'01', N'10', N'CORONEL GREGORIO ALBARRACIN LANCHIPA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'23', N'01', N'11', N'LA YARADA LOS PALOS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'23', N'02', N'00', N'CANDARAVE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'23', N'02', N'01', N'CANDARAVE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'23', N'02', N'02', N'CAIRANI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'23', N'02', N'03', N'CAMILACA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'23', N'02', N'04', N'CURIBAYA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'23', N'02', N'05', N'HUANUARA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'23', N'02', N'06', N'QUILAHUANI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'23', N'03', N'00', N'JORGE BASADRE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'23', N'03', N'01', N'LOCUMBA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'23', N'03', N'02', N'ILABAYA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'23', N'03', N'03', N'ITE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'23', N'04', N'00', N'TARATA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'23', N'04', N'01', N'TARATA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'23', N'04', N'02', N'HEROES ALBARRACIN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'23', N'04', N'03', N'ESTIQUE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'23', N'04', N'04', N'ESTIQUE-PAMPA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'23', N'04', N'05', N'SITAJARA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'23', N'04', N'06', N'SUSAPAYA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'23', N'04', N'07', N'TARUCACHI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'23', N'04', N'08', N'TICACO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'24', N'00', N'00', N'TUMBES')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'24', N'01', N'00', N'TUMBES')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'24', N'01', N'01', N'TUMBES')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'24', N'01', N'02', N'CORRALES')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'24', N'01', N'03', N'LA CRUZ')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'24', N'01', N'04', N'PAMPAS DE HOSPITAL')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'24', N'01', N'05', N'SAN JACINTO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'24', N'01', N'06', N'SAN JUAN DE LA VIRGEN')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'24', N'02', N'00', N'CONTRALMIRANTE VILLAR')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'24', N'02', N'01', N'ZORRITOS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'24', N'02', N'02', N'CASITAS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'24', N'02', N'03', N'CANOAS DE PUNTA SAL')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'24', N'03', N'00', N'ZARUMILLA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'24', N'03', N'01', N'ZARUMILLA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'24', N'03', N'02', N'AGUAS VERDES')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'24', N'03', N'03', N'MATAPALO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'24', N'03', N'04', N'PAPAYAL')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'25', N'00', N'00', N'UCAYALI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'25', N'01', N'00', N'CORONEL PORTILLO')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'25', N'01', N'01', N'CALLERIA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'25', N'01', N'02', N'CAMPOVERDE')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'25', N'01', N'03', N'IPARIA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'25', N'01', N'04', N'MASISEA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'25', N'01', N'05', N'YARINACOCHA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'25', N'01', N'06', N'NUEVA REQUENA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'25', N'01', N'07', N'MANANTAY')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'25', N'02', N'00', N'ATALAYA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'25', N'02', N'01', N'RAYMONDI')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'25', N'02', N'02', N'SEPAHUA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'25', N'02', N'03', N'TAHUANIA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'25', N'02', N'04', N'YURUA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'25', N'03', N'00', N'PADRE ABAD')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'25', N'03', N'01', N'PADRE ABAD')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'25', N'03', N'02', N'IRAZOLA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'25', N'03', N'03', N'CURIMANA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'25', N'03', N'04', N'NESHUYA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'25', N'03', N'05', N'ALEXANDER VON HUMBOLDT')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'25', N'04', N'00', N'PURUS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'25', N'04', N'01', N'PURUS')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'40', N'50', N'02', N'ACHOMA')
INSERT [dbo].[Ubigeo] ([DepartamentoId], [ProvinciaId], [DistritoId], [Nombre]) VALUES (N'99', N'00', N'00', N'TORONTO')
ALTER TABLE [dbo].[llxny_user] ADD  CONSTRAINT [DF_llxny_user_tms]  DEFAULT (getdate()) FOR [tms]
GO
/****** Object:  StoredProcedure [dbo].[usp_Configuracion_UsuarioListar]    Script Date: 02/06/2020 9:31:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[usp_Configuracion_UsuarioListar]
AS
BEGIN
	DECLARE @SepListas CHAR(1)='¯',    
	@SepFilas CHAR(1)='¬',    
	@SepColumna CHAR(1)='¦',    
	@SepComodin CHAR(1)='̈ '     

	SELECT
		ISNULL( STUFF((
		SELECT @sepFilas+
			cast(rowid as varchar(50))+@sepColumna+
			ISNULL([login],'')+@SepColumna+
			ISNULL(firstname,'')  +@sepColumna+
			ISNULL(lastname,'')+@sepColumna+
			ISNULL(email,'')+@sepColumna+
			ISNULL(cast(TipoUsuario as varchar(89)),'')+@sepColumna+
			ISNULL(trim([statut]),'')+@sepColumna+
			''
	FROM llxny_user order by rowid desc
	FOR XML PATH, TYPE).value('.[1]','nvarchar(max)'),1,1,''),'')
	+@SepListas+  
	ISNULL(STUFF((  
		 SELECT @sepFilas +  
		 Codigo +@sepColumna+  
		 Descripcion  
	 FROM [dbo].[MultitablaValor] WHERE IdMultiTabla='1' AND EstadoRegistro='REG'  
	 FOR XML PATH, TYPE).value('.[1]','nvarchar(max)'),1,1,''), '')
 END  
GO
/****** Object:  StoredProcedure [dbo].[usp_General_LineChart]    Script Date: 02/06/2020 9:31:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[usp_General_LineChart]   
as    
BEGIN    
 DECLARE @SepListas CHAR(1)='¯',  
 @SepFilas CHAR(1)='¬',  
 @SepColumna CHAR(1)='¦',  
 @SepComodin CHAR(1)='̈ '

 SELECT ISNULL( STUFF((
		SELECT @SepFilas+CONVERT(varchar(13), (DATEDIFF(SECOND,'1970-01-01',dateadd(day,1,fechaemision))*cast(1000 as bigint)))+','+CAST(SUM(TotalMontoPagar)AS VARCHAR)
		FROM [BoletaElectronica] where year(FechaEmision) = '2020' and TipoDocumento = '01' GROUP BY FechaEmision order by FechaEmision
	 FOR XML PATH, TYPE).value('.[1]','nvarchar(max)'),1,1,''), '')
	 +@SepListas+  
	ISNULL( STUFF((
		SELECT @SepFilas+CONVERT(varchar(13), (DATEDIFF(SECOND,'1970-01-01',dateadd(day,1,fechaemision))*cast(1000 as bigint)))+','+CAST(SUM(TotalMontoPagar)AS VARCHAR)
		FROM [BoletaElectronica] where year(FechaEmision) = '2020' and TipoDocumento = '03' GROUP BY FechaEmision order by FechaEmision
	 FOR XML PATH, TYPE).value('.[1]','nvarchar(max)'),1,1,''), '')
END

 --SELECT ISNULL( STUFF((
	--	SELECT @SepFilas+CONVERT(varchar(10),fechaemision, 121)+@SepColumna+CAST(SUM(TotalMontoPagar)AS VARCHAR)
	--	FROM [BoletaElectronica] where year(FechaEmision) = '2020' and TipoDocumento = '01' GROUP BY FechaEmision order by FechaEmision
	-- FOR XML PATH, TYPE).value('.[1]','nvarchar(max)'),1,1,''), '')
	-- +@SepListas+  ''
	--ISNULL( STUFF((
	--	SELECT @SepFilas+CONVERT(varchar(13), (DATEDIFF(SECOND,'1970-01-01',dateadd(day,1,fechaemision))*cast(1000 as bigint)))+','+CAST(SUM(TotalMontoPagar)AS VARCHAR)
	--	FROM [BoletaElectronica] where year(FechaEmision) = '2020' and TipoDocumento = '03' GROUP BY FechaEmision order by FechaEmision
	-- FOR XML PATH, TYPE).value('.[1]','nvarchar(max)'),1,1,''), '')
--END

--SELECT (DATEDIFF(SECOND,'1970-01-01',dateadd(day,1,fechaemision))*cast(1000 as bigint)) as fecha_unix, SUM(mtototal) as total FROM [Comprobante] where year(fechaemision) = '2019' GROUP BY fechaemision order by fechaemision
GO
/****** Object:  StoredProcedure [dbo].[uspClienteGrabar]    Script Date: 02/06/2020 9:31:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--exec uspExamenGrabar'D215055C-A579-4E87-847B-601CDA188F64¯'

create PROCEDURE  [dbo].[uspClienteGrabar]
@lstparametros varchar(max)
AS 
BEGIN
	BEGIN TRY
		BEGIN TRANSACTION

		DECLARE @SepListas CHAR(1)='¯',
		@SepFilas CHAR(1)='¬',
		@SepColumna CHAR(1)='¦',
		@SepComodin CHAR(1)='̈ ' 

		Declare @listaUsuario nvarchar(100), @listaData nvarchar(max)

		SELECT @listaUsuario=field1, 
				@listaData=field2
		FROM fnSplitStringTable05(@lstparametros,@SepComodin,@SepListas)


		/*OBTENER USUARIO*/
		  Declare @codigoUsuario varchar(20),@idUsuario int,@token nvarchar(100)

        SELECT @token=field1    
        FROM fnSplitStringTable05(@listaUsuario,@SepFilas,@SepColumna)
       
        SELECT             
         @codigoUsuario=CodigoUsuario            
        FROM Token WHERE GUIDToken=@token  
       
        SELECT @idUsuario = rowid FROM [dbo].[llxny_user] where login=@codigoUsuario  
declare @Idcliente	int	
declare @Nombre	    varchar(100)	
declare @Direccion	varchar(100)	
declare @Correo	     varchar(100)	
declare @telefono	int	
declare @Estado     varchar(1)	

		

	
		SELECT 
		    
			@Idcliente=cast(field1 as int),
			@Nombre	  =field2,
			@Direccion  =field3,
			@Correo  =field4,
			@telefono  =field5

		FROM  [dbo].[fnSplitStringTable40](@listaData,@SepFilas,@SepColumna)
		IF @Idcliente=0
		BEGIN 

		  INSERT INTO  [dbo].[Cliente]
			(		

			Nombre	,
			Direccion,
			Correo,
			telefono,
			Estado 
			
			 )
			values (	

			@Nombre	,
			@Direccion,
			@Correo,
			@telefono,
			'A'
			)
			SET @Idcliente=@@IDENTITY
		END
		ELSE
		BEGIN
			
			
		  UPDATE [dbo].[Cliente] SET 

			Nombre=@Nombre,
			Direccion=@Direccion,
			Correo=@Correo,
			telefono=@telefono,
			Estado='A'

			WHERE Idcliente =@Idcliente 	    

		END
		 select @Idcliente
		COMMIT TRANSACTION
          
	END TRY
	BEGIN CATCH
	ROLLBACK TRANSACTION
		SELECT '-1¯'+ERROR_MESSAGE()
	END CATCH

END


GO
/****** Object:  StoredProcedure [dbo].[uspClienteListar]    Script Date: 02/06/2020 9:31:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--exec uspExamenListar

create PROCEDURE [dbo].[uspClienteListar]
as	
BEGIN

	DECLARE @SepListas CHAR(1)='¯',
	@SepFilas CHAR(1)='¬',
	@SepColumna CHAR(1)='¦',
	@SepComodin CHAR(1)='̈ ' 

SELECT ISNULL( STUFF((
		SELECT @SepFilas+
	cast(Idcliente as varchar(20))+@SepColumna+
	isnull(Nombre,'')+@SepColumna+
	isnull(Direccion,'')+@SepColumna+
	isnull(Correo,'')+@SepColumna+
	cast(telefono as varchar(20))+@SepColumna+
	isnull(Estado,'')

		   
         from [dbo].[Cliente]  where  Estado='A'

    FOR XML PATH, TYPE).value('.[1]','nvarchar(max)'),1,1,''), '')
	
END

GO
/****** Object:  StoredProcedure [dbo].[uspConfiguracionPalletColorGrabar]    Script Date: 02/06/2020 9:31:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[uspConfiguracionPalletColorGrabar]  
@lstparametros nvarchar(max)  
AS  
BEGIN        
 BEGIN TRY        
  BEGIN TRANSACTION   
 DECLARE @SepListas CHAR(1)='¯',  
 @SepFilas CHAR(1)='¬',  
 @SepColumna CHAR(1)='¦',  
 @SepComodin CHAR(1)='̈ '   
  
 Declare @MAIN_NAME nvarchar(max),  
 @MAIN_HEADER_AREA_PRINCIPAL_BC nvarchar(max),  
 @MAIN_HEADER_TEXT_NAME_EMPRESA_C nvarchar(max),  
 @MAIN_HEADER_TEXT_C nvarchar(max),  
 @MAIN_MENU_HEADER_AREA_PRINCIPAL_BC nvarchar(max),  
 @MAIN_MENU_HEADER_NAME_EMPRESA_C nvarchar(max),  
 @MAIN_MENU_AREA_BC nvarchar(max), 
  @MAIN_MENU_TEXT_ICON_C nvarchar(max),  
 @MAIN_MENU_SUBMENU_AREA_BC nvarchar(max),  
 @MAIN_MODAL_HEADER_AREA_BC nvarchar(max),  
 @MAIN_MODAL_HEADER_TEXT_C nvarchar(max), 
  @MAIN_MODAL_FOOTER_AREA_BC nvarchar(max)
  
 SELECT  
 @MAIN_NAME =field1,
 @MAIN_HEADER_AREA_PRINCIPAL_BC =field2,
 @MAIN_HEADER_TEXT_NAME_EMPRESA_C =field3,
 @MAIN_HEADER_TEXT_C =field4,  
 @MAIN_MENU_HEADER_AREA_PRINCIPAL_BC =field5,  
 @MAIN_MENU_HEADER_NAME_EMPRESA_C =field6,  
 @MAIN_MENU_AREA_BC =field7, 
  @MAIN_MENU_TEXT_ICON_C =field8,  
 @MAIN_MENU_SUBMENU_AREA_BC =field9, 
 @MAIN_MODAL_HEADER_AREA_BC=field10,  
 @MAIN_MODAL_HEADER_TEXT_C =field11, 
  @MAIN_MODAL_FOOTER_AREA_BC =field12 
 FROM fnSplitStringTable20(@lstparametros,@SepFilas,@SepColumna)  
  
  
 update Constantes set Valor=@MAIN_NAME where Nombre='MAIN_NAME'  
 update Constantes set Valor=@MAIN_HEADER_AREA_PRINCIPAL_BC where Nombre='MAIN_HEADER_AREA_PRINCIPAL_BC'  
 update Constantes set Valor=@MAIN_HEADER_TEXT_NAME_EMPRESA_C where Nombre='MAIN_HEADER_TEXT_NAME_EMPRESA_C'  
 update Constantes set Valor=@MAIN_HEADER_TEXT_C where Nombre='MAIN_HEADER_TEXT_C'  
 update Constantes set Valor=@MAIN_MENU_HEADER_AREA_PRINCIPAL_BC where Nombre='MAIN_MENU_HEADER_AREA_PRINCIPAL_BC'  
 update Constantes set Valor=@MAIN_MENU_HEADER_NAME_EMPRESA_C where Nombre='MAIN_MENU_HEADER_NAME_EMPRESA_C'  
 update Constantes set Valor=@MAIN_MENU_AREA_BC where Nombre='MAIN_MENU_AREA_BC'
 update Constantes set Valor=@MAIN_MENU_TEXT_ICON_C where Nombre='MAIN_MENU_TEXT_ICON_C'  
 update Constantes set Valor=@MAIN_MENU_SUBMENU_AREA_BC where Nombre='MAIN_MENU_SUBMENU_AREA_BC'  
 update Constantes set Valor=@MAIN_MODAL_HEADER_AREA_BC where Nombre='MAIN_MODAL_HEADER_AREA_BC'  
 update Constantes set Valor=@MAIN_MODAL_HEADER_TEXT_C where Nombre='MAIN_MODAL_HEADER_TEXT_C'  
 update Constantes set Valor=@MAIN_MODAL_FOOTER_AREA_BC where Nombre='MAIN_MODAL_FOOTER_AREA_BC'   
  
  SELECT 'OK'
 COMMIT TRANSACTION
 END TRY        
 BEGIN CATCH        
 ROLLBACK TRANSACTION        
  SELECT '-1¯'+ERROR_MESSAGE()        
 END CATCH              
END 
  
  
  
  
  
  
  
GO
/****** Object:  StoredProcedure [dbo].[uspConfiguracionPalletColorListar]    Script Date: 02/06/2020 9:31:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[uspConfiguracionPalletColorListar]    
AS    
BEGIN    
 DECLARE @SepListas CHAR(1)='¯',    
 @SepFilas CHAR(1)='¬',    
 @SepColumna CHAR(1)='¦',    
 @SepComodin CHAR(1)='̈ '     
    
 SELECT ISNULL( STUFF((    
  --lista documento    
  SELECT @SepFilas +ISNULL(Valor, '')   
  FROM Constantes    
 FOR XML PATH, TYPE).value('.[1]','nvarchar(max)'),1,1,''), '')    
 +@SepListas+
  ISNULL( STUFF((
		SELECT @SepFilas+
		CAST(m.MenuId AS varchar)+@SepColumna +
		isnull(m.NombreMenu,'')+@SepColumna +
		ISNULL(m.URLPagina,'')+@SepColumna +
		isnull(CAST(MenuPadreId AS varchar), '')+@SepColumna +
		isnull(m.Comentario, '')+@SepColumna +
		isnull(m.IconName,'')+@SepColumna + 
		ISNULL(m.EstadoRegistro, '')+@SepColumna + 
		ISNULL(CAST(m.numeroOrden AS varchar), '')
		FROM Menu m ORDER BY m.numeroOrden ASC--where m.EstadoRegistro='REG'
 FOR XML PATH, TYPE).value('.[1]','nvarchar(max)'),1,1,''), '')
END    
    
GO
/****** Object:  StoredProcedure [dbo].[uspEliminarGenericoGrabar]    Script Date: 02/06/2020 9:31:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[uspEliminarGenericoGrabar]    
@lstparametros varchar(max)    
AS     
BEGIN    
 BEGIN TRY    
  BEGIN TRANSACTION    
    
  DECLARE @SepListas CHAR(1)='¯',    
  @SepFilas CHAR(1)='¬',    
  @SepColumna CHAR(1)='¦',    
  @SepComodin CHAR(1)='̈ '     
    
  Declare @listaUsuario nvarchar(100), @listaData nvarchar(5),@TablaId varchar(5),@PersonaId int    
    
  SELECT @listaUsuario=field1,     
   @listaData=field2,    
   @TablaId=CAST(field3 as int)  
  FROM fnSplitStringTable05(@lstparametros,@SepComodin,@SepListas)    
    
  --Declare @codigoUsuario varchar(20),@idUsuario int,@token nvarchar(100)    
  --SET @idUsuario = CONVERT(INT,@listaUsuario)    
    
  /*OBTENER USUARIO*/  

Declare @codigoUsuario varchar(20),@idUsuario int,@token nvarchar(100)

        SELECT @token=field1    
        FROM fnSplitStringTable05(@listaUsuario,@SepFilas,@SepColumna)
       
        SELECT             
         @codigoUsuario=CodigoUsuario            
        FROM Token WHERE GUIDToken=@token  
       
        SELECT @idUsuario = rowid FROM [dbo].[llxny_user] where login=@codigoUsuario  
       
 
   
    
      select @listaData    

 COMMIT TRANSACTION    
 END TRY    
 BEGIN CATCH    
  SELECT '-1¯'+ERROR_MESSAGE()    
  ROLLBACK TRANSACTION    
 END CATCH    
    
END    
    
    
GO
/****** Object:  StoredProcedure [dbo].[uspInvoiceConsultar]    Script Date: 02/06/2020 9:31:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec uspInvoiceConsultar'1'
CREATE procedure [dbo].[uspInvoiceConsultar]
@lstparametros as nvarchar(max)
as
BEGIN
DECLARE @SepListas CHAR(1)='¯',
	@SepFilas CHAR(1)='¬',
	@SepColumna CHAR(1)='¦',
	@SepComodin CHAR(1)='¨'

	declare @Id as int

	select 
	@Id= cast(@lstparametros as int)


SELECT ISNULL( STUFF((
	SELECT @SepFilas+

	     
		 
		 isnull(Codigo,'') +@SepColumna+
		 isnull(Nombre,'')+@SepColumna+
		 isnull(convert(varchar,fechaentrega,103),'')+@SepColumna+
		 isnull(Direccion,'') +@SepColumna+
		 isnull(convert(varchar,fechaterminada,103),'')+@SepColumna+
		 isnull(Correo,'') +@SepColumna+
		cast(telefono as varchar(10))+@SepColumna+
		cast(subtotal as varchar(10))+@SepColumna+
		cast(descuento as varchar(10))+@SepColumna+
		cast(subtotalmenos as varchar(10))+@SepColumna+
		cast(igv as varchar(10))+@SepColumna+
		cast(igvtotal as varchar(10))+@SepColumna+
		cast(total as varchar(10))+@SepColumna+
		cast(Id as varchar(20))
		   
         from [dbo].[Invoice_Cab] i  where Id=@Id AND Estado='A'
	FOR XML PATH, TYPE).value('.[1]','nvarchar(max)'),1,1,''), '')
	+@SepListas+
	ISNULL( STUFF((
	SELECT @SepFilas+

	     isnull(CAST(IdDetalle AS VARCHAR(50)),'')+@SepColumna+
	     isnull(Descripcion ,'')+@SepColumna+
		 cast(Cantidad as varchar(20))+@SepColumna+
	     isnull(CAST(PrecioU AS VARCHAR(50)),'')+@SepColumna+
	     isnull(CAST(TotalDEt AS VARCHAR(50)),'')
		
		from [dbo].[Invoice_Det] i where Id=@Id AND Estado='A'
	FOR XML PATH, TYPE).value('.[1]','nvarchar(max)'),1,1,''), '')
END
GO
/****** Object:  StoredProcedure [dbo].[uspInvoiceGrabar]    Script Date: 02/06/2020 9:31:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--exec uspInvoiceGrabar 'E56AC8DE-C69E-472B-9919-46B797CAF1FB¯2¦564¦Ysrael¦29/04/2020¦ashfjlsa¦20/05/2020¦Moise2017_19_@hotmail.com¦918273645¦12¦1¦2¦34¦2¦5¯2¦Webcam¦1¦120¦120'
--select*from Invoice_Cab
CREATE PROCEDURE  [dbo].[uspInvoiceGrabar]
@lstparametros varchar(max)
AS 
BEGIN
	BEGIN TRY
		BEGIN TRANSACTION

		DECLARE @SepListas CHAR(1)='¯',
		@SepFilas CHAR(1)='¬',
		@SepColumna CHAR(1)='¦',
		@SepComodin CHAR(1)='̈ ' 

		Declare @listaUsuario nvarchar(100), @listaData nvarchar(max),@listaDataDetalle as nvarchar(max)

		SELECT @listaUsuario=field1, 
				@listaData=field2,
				@listaDataDetalle = field3
		FROM fnSplitStringTable05(@lstparametros,@SepComodin,@SepListas)


		/*OBTENER USUARIO*/
		  Declare @codigoUsuario varchar(20),@idUsuario int,@token nvarchar(100)

        SELECT @token=field1    
        FROM fnSplitStringTable05(@listaUsuario,@SepFilas,@SepColumna)
       
        SELECT             
         @codigoUsuario=CodigoUsuario            
        FROM Token WHERE GUIDToken=@token  
       
        SELECT @idUsuario = rowid FROM [dbo].[llxny_user] where login=@codigoUsuario  

		declare @Id	            int	
		declare @Codigo	        varchar(10)	
		declare @Nombre	        varchar(100)	
		declare @fechaentrega	date	
		declare @Direccion	    varchar(100)	
		declare @fechaterminada	date	
		declare @Correo	        varchar(100)	
		declare @telefono	    int	
		declare @subtotal	    decimal(8, 4)	
		declare @descuento	    decimal(8, 4)	
		declare @subtotalmenos	decimal(8, 4)	
		declare @igv	        decimal(8, 4)	
		declare @igvtotal	    decimal(8, 4)	
		declare @total	        decimal(8, 4)	
		declare @Estado	        varchar(1)	
		
		
		      

		SELECT 
		@Id	           	 = cast(field1 as int),
		@Codigo	       	 = field2,
		@Nombre	       	 = field3,
		@fechaentrega	 = convert(date,field4),
		@Direccion	   	 = field5,
		@fechaterminada	 = convert(date,field6),
		@Correo	       	 = field7,
		@telefono	   	 = cast(field8 as int), 
		@subtotal	   	 = cast(field9 as decimal(8,4)), 
		@descuento	   	 = cast(field10 as decimal(8,4)),
		@subtotalmenos	 = cast(field11 as decimal(8,4)) ,
		@igv	       	 = cast(field12 as decimal(8,4)) ,
		@igvtotal	   	 = cast(field13 as decimal(8,4)) ,
		@total	         = cast(field14 as decimal(8,4))     
		
			

		FROM  [dbo].[fnSplitStringTable40](@listaData,@SepFilas,@SepColumna)
		IF @Id=0
		BEGIN 

		  INSERT INTO  [dbo].[Invoice_Cab]
			(		
			Codigo	       	,
			Nombre	       	,
			fechaentrega	,
			Direccion	   	,
			fechaterminada	,
			Correo	       	,
			telefono	   	,
			subtotal	   	,
			descuento	   	,
			subtotalmenos	,
			igv	       		,
			igvtotal	   	,
			total	        ,
			Estado   
			 )
			values (      	
			@Codigo	       	,
			@Nombre	       	,
			@fechaentrega	,
			@Direccion	   	,
			@fechaterminada	,
			@Correo	       	,
			@telefono	   	,
			@subtotal	   	,
			@descuento	   	,
			@subtotalmenos	,
			@igv	       	,
			@igvtotal	   	,
			@total	        ,
			'A'   
			)
			SET @Id=@@IDENTITY

			insert into [dbo].[Invoice_Det]

			(Id,Descripcion,Cantidad,PrecioU,TotalDEt,Estado)

			select 
			@Id,
			field2 ,
			cast(field3 as int),
			cast(field4 as decimal(8,4)),
			cast(field5 as decimal(8,4)),
			'A'

       	from [dbo].[fnSplitStringTable40](@listaDataDetalle,@SepFilas,@SepColumna)

		END
		ELSE
		BEGIN
			
			
		  UPDATE [dbo].[Invoice_Cab] SET 
			Codigo	       	=@Codigo	       	,
			Nombre	       	=@Nombre	       	,
			fechaentrega	=@fechaentrega	,
			Direccion	   	=@Direccion	   	,
			fechaterminada	=@fechaterminada	,
			Correo	       	=@Correo	       	,
			telefono	   	=@telefono	   	,
			subtotal	   	=@subtotal	   	,
			descuento	   	=@descuento	   	,
			subtotalmenos	=@subtotalmenos	,
			igv	       		=@igv	       	,
			igvtotal	   	=@igvtotal	   	,
			total	        =@total	        ,
			Estado   ='A'	 

			WHERE Id =@Id	    
			
			DECLARE @tablaDetalle as table 
		
			(
            IdDetalle	int	,
			Id	int	,
			Descripcion	varchar(100)	,
			Cantidad	int	,
			PrecioU	decimal(8, 4),	
			TotalDEt	decimal(8, 4)	,
			Estado	varchar(1)	
		
			)
			INSERT INTO @tablaDetalle
            select 
		    cast(field1 as int),
		    @Id,
		    field2 ,
		    cast(field3 as int),
			cast(field4 as decimal(8, 4)),
			cast(field5 as decimal(8, 4)),
			'A'
		    FROM [dbo].[fnSplitStringTable40](@listaDataDetalle,@SepFilas,@SepColumna)

			----1 CASO
		   UPDATE TD SET

		   Descripcion=TB.Descripcion,
		   Cantidad=TB.Cantidad,
		   PrecioU=TB.PrecioU,
		   TotalDEt=TB.TotalDEt

		   FROM [dbo].[Invoice_Det] TD INNER JOIN @tablaDetalle TB ON TD.Id=TB.Id
		   WHERE TD.Id=@Id

		   
		   --2 CASO
		   UPDATE TD SET
           Estado='I'

		   FROM [dbo].[Invoice_Det] TD LEFT JOIN @tablaDetalle TB ON TD.Id=TB.Id
		   WHERE TD.Id=@Id AND TB.Id IS NULL



		   --3 CASO
		  
		    INSERT INTO [dbo].[Invoice_Det] (Id,Descripcion,Cantidad,PrecioU,TotalDEt,Estado)

            SELECT 
            @Id ,
			Descripcion ,
			Cantidad ,
			PrecioU,
			TotalDEt,
			Estado
			FROM @tablaDetalle WHERE Id=0

		END
		select @Id
		COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
	ROLLBACK TRANSACTION
		SELECT '-1¯'+ERROR_MESSAGE()
	END CATCH

END

GO
/****** Object:  StoredProcedure [dbo].[uspInvoiceListar]    Script Date: 02/06/2020 9:31:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--exec uspTicketListar
CREATE PROCEDURE [dbo].[uspInvoiceListar]
as	
BEGIN

	DECLARE @SepListas CHAR(1)='¯',
	@SepFilas CHAR(1)='¬',
	@SepColumna CHAR(1)='¦',
	@SepComodin CHAR(1)='̈ ' 

SELECT ISNULL( STUFF((
		SELECT @SepFilas+
		cast(Id as varchar(20))+@SepColumna+
		 isnull(Codigo,'') +@SepColumna+
		 isnull(Nombre,'')+@SepColumna+		 
		 isnull(Direccion,'') +@SepColumna+
		 isnull(Correo,'') +@SepColumna+
		 ''
       
	   from [dbo].[Invoice_Cab] where  Estado='A'

    FOR XML PATH, TYPE).value('.[1]','nvarchar(max)'),1,1,''), '')
	
END


GO
/****** Object:  StoredProcedure [dbo].[uspInvoiceListas]    Script Date: 02/06/2020 9:31:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--select*from Cliente2
--exec uspInvoiceListas
CREATE PROCEDURE [dbo].[uspInvoiceListas]
AS
BEGIN
	DECLARE @SepListas CHAR(1)='¯',
	@SepFilas CHAR(1)='¬',
	@SepColumna CHAR(1)='¦',
	@SepComodin CHAR(1)='̈ ' 

	SELECT ISNULL( STUFF((
		--lista tipo persona
		SELECT @SepFilas + cast(Idcliente as varchar(20))+@SepColumna+Nombre+@SepColumna+Direccion+@SepColumna+Correo+@SepColumna+cast(telefono as varchar(20)) 
		FROM [dbo].[Cliente]
	FOR XML PATH, TYPE).value('.[1]','nvarchar(max)'),1,1,''), '')
	+@SepListas+
	 ISNULL( STUFF((
		--lista tipo persona
		SELECT @SepFilas + cast(IdProducto as varchar(20))+@SepColumna+Descripcion+@SepColumna+cast(Precio as varchar(20))
		FROM [dbo].[Producto]
	FOR XML PATH, TYPE).value('.[1]','nvarchar(max)'),1,1,''), '')

END
GO
/****** Object:  StoredProcedure [dbo].[uspMenu_Listar]    Script Date: 02/06/2020 9:31:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[uspMenu_Listar]
@lstParametros nvarchar(max)      
as    
BEGIN    
 DECLARE @SepListas CHAR(1)='¯',  
 @SepFilas CHAR(1)='¬',  
 @SepColumna CHAR(1)='¦',  
 @SepComodin CHAR(1)='̈ '
  
  /*OBTENER USUARIO*/  
  Declare @codigoUsuario varchar(20),@idUsuario varchar(20),@token nvarchar(100)  
  SELECT @token=field1  
  FROM fnSplitStringTable05(@lstParametros,@SepFilas,@SepColumna)  
  
  SELECT   
   @codigoUsuario=CodigoUsuario  
  FROM Token WHERE GUIDToken=@token and EstadoRegistro='REG'  
   
  SELECT @idUsuario = rowid FROM [dbo].[llxny_user] where login=@codigoUsuario 

 SELECT ISNULL( STUFF((
		SELECT @SepFilas+
		CAST(m.MenuId AS varchar)+@SepColumna +
		isnull(NombreMenu,'')+@SepColumna +
		ISNULL(URLPagina,'')+@SepColumna +
		isnull(CAST(MenuPadreId AS varchar), '')+@SepColumna +
		isnull(m.IconName,'')+@SepColumna + 
		ISNULL(Comentario, '')
		FROM Permisos p inner join  Menu m on p.MenuId=m.MenuId where  p.UsuarioId=@idUsuario and m.EstadoRegistro='REG' ORDER BY m.numeroOrden ASC
	FOR XML PATH, TYPE).value('.[1]','nvarchar(max)'),1,1,''), '')
	 +@SepListas+
  ISNULL( STUFF((  
  SELECT @SepFilas +Valor  
  FROM Constantes  
 FOR XML PATH, TYPE).value('.[1]','nvarchar(max)'),1,1,''), '')  
END  


--exec [uspMenu_Listar] '53AA8549-C8F7-4D16-8B43-3D5F8EA32FAE'
--select *from Constantes 
GO
/****** Object:  StoredProcedure [dbo].[uspMenuGrabar]    Script Date: 02/06/2020 9:31:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[uspMenuGrabar]
@lstparametros varchar(max)  
AS  
BEGIN  
 BEGIN TRY  
  BEGIN TRANSACTION  
  DECLARE @SepListas CHAR(1)='¯',  
  @SepFilas CHAR(1)='¬',  
  @SepColumna CHAR(1)='¦',  
  @SepComodin CHAR(1)='̈ '   

        declare @MenuId int   
        declare @NombreMenu nvarchar(100)   
        declare @URLPagina nvarchar(100)   
        declare @MenuPadreId int   
		declare @Comentario nvarchar(50)  
		declare @IconName nvarchar(20)  
		declare @EstadoRegistro char(3) 
		declare @numeroOrden int
  
  SELECT  
    
  @MenuId=cast(field1 as int),  
  @NombreMenu =field2,  
  @URLPagina = field3,   
  @MenuPadreId=cast(field4 as int),  
  @Comentario=field5,  
  @IconName =field6,
  @EstadoRegistro =field7,
  @numeroOrden =cast(field8 as int)   
  FROM fnSplitStringTable10(@lstparametros,@SepFilas,@SepColumna)  
   
   UPDATE [dbo].[Menu] SET    
		NombreMenu =@NombreMenu,  
		URLPagina = @URLPagina,
		MenuPadreId=@MenuPadreId,  
		Comentario=@Comentario,  
		IconName =@IconName,
		EstadoRegistro =@EstadoRegistro,
		numeroOrden =@numeroOrden  
   WHERE MenuId=@MenuId  

  select @MenuId  
  COMMIT TRANSACTION  
  
 END TRY  
 BEGIN CATCH  
 ROLLBACK TRANSACTION  
  SELECT '-1¯'+ERROR_MESSAGE()  
 END CATCH  
  
END  
  
GO
/****** Object:  StoredProcedure [dbo].[uspMenuObtenerPorId]    Script Date: 02/06/2020 9:31:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[uspMenuObtenerPorId]
@lstparametros as nvarchar(max)
as
BEGIN
   	DECLARE @SepListas CHAR(1)='¯',
	@SepFilas CHAR(1)='¬',
	@SepColumna CHAR(1)='¦',
	@SepComodin CHAR(1)='̈ ' 

	declare @MenuId as int
	select
	@MenuId=cast(field1 as int)
	from [dbo].[fnSplitStringTable10] (@lstparametros,@SepFilas,@SepColumna)

SELECT ISNULL( STUFF((
		SELECT @SepFilas+
            isnull(cast(MenuId as varchar(20)),'')+@SepColumna+
			isnull(NombreMenu,'')+@SepColumna+
         	isnull(URLPagina,'')+@SepColumna+
		    isnull(cast(MenuPadreId as varchar(20)),'')+@SepColumna+
			isnull(Comentario,'')+@SepColumna+
			isnull(IconName,'')+@SepColumna+
			isnull(EstadoRegistro,'')+@SepColumna+
			isnull(cast(numeroOrden as varchar(20)),'')
		  from Menu where MenuId = @MenuId
    FOR XML PATH, TYPE).value('.[1]','nvarchar(max)'),1,1,''), '')
	

END


GO
/****** Object:  StoredProcedure [dbo].[uspPemisoObtenerPorId]    Script Date: 02/06/2020 9:31:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[uspPemisoObtenerPorId]    
@lstParametros varchar(max)    
as    
begin
DECLARE @SepListas CHAR(1)='¯',    
	@SepFilas CHAR(1)='¬',    
	@SepColumna CHAR(1)='¦',    
	@SepComodin CHAR(1)='̈ '

DEclare @UsuarioId int     
SELECT @UsuarioId=CAST(@lstParametros as int)    
    
 SELECT
	ISNULL(STUFF((  
		 SELECT '¬'+CAST(m.MenuId AS varchar)+'¦'+ NombreMenu+'¦'+CAST(MenuPadreId as varchar)    
		FROM  Menu m     
		where EstadoRegistro='REG' order by m.numeroOrden asc    
	FOR XML PATH, TYPE).value('.[1]','nvarchar(max)'),1,1,''),'')
	+@SepListas+
	ISNULL( STUFF((    
		SELECT '¬'+ CAST(MenuId as varchar)+'¦'+ cast(PadreId as varchar)    
	  FROM Permisos where UsuarioId=@UsuarioId     
	  FOR XML PATH, TYPE).value('.[1]','nvarchar(max)'),1,1,''),'')   
end    
    
GO
/****** Object:  StoredProcedure [dbo].[uspPermisoGrabar]    Script Date: 02/06/2020 9:31:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[uspPermisoGrabar]  
@lstParametros varchar(max)  
as   
BEGIN  
 BEGIN TRY  
  BEGIN TRANSACTION  
  
 DECLARE @SepListas CHAR(1)='¯',  
  @SepFilas CHAR(1)='¬',  
  @SepColumna CHAR(1)='¦',  
  @SepComodin CHAR(1)='̈ '
 declare @MenuId int   
 declare @UsuarioId int  
 declare @lstPermiso nvarchar(max)  
 declare @UsuarioPermiso int  
  
 select   
 @lstPermiso=field1,  
 @UsuarioPermiso=cast(field2 as int)
 from  [dbo].[fnSplitStringTable10](@lstParametros,@SepComodin,@SepListas)  
  
 if exists(SELECT 0 FROM [dbo].[Permisos] WHERE UsuarioId=@UsuarioPermiso)  
 BEGIN  
  DELETE FROM [dbo].[Permisos] WHERE UsuarioId=@UsuarioPermiso  
 END  
 INSERT INTO [dbo].[Permisos]([MenuId],[UsuarioId],PadreId)  
 select   
 cast(field1 as int),  
 cast(field2 as int),  
 cast(field3 as int)  
 from  [dbo].[fnSplitStringTable10](@lstPermiso,@SepFilas,@SepColumna)
 select '1'  
 COMMIT TRANSACTION  
 END TRY  
 BEGIN CATCH  
  SELECT '-1¯'+ERROR_MESSAGE()
  ROLLBACK TRANSACTION  
 END CATCH  
  
END  
  
  
  
  
  
GO
/****** Object:  StoredProcedure [dbo].[uspProductoListar]    Script Date: 02/06/2020 9:31:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--exec uspClienteListar

create PROCEDURE [dbo].[uspProductoListar]
as	
BEGIN

	DECLARE @SepListas CHAR(1)='¯',
	@SepFilas CHAR(1)='¬',
	@SepColumna CHAR(1)='¦',
	@SepComodin CHAR(1)='̈ ' 

SELECT ISNULL( STUFF((
		SELECT @SepFilas+
	cast(IdProducto as varchar(20))+@SepColumna+
	isnull(Descripcion,'')+@SepColumna+
	cast(Precio as varchar(20))+@SepColumna+
	isnull(Estado,'')

		   
         from [dbo].[Producto]  where  Estado='A'

    FOR XML PATH, TYPE).value('.[1]','nvarchar(max)'),1,1,''), '')
	
END

GO
/****** Object:  StoredProcedure [dbo].[uspProsuctoGrabar]    Script Date: 02/06/2020 9:31:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--exec uspProsuctoGrabar'D215055C-A579-4E87-847B-601CDA188F64¯0¦sdkhvfds¦12'

CREATE PROCEDURE  [dbo].[uspProsuctoGrabar]
@lstparametros varchar(max)
AS 
BEGIN
	BEGIN TRY
		BEGIN TRANSACTION

		DECLARE @SepListas CHAR(1)='¯',
		@SepFilas CHAR(1)='¬',
		@SepColumna CHAR(1)='¦',
		@SepComodin CHAR(1)='̈ ' 

		Declare @listaUsuario nvarchar(100), @listaData nvarchar(max)

		SELECT @listaUsuario=field1, 
				@listaData=field2
		FROM fnSplitStringTable05(@lstparametros,@SepComodin,@SepListas)


		/*OBTENER USUARIO*/
		  Declare @codigoUsuario varchar(20),@idUsuario int,@token nvarchar(100)

        SELECT @token=field1    
        FROM fnSplitStringTable05(@listaUsuario,@SepFilas,@SepColumna)
       
        SELECT             
         @codigoUsuario=CodigoUsuario            
        FROM Token WHERE GUIDToken=@token  
       
        SELECT @idUsuario = rowid FROM [dbo].[llxny_user] where login=@codigoUsuario  

        declare @IdProducto	int	
        declare @Descripcion	varchar(100)	
        declare @Precio			decimal(8, 4)	
        declare @Estado			varchar(1)	

		

	
		SELECT 
			@IdProducto=cast(field1 as int),
		    @Descripcion=field2,
			@Precio		=cast(field3 as decimal(8,4)),
			@Estado		=field4

		FROM  [dbo].[fnSplitStringTable40](@listaData,@SepFilas,@SepColumna)
		IF @IdProducto=0
		BEGIN 

		  INSERT INTO  [dbo].[Producto]
			(		

			Descripcion,
			Precio,
			Estado 
			
			 )
			values (	

			@Descripcion,
			@Precio,
			'A'
			)
			SET @IdProducto=@@IDENTITY
		END
		ELSE
		BEGIN
			
			
		  UPDATE [dbo].[Producto] SET 

			Descripcion=@Descripcion,
			Precio=@Precio,
			Estado='A'

			WHERE IdProducto =@IdProducto 	    

		END
		 select @IdProducto
		COMMIT TRANSACTION
          
	END TRY
	BEGIN CATCH
	ROLLBACK TRANSACTION
		SELECT '-1¯'+ERROR_MESSAGE()
	END CATCH

END


GO
/****** Object:  StoredProcedure [dbo].[uspUserChat_List]    Script Date: 02/06/2020 9:31:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[uspUserChat_List]   
as    
BEGIN    
 DECLARE @SepListas CHAR(1)='¯',  
 @SepFilas CHAR(1)='¬',  
 @SepColumna CHAR(1)='¦',  
 @SepComodin CHAR(1)='̈ '

 SELECT ISNULL( STUFF((
		SELECT @SepFilas+CAST(rowid AS varchar)+@SepColumna +isnull(login, '')
		FROM [llxny_user] where statut = 'REG' order by login asc
	FOR XML PATH, TYPE).value('.[1]','nvarchar(max)'),1,1,''), '')   
	+@SepListas+  
	ISNULL(STUFF((  
		 select DISTINCT @sepFilas +  
		   LEFT(trim([login]), 1)
	 FROM [dbo].[llxny_user] where statut = 'REG'  
	 FOR XML PATH, TYPE).value('.[1]','nvarchar(max)'),1,1,''), '')
END
GO
/****** Object:  StoredProcedure [dbo].[uspUsuarioValidarLogin]    Script Date: 02/06/2020 9:31:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[uspUsuarioValidarLogin]
@lstParametros varchar(100)
AS
BEGIN
	Declare @Lista nvarchar(Max) = ''
	Declare @GUID UniqueIdentifier
	Declare @UsuarioId int
	Declare @CodigoUsuario varchar(20)
	Declare @CodUsuario varchar(20)
	Declare @Contrasena nvarchar(64)
	DECLARE @TipoUsuario AS INT

  Declare @NombreUsuario varchar(max)

	Select
		@CodigoUsuario  = Convert ( varchar(20), field1),
		@Contrasena = Convert ( nvarchar(64), field2 )
	From fnSplitStringTable05(@lstParametros,  '¬', '¦') 
  
	Declare @sepListas char(1) = '¯'
	Declare @sepRegistros char(1) = '¬'
	Declare @sepCampos char(1) = '¦'


	Set @GUID = NewID()

	Select  
		@UsuarioId=rowid,
		@CodUsuario=login,
		@NombreUsuario=isnull(lastname,'')+' '+isnull(firstname,''),
		@TipoUsuario=TipoUsuario
	    From [dbo].[llxny_user]
	    Where upper(login)=upper(@CodigoUsuario) and upper(pass_crypted)=upper(@Contrasena) AND (statut<>'ANU')
 
    If (isnull(@CodUsuario,'')<>'')
		BEGIN
			update dbo.ToKen set EstadoRegistro='ANU' where CodigoUsuario=@CodigoUsuario
			Insert into dbo.ToKen(GUIDToken,CodigoUsuario,EstadoRegistro)Values(@GUID,@CodigoUsuario,'REG')
			Set @Lista =CAST(@UsuarioId AS VARCHAR) + @sepCampos +  CONVERT(VARCHAR(50),@GUID )+ @sepCampos + @NombreUsuario+ @sepCampos +cast(@TipoUsuario as varchar(1))
			Select @Lista
		END
	Else
		BEGIN
			Select '-1'
		END
END


--exec [uspUsuarioValidarLogin] 'mpuma¦3132333635344162'
GO
/****** Object:  StoredProcedure [dbo].[uspUsuarioValidarToken]    Script Date: 02/06/2020 9:31:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[uspUsuarioValidarToken]
@lstParametros nvarchar(100)
as
BEGIN
	Declare @existe nvarchar(20) 

	Declare @sepListas char(1) = '¯'
	Declare @sepRegistros char(1) = '¬'
	Declare @sepCampos char(1) = '¦'

	Select  
	@existe = CodigoUsuario
	from dbo.Token 
	where convert(nvarchar(100),GUIDToken) =@lstParametros and EstadoRegistro='REG'

	if (isnull(@existe,'')<>'')
	begin
		select 'ok'
	end
	else
	--select '-1'
	BEGIN  
	   SELECT ''  
	END  
END


GO
